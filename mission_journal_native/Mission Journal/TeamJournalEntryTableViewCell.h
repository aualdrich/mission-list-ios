//
//  TeamJournalEntry.h
//  Mission Journal
//
//  Created by Austin Aldrich on 3/30/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "JournalEntryTableViewCell.h"
#import <FacebookSDK/FacebookSDK.h>

@interface TeamJournalEntryTableViewCell : JournalEntryTableViewCell

@property (nonatomic, strong) IBOutlet UILabel *teamMemberNameLabel;
@property (nonatomic, strong) IBOutlet FBProfilePictureView *teamProfilePictureView;


-(void) setPropertiesFromInput:(NSDate *)date :(NSString *)description userID:(NSString *)userID;

@end
