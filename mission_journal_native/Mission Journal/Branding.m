//
//  Branding.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "Branding.h"

@implementation Branding


+(UIColor *) getBlueColor {
    return [UIColor colorWithRed:0.34 green:0.78 blue:0.82 alpha:1.0];
}

+(UIColor *) getDarkestGreyColor {
    return [UIColor colorWithRed:0.47 green:0.48 blue:0.49 alpha:1.0];
}

+(UIColor *) getDarkGreyColor {
    return [UIColor colorWithRed:0.60 green:0.62 blue:0.64 alpha:1.0];
}

+(UIColor *) getLightGreyColor {
    return [UIColor colorWithRed:0.89 green:0.90 blue:0.91 alpha:1.0];
}


@end
