//
//  JournalEntryPickerTableViewController.m
//  Mission List
//
//  Created by Austin Aldrich on 7/26/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "JournalEntryPickerTableViewController.h"
#import "JournalEntryTableViewCell.h"
#import "TeamJournalEntryTableViewCell.h"

@interface JournalEntryPickerTableViewController ()

@end

@implementation JournalEntryPickerTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TeamJournalEntryTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TeamCell"];
    
    self.journalEntries = [JournalEntryModel getJournalEntriesForTrip:self.trip.documentID includeTeamPosts:YES];
    
    self.navigationItem.title = @"Pick Entry";
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.journalEntries.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    JournalEntryModel *entry = [self.journalEntries objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"TeamCell";
    TeamJournalEntryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    [cell setPropertiesFromInput:entry.entryDate :entry.entryDescription userID: entry.userID];
    
    
    return cell;
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    JournalEntryModel *entry = [self.journalEntries objectAtIndex:indexPath.row];
    [self.delegate journalEntryPicked:entry];
    [self.navigationController popViewControllerAnimated:YES];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 134.0f;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
