//
//  AnonymousProfileViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 3/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "AnonymousProfileViewController.h"
#import "UserService.h"
#import "FBUserModel.h"
#import "AnonymousUserModel.h"
#import "MyProfileViewController.h"
#import "DatabaseService.h"
#import "WelcomeService.h"


@interface AnonymousProfileViewController ()

@end

@implementation AnonymousProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
          self.loginView = [[FBLoginView alloc] init];
        self.loginView.delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(menuPressed:)];
    self.navigationItem.leftBarButtonItem = menuButton;
    
   [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login Error" message:@"Oops! We ran into an error when trying to connect to Facebook. Please ensure you have a connection and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alertView show];
}


-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user {
    [UserService resetCurrentUser];
    [UserService saveFBUserDefaults:user];
    [WelcomeService sendWelcomeEmail];
    [DatabaseService generateViews]; //the user id has changed, so the view's user check is out of sync
    
    
    MyProfileViewController *profileViewController = [MyProfileViewController new];
    [self.navigationController pushViewController:profileViewController animated:NO];
    
    
}

-(IBAction)menuPressed:(id)sender {
    
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionAnchoredRight) {
        [self.slidingViewController resetTopViewAnimated:YES];
    }
    
    else {
        [self.slidingViewController anchorTopViewToRightAnimated:YES];
    }
    
    
}


@end
