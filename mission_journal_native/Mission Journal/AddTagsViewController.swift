//
//  AddTagsViewController.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class AddTagsViewController : UIViewController, UIAlertViewDelegate, UITextFieldDelegate {
    @IBOutlet var tagListView = AMTagListView()
    @IBOutlet var addTagField = UITextField()
    var tags = NSMutableArray()
    var selectedTag = AMTagView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AMTagView.appearance().tagColor = Branding.getBlueColor()
        AMTagView.appearance().innerTagColor = Branding.getBlueColor()
        AMTagView.appearance().textColor = UIColor.whiteColor()
        
        self.populateTagsFromModel()
        
    }
    
    func alertView(alertView :UIAlertView, clickedButtonAtIndex buttonIndex :NSInteger) {
        if(buttonIndex == 1) {
            var tag = self.selectedTag.tagText()
            self.tagListView.removeTag(self.selectedTag)
        }
    }
    
    func textFieldShouldReturn(textField :UITextField) -> Bool {
       var tagText = textField.text
        
        if(!self.tagExists(tagText)) {
            var newTag = TagModel.getNewOrExistingTag(tagText)
            self.tags.addObject(newTag)
            
            self.tagListView.addTag(tagText)
            textField.text = ""
        }
        
        return true
    }
    
    func tagExists(tag :NSString) -> Bool {
        for tag :AnyObject in self.tags {
           var objTag = tag as TagModel
            
            if(objTag.tagName.lowercaseString == tag.lowercaseString()) {
               return true
            }
        }
        
        return false
    }
    
    
    func removeTag(tag :String) {
       var tagIndex = -1
        
        for i in 0..self.tags.count() {
           var t = self.tags.objectAtIndex(i) as TagModel
            
            if t.tagName.lowercaseString == tag.lowercaseString {
                tagIndex = i
                break
            }
        }
        
        if(tagIndex != -1) {
            self.tags.removeObjectAtIndex(tagIndex)
        }
        
    }
    
    func populateTagsFromModel() {
        for t in self.tags {
            var tag = t as TagModel
            self.tagListView.addTag(tag.tagName)
        }
    }
    
    
    
    
}