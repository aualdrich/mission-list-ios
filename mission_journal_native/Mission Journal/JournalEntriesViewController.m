    //
//  JournalEntriesViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/9/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "JournalEntriesViewController.h"
#import "AddJournalEntryViewController.h"
#import "JournalEntryTableViewCell.h"
#import "AddTripViewController.h"
#import "TeamListTableViewController.h"
#import <Reachability.h>
#import "TeamJournalEntryTableViewCell.h"
#import "ViewJournalEntryViewController.h"
#import "ManageNewslettersViewController.h"

#define kMyEntryTypesIndex 0
#define kTeamEntryTypesIndex 1

@interface JournalEntriesViewController ()

@end

@implementation JournalEntriesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.journalEntries = [[NSMutableDictionary alloc] init];
        self.entriesTableView = [[UITableView alloc] init];
        self.entriesTableView.delegate = self;
        self.entriesTableView.dataSource = self;
        
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addJournalEntry:)];
    
    [self.entriesTableView registerNib:[UINib nibWithNibName:@"JournalEntryTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"Cell"];
    
    [self.entriesTableView registerNib:[UINib nibWithNibName:@"TeamJournalEntryTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TeamCell"];
    
    
}

- (void) viewWillAppear:(BOOL)animated {
    self.navigationItem.title = self.trip.tripName;

    
    
    if([self.defaultViewMode isEqualToString: @"Team"])
        self.entryTypeSegmentedControl.selectedSegmentIndex = kTeamEntryTypesIndex;
    
    [self loadTripEntriesForCurrentView];
    
    if(![self.trip isTeammate]) {
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
        self.newsletterButton.enabled = NO;
    }
    
    self.editTripButton.enabled = [self.trip isTripOwner];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.journalEntries allKeys] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSDate *key = [[self.journalEntries allKeys] objectAtIndex:section];
    NSArray *itemsWithKey = [self.journalEntries objectForKey:key];
    
    return itemsWithKey.count;
    
}

-(JournalEntryModel *) getJournalEntryForIndexPath:(NSIndexPath *)indexPath {
    NSDate *dateKey = [[self.journalEntries allKeys] objectAtIndex:indexPath.section];
    NSArray *entriesInSection = [self.journalEntries objectForKey:dateKey];
    JournalEntryModel *entry = [entriesInSection objectAtIndex:indexPath.row];
    
    return entry;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    JournalEntryModel *entry = [self getJournalEntryForIndexPath:indexPath];
    
  
    if([self isInPersonalMode]) {
        static NSString *CellIdentifier = @"Cell";
        JournalEntryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        // Configure the cell...
        [cell setPropertiesFromInput:entry.entryDate :entry.entryDescription];
            
        return cell;
        
    }
   
    //team cell
    static NSString *CellIdentifier = @"TeamCell";
    
    TeamJournalEntryTableViewCell *teamCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    [teamCell setPropertiesFromInput:entry.entryDate :entry.entryDescription userID:entry.userID];
    
    return teamCell;
}


- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSDate *dateKey = [[self.journalEntries allKeys] objectAtIndex:section];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM yyyy"];
    
    return [dateFormatter stringFromDate:dateKey];
    
}

-(IBAction)addJournalEntry:(id)sender {
    AddJournalEntryViewController *addJournalEntryViewController = [AddJournalEntryViewController new];
    addJournalEntryViewController.trip = self.trip;
    addJournalEntryViewController.newEntryMode = YES;
    [self.navigationController pushViewController:addJournalEntryViewController animated:YES];
}


-(void) getJournalEntriesWithOrWithoutTeamPosts:(BOOL)includeTeamPosts {
    
    [self.journalEntries removeAllObjects];
    
    NSArray *tempEntries = [JournalEntryModel getJournalEntriesForTrip:self.trip.documentID includeTeamPosts:includeTeamPosts];
    
    
    if(tempEntries != nil) {
    
        NSSortDescriptor *dateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"entryDate" ascending:NO];
        NSArray *sortedEntriesByDate = [tempEntries sortedArrayUsingDescriptors:@[dateSortDescriptor]];
        
        for (JournalEntryModel *j in sortedEntriesByDate) {
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                                           fromDate:j.entryDate];
            //we group all entries by month and year, so we always need to use the 1st day to keep dates in the same month and year together.
            components.day = 1;
            NSDate *dateKey = [[NSCalendar currentCalendar] dateFromComponents: components];
            
            NSMutableArray *itemsForKey = [self.journalEntries objectForKey:dateKey];
            if(itemsForKey == nil) {
                itemsForKey = [[NSMutableArray alloc] init];
            }
            [itemsForKey addObject:j];
            
            [self.journalEntries setObject:itemsForKey forKey:dateKey];
            

        }
        
    }
    
    [self.entriesTableView reloadData];
    

    
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    JournalEntryModel *entry = [self getJournalEntryForIndexPath:indexPath];
    return [entry isJournalEntryOwner];
}

- (BOOL) tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    JournalEntryModel *entry = [self getJournalEntryForIndexPath:indexPath];
    
    if([entry isJournalEntryOwner]) {
        AddJournalEntryViewController *editJournalEntryViewController = [AddJournalEntryViewController new];
        editJournalEntryViewController.trip = self.trip;
        editJournalEntryViewController.newEntryMode = NO;
        editJournalEntryViewController.journalEntry = entry;
        
        [self.navigationController pushViewController:editJournalEntryViewController animated:YES];
    }
    
    else {
        ViewJournalEntryViewController *viewJournalEntryViewController = [ViewJournalEntryViewController new];
        viewJournalEntryViewController.trip = self.trip;
        viewJournalEntryViewController.entry = entry;
        
        [self.navigationController pushViewController:viewJournalEntryViewController animated:YES];
    }
        
    
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSString *dateKey = [[self.journalEntries allKeys] objectAtIndex:indexPath.section];
        
        NSMutableArray *entriesForDates = [self.journalEntries objectForKey:dateKey];
        JournalEntryModel *journalEntryToDelete = [entriesForDates objectAtIndex:indexPath.row];
        
       [journalEntryToDelete deleteDoc];
        
        [entriesForDates removeObjectAtIndex:indexPath.row];
        
        //also remove the section if that was the last entry for the section
        if(entriesForDates.count == 0) {
            [self.journalEntries removeObjectForKey:dateKey];
        }
        
        [self.entriesTableView reloadData];
        
    }
}

-(IBAction)editTrip:(id)sender {
    AddTripViewController *editTripViewController = [[AddTripViewController alloc] initWithStyle:UITableViewStylePlain];
    editTripViewController.trip = self.trip;
    editTripViewController.editMode = YES;
    [editTripViewController updateFormFields];
    [self.navigationController pushViewController:editTripViewController animated:YES];
    
    
    
}

-(IBAction)myTeamClicked:(id)sender {
    TeamListTableViewController *teamListTableViewController = [TeamListTableViewController new];
    teamListTableViewController.trip = self.trip;
    [self.navigationController pushViewController:teamListTableViewController animated:YES];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self isInTeamMode] ? 134.0f : 93.0f;
}

-(IBAction)entryTypeValueChanged:(id)sender {
    [self loadTripEntriesForCurrentView];

    
}

-(void) loadTripEntriesForCurrentView {
    if(self.entryTypeSegmentedControl.selectedSegmentIndex == kMyEntryTypesIndex) {
        [self getJournalEntriesWithOrWithoutTeamPosts:NO];
    }
    
    else if([self isInTeamMode]) {
        [self getJournalEntriesWithOrWithoutTeamPosts:YES];
    }
}

-(BOOL) isInTeamMode {
    return self.entryTypeSegmentedControl.selectedSegmentIndex == kTeamEntryTypesIndex;
}

-(BOOL) isInPersonalMode {
    return self.entryTypeSegmentedControl.selectedSegmentIndex == kMyEntryTypesIndex;
}

-(IBAction)shareTripClicked:(id)sender {
    
    [FBDialogs presentShareDialogWithLink:[self.trip getTripURL] name:self.trip.tripName handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
        if(error) {
            NSLog(@"Error: %@", error.description);
        } else {
            NSLog(@"Success!");
        }
        
    }];
    
}

-(IBAction)newslettersClicked:(id)sender {
    ManageNewslettersViewController *newslettersController = [ManageNewslettersViewController new];
    newslettersController.trip = self.trip;
    
    [self.navigationController pushViewController:newslettersController animated:YES];
}




@end
