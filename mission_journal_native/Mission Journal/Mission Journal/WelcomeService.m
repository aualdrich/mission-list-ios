//
//  WelcomeService.m
//  Mission List
//
//  Created by Austin Aldrich on 7/19/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "WelcomeService.h"
#import "UserService.h"
#import <AFNetworking.h>
#import "Constants.h"
#import "APIService.h"

@implementation WelcomeService


+(void) sendWelcomeEmail {
    FBUserModel *user = (FBUserModel *)[UserService currentUser];
    
    if([user.welcomeEmailSent.lowercaseString isEqualToString:@"true"])
        return;
    
    AFHTTPRequestOperationManager *manager = [APIService buildRequestManager];
    NSDictionary *parameters = @{@"to_address": user.email, @"first_name": user.firstName == nil ? @"" : user.firstName};
    
    NSString *url = [NSString stringWithFormat:@"%@/welcome/send_welcome_email", [Constants getBaseURL]];
    
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        user.welcomeEmailSent = @"true";
        [user save];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

@end
