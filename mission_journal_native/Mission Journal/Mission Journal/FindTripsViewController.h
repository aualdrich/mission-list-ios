//
//  FindTripsViewController.h
//  Mission List
//
//  Created by Austin Aldrich on 7/7/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripResultTableViewCell.h"

@interface FindTripsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) IBOutlet UITableView *searchTableView;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;



@end
