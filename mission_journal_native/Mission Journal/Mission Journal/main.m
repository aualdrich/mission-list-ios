//
//  main.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/1/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MissionsInMotionAppDelegate.h"
#import "TestAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        BOOL runningTests = NSClassFromString(@"XCTestC ase") != nil;
        if(!runningTests)
        {
            return UIApplicationMain(argc, argv, nil, NSStringFromClass([MissionsInMotionAppDelegate class]));
        }
        else
        {
            return UIApplicationMain(argc, argv, nil, NSStringFromClass([TestAppDelegate class]));
        }
    }
}
