//
//  AnonymousUserModel.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "AnonymousUserModel.h"

@implementation AnonymousUserModel

-(NSMutableDictionary *) getDocumentModel {
    NSMutableDictionary *model = [super getDocumentModel];
    [model addEntriesFromDictionary:@{@"doc_type": @"anonymous_user"}];
    return model;
}

@end
