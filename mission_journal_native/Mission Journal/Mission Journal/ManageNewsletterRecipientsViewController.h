//
//  ManageNewsletterRecipientsViewController.h
//  Mission List
//
//  Created by Austin Aldrich on 8/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsletterModel.h"
#import "AddNewsletterRecipientViewController.h"
#import "NewsletterRecipientModel.h"
#import "TripNewsletterRecipientsModel.h"
#import "TripModel.h"


@interface ManageNewsletterRecipientsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, AddNewsletterRecipientDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *recipients;
@property (nonatomic, strong) TripNewsletterRecipientsModel *tripNewsletterRecipients;

-(void) build:(TripModel *) trip;



@end
