//
//  TripModel.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/4/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class TripModel : DocumentModel {
    var tripName = ""
    var teamName = ""
    var startDate = NSDate()
    var endDate = NSDate()
    var userID = ""
    var isPublicTrip = false
    var teammateUserIDs = NSMutableArray()
    var tripDescription = ""
    
    init() {
        
    }
    
    
    override func getDocumentModel() -> NSMutableDictionary {
        var model = NSMutableDictionary()
        
        model.setValue(self.tripName, forKey: "trip_name")
        model.setValue(self.teamName, forKey: "team_name")
        
        var startDateVal = self.startDate != nil ? CBLJSON.JSONObjectWithDate(self.startDate) : ""
        var endDateVal = self.endDate != nil ? CBLJSON.JSONObjectWithDate(self.endDate) : ""
        
        model.setValue(startDateVal, forKey: "start_date")
        model.setValue(endDateVal, forKey: "end_date")
        model.setValue(self.userID, forKey: "user_id")
        model.setValue(self.isPublicTrip ? "true" : "false", forKey: "is_public_trip")
        model.setValue("trip", forKey: "doc_type")
        model.setValue(self.teammateUserIDs, forKey: "teammate_user_ids")
        model.setValue(self.tripDescription, forKey: "description")
        
        return model
    }
    
    
    class func getTrips(rows :CBLQueryEnumerator) -> NSMutableArray {
        return TripQueries.getTrips(rows);
    }
    
    class func getTripByID(tripID :String) -> TripModel {
      
        var document = DatabaseService.getDatabase().documentWithID(tripID)
        return self.buildTripFromDocument(document)
    }
    
    class func buildTripFromDocument(document :CBLDocument) -> TripModel {
        var trip = TripModel()
        trip.documentID = document.documentID
        trip.isPublicTrip = true
        
        var tripName = document.properties.valueForKey("trip_name") as String
        var teamName = document.properties.valueForKey("team_name") as String
        var startDate = document.properties.valueForKey("start_date") as String
        var endDate = document.properties.valueForKey("end_date") as String
        var is_public_trip = document.properties.valueForKey("is_public_trip")
        var userID = document.properties.valueForKey("user_id") as String
        var teammateUserIDs = document.properties.valueForKey("teammate_user_ids") as NSMutableArray
        var tripDesc = document.properties.valueForKey("description") as String
        
        
        if(!tripName.isEmpty) {
            trip.tripName = tripName
        }
        
        if(!teamName.isEmpty) {
            trip.teamName = teamName
        }
        
        if(!startDate.isEmpty) {
           trip.startDate = CBLJSON.dateWithJSONObject(startDate)
        }
        
        if(!endDate.isEmpty) {
           trip.endDate = CBLJSON.dateWithJSONObject(endDate)
        }
        
        if(!userID.isEmpty) {
            trip.userID = userID
        }
        
        //this is an awful hack to deal with the way the app stores TRUE as a string but
        //Rails stores it as an actual bit field.
        if(is_public_trip.isKindOfClass(NSNumber)) {
           
            var numVal = is_public_trip as NSNumber
            if(numVal.isEqual(0)) {
                trip.isPublicTrip = false
            }
        }
        
        else if(is_public_trip.isEqualToString("false")) {
            trip.isPublicTrip = true
        }
        
        if(teammateUserIDs.count() > 0) {
            trip.teammateUserIDs = NSMutableArray(array: teammateUserIDs)
        }
        
        if(!tripDesc.isEmpty) {
            trip.tripDescription = tripDesc
        }
       
        return trip
        
    }
    
    func isTeammateInTrip(teammateUserIDs :NSMutableArray, currentUserID :NSString) -> Bool{
        if(teammateUserIDs == nil) {
            return false
        }
        
        return teammateUserIDs.containsObject(currentUserID)
        
        
    }
    
    func isTripOwner() -> Bool {
        return self.userID == UserService.currentUser.userID
    }
    
    func isTeammate() -> Bool {
        var currentUser = UserService.currentUser
        
        if(self.userID == currentUser.userID) {
            return true
        }
        
        if(self.teammateUserIDs == nil || self.teammateUserIDs.count() > 0) {
            return false
        }
        
        return self.teammateUserIDs.containsObject(currentUser.userID)
    }
    
    override func save() {
        self.saveOrUpdateWithId(self.documentID)
    }
    
    func getFormattedTripDates() -> String {
       var dateFormatter = NSDateFormatter()
       dateFormatter.setDateFormat("MMM d yyyy")
       
        var formattedStartDate = (self.startDate != nil) ? dateFormatter.stringFromDate(self.startDate) : ""
        var formattedEndDate = (self.endDate != nil) ? dateFormatter.stringFromDate(self.endDate) : ""
        var formattedDates = "\(formattedStartDate) - \(formattedEndDate)"
        
        return formattedDates
    }
    
    func getTripURL() -> NSURL {
        var url = "http://www.missionlist.org/trips/show/\(self.documentID)"
        return NSURL.URLWithString(url) as NSURL
    }
    
    func userIsFollowingTrip() -> Bool {
        var currentUser = UserService.currentUser
        
        var tripsFollowed = currentUser.tripsFollowed as NSMutableArray
        
        for tripID :AnyObject in currentUser.tripsFollowed {
            var tripIDString = tripID as String
            if(tripIDString.lowercaseString == self.documentID.lowercaseString) {
               return true
            }
        }
        
        return false
    }
    
    
}
