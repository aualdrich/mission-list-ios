//
//  TagModel.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/11/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TagModel.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"
#import "UserService.h"

@implementation TagModel


-(NSMutableDictionary *) getDocumentModel {    
    return [@{@"tag_name": self.tagName, @"user_id": self.userID, @"doc_type": @"tag"} mutableCopy];
}


+(NSMutableArray *) getTags {
    
    NSMutableArray *tags = [@[] mutableCopy];
    
    CBLDatabase *database = [DatabaseService getDatabase];
    
    CBLQuery *tripListQuery = [[database viewNamed:@"tag_list"] createQuery];
    
    NSError *error;
    
    CBLQueryEnumerator *enumerator = [tripListQuery run:&error];
    
    for(CBLQueryRow *row in enumerator) {
        TagModel *tag = [TagModel new];
        
        [tag setTagProperties:row.document];
        [tags addObject:tag];
        
        
    }
    
    
    NSMutableArray *combinedTags = [NSMutableArray arrayWithArray:tags];
    
    NSSet *distinctTagsSet = [NSSet setWithArray:combinedTags];
    
    NSMutableArray *distinctTags = [[distinctTagsSet allObjects] mutableCopy];
    
    return distinctTags;
    
    
}

+(NSMutableArray *) getDefaultTags {
    
    NSString *userID = [[UserService currentUser] userID];
    
    TagModel *miracle = [TagModel new];
    TagModel *salvation = [TagModel new];
    TagModel *interesting_location = [TagModel new];
    TagModel *story = [TagModel new];
    
    miracle.tagName = @"miracle";
    miracle.userID = userID;
    
    salvation.tagName = @"salvation";
    salvation.userID = userID;
    
    interesting_location.tagName = @"interesting location";
    interesting_location.userID = userID;
    
    story.tagName = @"story";
    story.userID = userID;
    
    return [@[miracle, interesting_location, salvation, story] mutableCopy];
    
}

+(TagModel *) getNewOrExistingTag:(NSString *)tagName {
    
    NSMutableArray *tags = [self getTags];
    
    for (TagModel *tag in tags) {
        if([tag.tagName.lowercaseString isEqualToString:tagName.lowercaseString]) {
            return tag;
        }
    }
    
    
    TagModel *newTag = [[TagModel alloc] init];
    newTag.tagName = tagName;
    newTag.userID = [[UserService currentUser] userID];
    newTag.documentID = [[NSUUID UUID] UUIDString];
    return newTag;
    
}

+(void) saveTags:(NSArray *)tags {
    for (TagModel *tag in tags) {
        [tag save];
    }
}

-(void) save {
    [self saveOrUpdateWithId:self.documentID];
    
}

-(void) setTagProperties:(CBLDocument *)document {
    
    self.documentID = document.documentID;
    
    NSString *tagName = [document.properties valueForKey:@"tag_name"];
    NSString *userID = [document.properties valueForKey:@"user_id"];
    
    if(tagName)
        self.tagName = tagName;
    
    if(userID)
        self.userID = userID;

}


+(TagModel *) getTag:(NSString *)docID {
    
    CBLDatabase *db = [DatabaseService getDatabase];
    CBLDocument *doc = [db documentWithID:docID];
    
    TagModel *tag = [[TagModel alloc] init];
    [tag setTagProperties:doc];
    
    return tag;
}




@end
