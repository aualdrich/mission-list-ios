//
//  NewsletterHTMLRenderer.m
//  Mission List
//
//  Created by Austin Aldrich on 7/28/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "NewsletterHTMLRenderer.h"
#import <GRMustache.h>
#import "NewsletterModel.h"

@implementation NewsletterHTMLRenderer


-(id) init {
    
    self = [super init];
    
    if(self) {
       
    }
    
    return self;
}

-(NSString *) buildHTMLForItems:(NSMutableArray *)items :(TripModel *) trip :(NSString *) customMessage {
    
    NSMutableString *html = [NSMutableString string];
    
    for (id item in items) {
        
        if([item isKindOfClass:[JournalEntryModel class]]) {
            NSString *entryHTML = [self buildHTMLForJournalEntry:(JournalEntryModel *) item];
            [html appendString:entryHTML];
        }
        
        else if([item isKindOfClass:[PhotoModel class]]) {
            NSString *entryHTML = [self buildHTMLForPhoto:(PhotoModel *) item];
            [html appendString:entryHTML];
        }
        
    }
    
    
    GRMustacheTemplate *template = [GRMustacheTemplate templateFromResource:@"newsletter_shell" bundle:nil error:nil];
    
    NewsletterModel *newsletter = [NewsletterModel new];
    newsletter.itemsContent = html;
    newsletter.customMessage = customMessage;
    newsletter.tripName = trip.tripName;
    
    NSString *rendering = [template renderObject:newsletter error:nil];
    
    return rendering;
}

-(NSString *) buildHTMLForJournalEntry:(JournalEntryModel *)entry {
    
    GRMustacheTemplate *template = [GRMustacheTemplate templateFromResource:@"journal_entry" bundle:nil error:nil];
    
    NSString *rendering = [template renderObject:entry error:nil];
    
    return rendering;
    
}

-(NSString *) buildHTMLForPhoto:(PhotoModel *)photo {
    
    GRMustacheTemplate *template = [GRMustacheTemplate templateFromResource:@"photo" bundle:nil error:nil];
    
    NSString *rendering = [template renderObject:photo error:nil];
    
    return rendering;
}

@end
