//
//  NewsletterService.m
//  Mission List
//
//  Created by Austin Aldrich on 8/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "NewsletterService.h"
#import <AFNetworking.h>
#import "APIService.h"
#import "Constants.h"
#import "TripNewsletterRecipientsModel.h"
#import "NewsletterRecipientModel.h"
#import "UserService.h"

@implementation NewsletterService


+(void) sendNewsletter:(NSString *) newsletterID :(TripModel *) trip {
    
    AFHTTPRequestOperationManager *manager = [APIService buildRequestManager];
    
    NSString *userID = [[UserService currentUser] userID];
    
    NSDictionary *parameters = @{@"newsletter_id": newsletterID, @"user_id":userID };
    
    NSString *url = [NSString stringWithFormat:@"%@/newsletter/send_newsletter", [Constants getBaseURL]];
    
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
       //do something after success
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}


@end