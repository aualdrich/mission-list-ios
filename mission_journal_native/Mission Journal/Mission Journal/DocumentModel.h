//
//  DocumentModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/7/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DocumentModel : NSObject

@property (nonatomic, strong) NSString *documentID;

-(NSMutableDictionary *)getDocumentModel;

-(void) save;
-(void) update;
-(void) saveOrUpdateWithId:(NSString *) docID;
-(void) deleteDoc;

@end
