//
//  MissionsInMotionAppDelegate.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/1/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "MissionsInMotionAppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import <ECSlidingViewController.h>
#import "TripListViewController.h"
#import "MenuViewController.h"
#import "Branding.h"
#import "UserService.h"
#import "PhotoModel.h"
#import <Reachability.h>
#import "LoginViewController.h"
#import "TutorialViewController.h"
#import <TestFlight.h>
#import "JournalEntriesViewController.h"
#import "TripNotificationBuilder.h"
//#import <Pushbots/Pushbots.h>

@implementation MissionsInMotionAppDelegate


// FBSample logic
// If we have a valid session at the time of openURL call, we handle Facebook transitions
// by passing the url argument to handleOpenURL; see the "Just Login" sample application for
// a more detailed discussion of handleOpenURL
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                    fallbackHandler:^(FBAppCall *call) {
                        NSLog(@"In fallback handler");
                    }];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // FBSample logic
    // if the app is going away, we close the session object
    [FBSession.activeSession close];
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    // Override point for customization after application launch.
    [FBLoginView class];
    [FBProfilePictureView class];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    
    
    if([self firstLaunch]) {
        
        TutorialViewController *welcomeViewController = [[TutorialViewController alloc] init];
        [self.window setRootViewController:welcomeViewController];        
    }
    
    else {
        TripListViewController *tripListViewController = [TripListViewController new];
        [self setRootViewControllerState:tripListViewController];
    }
    
    [TripNotificationBuilder buildTripNotificationsForUser];

    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
	[[UINavigationBar appearance] setTitleTextAttributes:attributes];
    [[UINavigationBar appearance] setBarTintColor:[Branding getBlueColor]];
	[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    
    return YES;
}

-(void) showLoginViewController {
    LoginViewController *loginViewController = [LoginViewController new];
    [self.window setRootViewController:loginViewController];
}

-(void) setRootViewControllerState:(UIViewController *)rootViewController {
    self.navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    
    ECSlidingViewController *slidingViewController = [ECSlidingViewController slidingWithTopViewController:self.navController];
    
    MenuViewController *menuViewController = [MenuViewController new];
    slidingViewController.underLeftViewController = menuViewController;
    
    [self.navController.view addGestureRecognizer:slidingViewController.panGesture];
    
    [self.window setRootViewController:slidingViewController];
}

- (BOOL) firstLaunch {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL firstLaunch = ![userDefaults boolForKey:@"HasLaunched"];
    return firstLaunch;
}

-(void) finishFirstLaunch {
    
    TripListViewController *tripListViewController = [TripListViewController new];
    
    [self setRootViewControllerState:tripListViewController];
}


//foreground notification event
-(void )application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    NSString *tripID = [notification.userInfo objectForKey:@"trip_id"];
    
    if(tripID) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Trip Alert" message:notification.alertBody delegate:self cancelButtonTitle:@"Thanks !" otherButtonTitles: notification.alertAction,nil];
        
        self.tripIDFromNotification = tripID;
        
        [message show];
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex != alertView.cancelButtonIndex) {
        NSInteger badgeCount = [[UIApplication sharedApplication] applicationIconBadgeNumber];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: badgeCount > 0 ? badgeCount - 1 : 0];
       
        JournalEntriesViewController *journalEntriesViewController = [JournalEntriesViewController new];
        journalEntriesViewController.trip = [TripModel getTrip:self.tripIDFromNotification];
        
        [self.navController pushViewController:journalEntriesViewController animated:YES];
        
    }
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
       [FBAppEvents activateApp];
       [FBAppCall handleDidBecomeActive];
    
        [PhotoModel syncRemotePhotosInQueue:@""];
    
}


//-(void)onReceivePushNotification:(NSDictionary *) pushDict andPayload:(NSDictionary *)payload {
//    [payload valueForKey:@"title"];
//    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"New Alert!" message:[pushDict valueForKey:@"alert"] delegate:self cancelButtonTitle:@"Thanks !" otherButtonTitles: nil,nil];
//    [message show];
//}



#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}




@end
