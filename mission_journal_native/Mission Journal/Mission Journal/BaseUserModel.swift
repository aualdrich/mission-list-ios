//
//  BaseUserModel.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/4/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation


class BaseUserModel : DocumentModel {
    var userID = ""
    var tripsFollowed = NSMutableArray()
   
    override func save() {
        self.saveOrUpdateWithId(self.userID)
    }
    
    
}