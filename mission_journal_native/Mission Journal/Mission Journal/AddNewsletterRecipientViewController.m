//
//  AddNewsletterRecipientViewController.m
//  Mission List
//
//  Created by Austin Aldrich on 8/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "AddNewsletterRecipientViewController.h"

@interface AddNewsletterRecipientViewController ()

@end

@implementation AddNewsletterRecipientViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
    
    self.firstNameCell = [BZGFormFieldCell new];
    self.firstNameCell.label.text = @"First Name";
    self.firstNameCell.textField.returnKeyType = UIReturnKeyDone;
    
    self.lastNameCell = [BZGFormFieldCell new];
    self.lastNameCell.label.text = @"Last Name";
    self.lastNameCell.textField.returnKeyType = UIReturnKeyDone;
    
    self.emailCell = [BZGFormFieldCell new];
    self.emailCell.label.text = @"Email";
    self.emailCell.textField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailCell.textField.returnKeyType = UIReturnKeyDone;
   
    self.formFieldCells = [@[self.firstNameCell, self.lastNameCell, self.emailCell] mutableCopy];
    
    self.navigationItem.title = @"Add Recipient";
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) save {
    self.recipient.firstName = self.firstNameCell.textField.text;
    self.recipient.lastName = self.lastNameCell.textField.text;
    self.recipient.email = self.emailCell.textField.text;
    
    if(!self.editMode)
        self.recipient.documentID = [[NSUUID UUID] UUIDString];
    
    [self.recipient save];
    
    [self.delegate newsletterRecipientSaved:self.recipient];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void) build:(NewsletterRecipientModel *)recipient :(BOOL)editMode {
    self.recipient = recipient;
    self.editMode = editMode;
    self.firstNameCell.textField.text = recipient.firstName;
    self.lastNameCell.textField.text = recipient.lastName;
    self.emailCell.textField.text = recipient.email;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
