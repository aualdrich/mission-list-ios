//
//  ManageNewsletterRecipientsViewController.m
//  Mission List
//
//  Created by Austin Aldrich on 8/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "ManageNewsletterRecipientsViewController.h"
#import "NewsletterRecipientModel.h"
#import "AddNewsletterRecipientViewController.h"
#import "UserService.h"

@interface ManageNewsletterRecipientsViewController ()

@end

@implementation ManageNewsletterRecipientsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.recipients = [@[] mutableCopy];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addRecipient)];
    
    
    self.navigationItem.title = @"Manage Recipients";
    

}

-(void) build:(TripModel *)trip {
    if(!trip.tripNewsletterRecipientsID) {
        self.tripNewsletterRecipients = [TripNewsletterRecipientsModel new];
        self.tripNewsletterRecipients.documentID = [[NSUUID UUID] UUIDString];
        self.tripNewsletterRecipients.tripID = trip.documentID;
        self.tripNewsletterRecipients.userID = [[UserService currentUser] userID];
        self.tripNewsletterRecipients.recipientIDs = [@[] mutableCopy];
        
        [self.tripNewsletterRecipients save];
        
        trip.tripNewsletterRecipientsID = self.tripNewsletterRecipients.documentID;
        [trip save];
    }
    
    else {
        self.tripNewsletterRecipients = [TripNewsletterRecipientsModel getTripNewsletterRecipients:trip.tripNewsletterRecipientsID];
    }
    
    [self reloadRecipients];
    
    
}

-(void) reloadRecipients {
    [self.recipients removeAllObjects];
    
    for(NSString *recipientID in self.tripNewsletterRecipients.recipientIDs) {
        NewsletterRecipientModel *recipient = [NewsletterRecipientModel getByID:recipientID];
        [self.recipients addObject:recipient];
    }
    
    [self.tableView reloadData];
}

-(void) viewWillAppear:(BOOL)animated {
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) addRecipient {
    AddNewsletterRecipientViewController *addNewsletterRecipientViewController = [AddNewsletterRecipientViewController new];
    addNewsletterRecipientViewController.delegate = self;
    
    NewsletterRecipientModel *recipient = [NewsletterRecipientModel new];
    [addNewsletterRecipientViewController build:recipient :NO];
    [self.navigationController pushViewController:addNewsletterRecipientViewController animated:YES];
    
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.recipients.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NewsletterRecipientModel *recipient = [self.recipients objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", recipient.firstName, recipient.lastName];
    
    
    return cell;
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        
        NewsletterRecipientModel *recipient = [self.recipients objectAtIndex:indexPath.row];
        [recipient deleteDoc];
        
        [self.recipients removeObjectAtIndex:indexPath.row];
        [self.tripNewsletterRecipients.recipientIDs removeObjectAtIndex:indexPath.row];
        [self.tripNewsletterRecipients save];
        
        [self.tableView reloadData];
    }
}

-(void) newsletterRecipientSaved:(NewsletterRecipientModel *)recipient {
    if(![self.recipients containsObject:recipient]) {
        
        [self.recipients addObject:recipient];
        [self.tripNewsletterRecipients.recipientIDs addObject:recipient.documentID];
        [self.tripNewsletterRecipients save];
    }
    
    [self.tableView reloadData];
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsletterRecipientModel *recipient = [self.recipients objectAtIndex:indexPath.row];
    
    AddNewsletterRecipientViewController *addNewsletterRecipientViewController = [AddNewsletterRecipientViewController new];
    addNewsletterRecipientViewController.delegate = self;
    
    [addNewsletterRecipientViewController build:recipient :YES];
    [self.navigationController pushViewController:addNewsletterRecipientViewController animated:YES];
}


@end
