//
//  AddJournalEntryViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/9/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripModel.h"
#import "AddTagsViewController.h"
#import "JournalEntryModel.h"
#import "MWPhotoBrowser.h"
#import <AssetsLibrary/AssetsLibrary.h>


@interface AddJournalEntryViewController : UIViewController <UITextViewDelegate, MWPhotoBrowserDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) TripModel *trip;
@property (nonatomic, strong) IBOutlet UITextView *descriptionTextView;
@property (nonatomic, strong) IBOutlet UITextField *dateTextField;
@property (nonatomic, strong) IBOutlet UISwitch *isPrivateSwitch;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) AddTagsViewController *addTagsViewController;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *mwPhotos;
@property (nonatomic, strong) NSMutableArray *mwThumbPhotos;
@property (nonatomic, strong) JournalEntryModel *journalEntry;
@property (nonatomic) BOOL newEntryMode;
@property (nonatomic, strong) NSString *PLACEHOLDER_TEXT;
@property (nonatomic, strong) MWPhotoBrowser *photoBrowser;

-(IBAction)saveEntry:(id)sender;
-(IBAction)tagsSelected:(id)sender;
-(IBAction)shareSelected:(id)sender;
-(IBAction)dateChanged:(id)sender;
-(IBAction)dismissKeyboard:(id)sender;
-(IBAction)photosSelected:(id)sender;
-(IBAction)addPhotoClicked:(id)sender;

-(void) populateFieldsFromEntry;


@end
