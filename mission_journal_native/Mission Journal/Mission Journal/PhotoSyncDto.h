//
//  PhotoSyncDto.h
//  Mission Journal
//
//  Created by Austin Aldrich on 4/19/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoSyncDto : NSObject <NSCoding>

@property (nonatomic, strong) NSString *photoID;
@property (nonatomic) BOOL isPrivate;


@end
