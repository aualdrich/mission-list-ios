//
//  TripQueries.h
//  MissionList
//
//  Created by Austin Aldrich on 7/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TripQueries : NSObject

+(NSMutableArray *) getTrips:(CBLQueryEnumerator *) rows;

@end
