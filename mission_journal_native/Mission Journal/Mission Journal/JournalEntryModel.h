//
//  JournalEntryModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/9/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentModel.h"

@interface JournalEntryModel : DocumentModel

@property (nonatomic, strong) NSString *entryDescription;
@property (nonatomic, strong) NSDate *entryDate;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *tripID;
@property (nonatomic, strong) NSMutableArray *tagIDs;
@property (nonatomic, strong) NSMutableArray *photoIDs;
@property (nonatomic) BOOL isPrivate;
@property (nonatomic, strong) NSString *entryDateFormatted;



+(NSMutableArray *) getJournalEntriesForTrip:(NSString *) tripID includeTeamPosts:(BOOL)includeTeamPosts;
+(JournalEntryModel *) getJournalEntry:(NSString *) journalEntryID;



-(BOOL) isJournalEntryOwner;

-(BOOL) photoExists:(NSString *) photoID;


-(NSURL *) getEntryURL;


@end
