//
//  JournalEntryModel.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class JournalEntryModel : DocumentModel {
    
    var entryDescription = ""
    var entryDate = NSDate()
    var userID = ""
    var tripID = ""
    var tagIDs = String[]()
    var photoIDs = String[]()
    var isPrivate = false
    
    
    init() {
        
    }
    
    class func getJournalEntriesForTrip(tripID :NSString, includeTeamPosts :Bool) -> NSMutableArray {
       return JournalEntryModelAdapter.getJournalEntriesForTrip(tripID, includeTeamPosts: includeTeamPosts)
    }
    
    func isJournalEntryOwner() -> Bool {
        var currentUser = UserService.currentUser
        return self.userID == currentUser.userID
    }

    override func getDocumentModel() -> NSMutableDictionary {
        var date = CBLJSON.JSONObjectWithDate(self.entryDate)
        var model = NSMutableDictionary()

        model.setValue(self.entryDescription, forKey: "entry_description")
        model.setValue(date, forKey: "entry_date")
        model.setValue(self.userID, forKey: "user_id")
        model.setValue(self.tripID, forKey: "trip_id")
        model.setValue("journal_entry", forKey: "doc_type")
        model.setValue(self.tagIDs, forKey: "tag_ids")
        model.setValue(self.photoIDs, forKey: "photo_ids")
        model.setValue(self.isPrivate ? "true" : "false", forKey: "is_private")

        return model
    }
    
    override func save() {
       self.saveOrUpdateWithId(self.documentID)
    }
    
    func photoExists(photoID :NSString) -> Bool {
       
        for p in self.photoIDs {
            if p == photoID {
                return true
            }
        }
        
        return false
    }
    
    func getEntryURL() -> NSURL {
       var url = "http://www.missionlist.org/journal_entry/show/\(self.documentID)"
        return NSURL.URLWithString(url) as NSURL
    }
    

}