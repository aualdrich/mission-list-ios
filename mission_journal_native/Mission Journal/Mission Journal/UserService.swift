//
//  UserService.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

struct UserService {
    
    static var _currentUser = BaseUserModel()
    
    static var currentUser :BaseUserModel {
        get {
        
            if(_currentUser.userID.isEmpty) {
               _currentUser = generateAppropriateUserModel()
            }
        
            return _currentUser
        
        }
    
        set(value) {
           _currentUser = value
        }
    }
    
    static func resetCurrentUser() {
        currentUser = generateAppropriateUserModel()
    }
    
    static func generateAppropriateUserModel() -> BaseUserModel {
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        var fbSession = FBSession.activeSession()
        var fbState = fbSession.state
    
        //Use FB Authentication
        
        
        if fbState.value == FBSessionStateOpen.value || fbState.value == FBSessionStateCreatedTokenLoaded.value || fbState.value == FBSessionStateCreatedOpening.value || fbState.value == FBSessionStateOpenTokenExtended.value  {
            var fbUserModel = FBUserModel()
            fbUserModel.userID = userDefaults.stringForKey("FBUserID")
            fbUserModel.accessToken = userDefaults.stringForKey("FBToken")
    
            openFBSession()
    
            return fbUserModel
    
    }
    
        //Use Anonymous Authentication
        else {
            var anonymousUserModel = AnonymousUserModel()
    
            var userDefaults = NSUserDefaults.standardUserDefaults()
            var userID = userDefaults.stringForKey("AnonymousUserID")
    
            if(userID != nil) {
                anonymousUserModel.userID = userID;
            }
    
            else {
                self.saveAnonymousUserDefaults()
                anonymousUserModel.userID = userDefaults.stringForKey("AnonymousUserID")
            }
    
            return anonymousUserModel;
        }
    
    }
    
    static func saveAnonymousUserDefaults() {
        var userDefaults = NSUserDefaults.standardUserDefaults()
    
        if(userDefaults.stringForKey("AnonymousUserID").isEmpty) {
            var newUserID = NSUUID.UUID().UUIDString()
            userDefaults.setObject(newUserID, forKey: "AnonymousUserID")
            userDefaults.synchronize()
        }
    
    }
    
    static func saveFBUserDefaults(user :AnyObject) {
        var fbToken = FBSession.activeSession().accessTokenData.accessToken
        var objUser = user as FBGraphUser
        var fbUserID = objUser.objectID
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setObject(fbToken, forKey: "FBToken")
        userDefaults.setObject(fbUserID, forKey: "FBUserID")
        
        userDefaults.synchronize()
    }
    
     static func openFBSession() {
        /*
        if !FBSession.activeSession().isOpen {
            FBSession.activeSession().openWithCompletionHandler({
                (session :FBSession!, state :FBSessionState, error :NSError) in
                    if state.value == FBSessionStateClosedLoginFailed.value {
                        var alertView = UIAlertView(title: "Error", message: error.localizedDescription(), delegate: nil, cancelButtonTitle: "OK")
                        alertView.show()
                    }
                } as FBSessionStateHandler)
        }
        */
    }
    
}