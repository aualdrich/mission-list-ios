//
//  File.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation


class TagModel : DocumentModel {
    var tagName = ""
    var userID = ""
    
    override func getDocumentModel() -> NSMutableDictionary {
        var model = NSMutableDictionary()
        
        model.setValue(self.tagName, forKey: "tag_name")
        model.setValue(self.userID, forKey: "user_id")
        model.setValue("tag", forKey: "doc_type")
        
        return model
    }
    
    class func getTags() -> TagModel[] {
        var tags = TagModel[]()
        var items = TagModelQueries.getTags()
        
        for tag :AnyObject in items {
            var tagItem = tag as TagModel
            tags.append(tagItem)
        }
        
        return tags
        
    }
    
    
    class func getDefaultTags() -> TagModel[] {
        var tags = TagModel[]()
        
        var items = TagModelQueries.getDefaultTags()
        
        for tag :AnyObject in items {
            var tagItem = tag as TagModel
            tags.append(tagItem)
        }
        
        return tags
        
    }
    
    class func getNewOrExistingTag(tagName :String) -> TagModel {
        var tags = self.getTags()
        
        for tag in tags {
            if(tag.tagName.lowercaseString == tagName.lowercaseString) {
                return tag
            }
        }
        
        var newTag = TagModel()
        newTag.tagName = tagName
        newTag.userID = UserService.currentUser.userID
        newTag.documentID = NSUUID.UUID().UUIDString()
        return newTag
        
    }
    
    class func saveTags(tags :TagModel[]) {
        for tag in tags {
            tag.save()
        }
    }
    
    override func save() {
        self.saveOrUpdateWithId(self.documentID)
    }
    
    func setTagProperties(document :CBLDocument) {
        self.documentID = document.documentID
        
        var tagName = document.properties.valueForKey("tag_name") as String
        var userID = document.properties.valueForKey("user_id") as String
        
        if(!tagName.isEmpty) {
            self.tagName = tagName
        }
        
        if(!userID.isEmpty) {
            self.userID = userID
        }
        
    }
    
    class func getTag(docID :String) -> TagModel {
        var db = DatabaseService.getDatabase()
        var doc = db.documentWithID(docID)
        
        var tag = TagModel()
        tag.setTagProperties(doc)
        
        return tag
    }
    
}
