//
//  TutorialViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 4/3/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TutorialViewController.h"
#import <EAIntroView.h>
#import <QuartzCore/QuartzCore.h>
#import "MissionsInMotionAppDelegate.h"
#import "LoginViewController.h"

@interface TutorialViewController () {
    UIView *rootView;
}

@end

@implementation TutorialViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
        rootView = self.view;
    
        EAIntroPage *appDescriptionPage = [EAIntroPage page];
        appDescriptionPage.title = @"Welcome!";
        appDescriptionPage.desc = @"Mission List makes it easy for you to capture and share moments from your mission trip, whether you're on or off the field.";
        appDescriptionPage.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Globe"]];
    
        EAIntroPage *connectionPage = [EAIntroPage page];
        connectionPage.title = @"Data Usage";
        connectionPage.desc = @"Remember to put your phone on airplane mode if your location doesn't have reliable or affordable internet access.  Mission List will still work even when you're offline.";
        connectionPage.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LowConnection"]];
    
    
        EAIntroView *introView = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[appDescriptionPage,connectionPage]];
        [introView showInView:rootView animateDuration:0.0];
        introView.bgImage = [UIImage imageNamed:@"WelcomeBG"];
        [introView.skipButton setHidden:YES];
        [introView setDelegate:self];
    
    
}

-(void) introDidFinish:(EAIntroView *)introView {
    MissionsInMotionAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate showLoginViewController];
}


@end
