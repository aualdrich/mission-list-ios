//
//  AddNewsletterViewController.m
//  Mission List
//
//  Created by Austin Aldrich on 7/23/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "AddNewsletterViewController.h"
#import "Branding.h"
#import "JournalEntryTableViewCell.h"
#import "TeamJournalEntryTableViewCell.h"
#import "MWPhotoBrowser.h"
#import "PhotoModel.h"
#import <UIImage+Resize.h>
#import "NewsletterHTMLRenderer.h"
#import "NewsletterModel.h"
#import "UserService.h"
#import "TextViewModalViewController.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"
#import "NewsletterService.h"
#import "ManageNewsletterRecipientsViewController.h"
#import <Toast+UIView.h>

#define kAddEntryButtonIndex 0
#define kAddPhotoButtonIndex 1
#define kCancelAddItemButtonIndex 2
#define kEntryCellHeight 134.0f
#define kPhotoCellHeight 100.0f
#define kFieldsSection 0
#define kItemsSection 1

@interface AddNewsletterViewController ()

@end

@implementation AddNewsletterViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.items = [[NSMutableArray alloc] init];
        self.tripPhotos = [[NSMutableArray alloc] init];
    }
    return self;
}


-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    UIBarButtonItem *addItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addItemClicked:)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *manageRecipientsItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Users"] style:UIBarButtonItemStylePlain target:self action:@selector(manageRecipients:)];
    
    NSMutableArray *items = [[NSMutableArray alloc] initWithObjects: flexibleSpace, addItem, flexibleSpace, manageRecipientsItem, flexibleSpace, nil];
    self.toolbarItems = items;
    [self.navigationController.toolbar setTintColor:[Branding getBlueColor]];
    self.navigationController.toolbarHidden = NO;

}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationController.toolbarHidden = YES;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"Add Newsletter";
    
    TextViewModalViewController *textViewModalViewController = [TextViewModalViewController new];
    textViewModalViewController.delegate = self;
    
    AddNewsletterViewController *me = self;
    
    self.nameCell = [BZGFormFieldCell new];
    self.nameCell.label.text = @"Name";
    self.nameCell.textField.delegate = self;
    [self.nameCell.textField setReturnKeyType:UIReturnKeyDone];
    
    self.messageCell = [BZGFormFieldCell new];
    self.messageCell.label.text = @"Message";
    self.messageCell.textField.delegate = self;
    self.messageCell.didBeginEditingBlock = ^void(BZGFormFieldCell *cell, NSString *text) {
        
        UINavigationController *modalNav = [[UINavigationController alloc] initWithRootViewController:textViewModalViewController];
        [me presentViewController:modalNav animated:YES completion:nil];
    };
    
    self.formFieldCells = [NSMutableArray arrayWithObjects:self.nameCell, self.messageCell, nil];
    self.formSection = kFieldsSection;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"more"] style:UIBarButtonItemStyleDone target:self action:@selector(moreActions:)];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TeamJournalEntryTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TeamCell"];
   
    self.selectedPhotos = [NSMutableArray new];
    self.photoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    self.photoBrowser.displaySelectionButtons = YES;
    self.photoBrowser.startOnGrid = YES;
    self.photoBrowser.displayActionButton = NO;
    
    self.manageRecipientsViewController = [ManageNewsletterRecipientsViewController new];
    
}

-(void) build:(TripModel *)trip :(NewsletterModel *)newsletter :(BOOL)editMode {
    
    self.editMode = editMode;
    
    self.tripModel = trip;
    [self buildTripPhotos];
    
    //default name
    if(self.messageCell.textField.text.length == 0) {
        self.messageCell.textField.text = [NSString stringWithFormat:@"%@ update", self.tripModel.tripName];
    }
    
    self.newsletter = newsletter;
    self.nameCell.textField.text = newsletter.name;
    self.messageCell.textField.text = newsletter.customMessage;
    
    if(editMode) {
        self.items = [self.newsletter getNewsletterItems];
    }
    
    
    if(!editMode) {
        self.newsletter.documentID = [[NSUUID UUID] UUIDString];
    }
    

    
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) addCustomToolbarItemWithButton:(UIButton *)button {
    
}


-(IBAction)addItemClicked:(id)sender {
    UIActionSheet *addItemActionSheet = [[UIActionSheet alloc] initWithTitle:@"Add an Item" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Add a journal entry", @"Add a photo", nil];
   
    [addItemActionSheet showInView:self.view];
    
}


-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == kAddEntryButtonIndex) {
        JournalEntryPickerTableViewController *picker = [[JournalEntryPickerTableViewController alloc] initWithStyle:UITableViewStylePlain];
        picker.delegate = self;
        picker.trip = self.tripModel;
        [self.navigationController pushViewController:picker animated:YES];
    }
    
    else if(buttonIndex == kAddPhotoButtonIndex) {
        UINavigationController *photoNavController = [[UINavigationController alloc] initWithRootViewController:self.photoBrowser];
        
        [self presentViewController:photoNavController animated:YES completion: nil];
    }
    
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return section == kFieldsSection ? self.formFieldCells.count : self.items.count;
}

-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        [self.items removeObjectAtIndex:indexPath.row];
        [self.tableView reloadData];
    }
    
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == kItemsSection ) {
    
    id item = [self.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[JournalEntryModel class]]) {
        return kEntryCellHeight;
    }
    
    return kPhotoCellHeight;
        
    }
    
    
    return 44.0f;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if(indexPath.section == kFieldsSection) {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    
    
    if(indexPath.section == kItemsSection) {
    
        id item = [self.items objectAtIndex:indexPath.row];
        static NSString *CellIdentifier = @"TeamCell";
        
        if([item isKindOfClass:[JournalEntryModel class]]) {
            JournalEntryModel *entry = (JournalEntryModel *) item;
            
            TeamJournalEntryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            [cell setPropertiesFromInput:entry.entryDate :entry.entryDescription userID:entry.userID];
            cell.accessoryType = UITableViewCellAccessoryNone;
            return cell;
        }
        
        else if([item isKindOfClass:[PhotoModel class]]) {
            PhotoModel *photo = [self.items objectAtIndex:indexPath.row];

            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Photo"];
            
            if(cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Photo"];
           
                UIImageView *imageView = [[UIImageView alloc] initWithImage:[photo getRemoteThumbnailImage]];
                
                imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
                [cell.contentView addSubview:imageView];
                imageView.center = CGPointMake(cell.contentView.bounds.size.width/2,cell.contentView.bounds.size.height/2);
            }
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            return cell;
        }
        
    }
    
    return nil;
    
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return section == kFieldsSection ? @"Newsletter Info" : @"Newsletter Items";
}

-(void) textViewDidEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
}

-(IBAction)preview:(id)sender {
    NewsletterHTMLRenderer *renderer = [[NewsletterHTMLRenderer alloc] init];
    NSString *html = [renderer buildHTMLForItems:self.items :self.tripModel :self.messageCell.textField.text];
    
    UIViewController *webViewController = [[UIViewController alloc] init];
    UINavigationController *webViewNavController = [[UINavigationController alloc] initWithRootViewController:webViewController];
    UIWebView *webView = [[UIWebView alloc] init];
    [webView loadHTMLString:html baseURL:nil];
    webViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissPreview:)];
    webViewController.navigationItem.title = @"Preview Newsletter";
    
    webViewController.view = webView;
    
    [self.navigationController presentViewController:webViewNavController animated:YES completion:nil];
    
}

-(void) saveNonFieldInfo {
    self.newsletter.tripName = self.tripModel.tripName;
    self.newsletter.tripID = self.tripModel.documentID;
    self.newsletter.userID = [[UserService currentUser] userID];
    
    self.newsletter.newsletterItemIDs = [[NSMutableArray alloc] init];
    self.newsletter.dateCreated = [NSDate date];
    
    for (id item in self.items) {
        if([item isKindOfClass:[JournalEntryModel class]]) {
            JournalEntryModel *entryModel = (JournalEntryModel *)item;
            [self.newsletter.newsletterItemIDs addObject:entryModel.documentID];
        }
        
        else if([item isKindOfClass:[PhotoModel class]]) {
            PhotoModel *photoModel = (PhotoModel *)item;
            [self.newsletter.newsletterItemIDs addObject:photoModel.documentID];
        }
        
    }
    
    [self.newsletter save];
    
    
 
}

-(IBAction)save:(id)sender {
    [self saveNonFieldInfo];
    
    self.newsletter.name = self.nameCell.textField.text;
    self.newsletter.customMessage = self.messageCell.textField.text;

    [self.newsletter save];
    
}

-(void) buildTripPhotos {
    
    self.tripPhotos = [[NSMutableArray alloc] init];
    self.tripPhotosSrc = [[NSMutableArray alloc] init];
    self.tripThumbPhotos = [[NSMutableArray alloc] init];
    
    NSMutableArray *entriesFromTrip = [JournalEntryModel getJournalEntriesForTrip:self.tripModel.documentID includeTeamPosts:YES];
    
    for (JournalEntryModel *entry in entriesFromTrip) {
        for (NSString *photoID in entry.photoIDs) {
            PhotoModel *photoModel = [PhotoModel getPhoto:photoID];
            [self.tripPhotosSrc addObject:photoModel];
        
            MWPhoto *thumbPhoto = [[MWPhoto alloc] initWithURL:[photoModel getRemoteImageThumbURL]];
            [self.tripThumbPhotos addObject:thumbPhoto];
        
            MWPhoto *photo = [[MWPhoto alloc] initWithURL:[photoModel getRemoteImageURL]];
            [self.tripPhotos addObject:photo];
        }
    }
}

-(void) journalEntryPicked:(JournalEntryModel *)journalEntry {
    [self.items addObject:journalEntry];
    [self.tableView reloadData];
}


- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.tripPhotos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    MWPhoto *photo = [self.tripPhotos objectAtIndex:index];
    return photo;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    MWPhoto *photo = [self.tripThumbPhotos objectAtIndex:index];
    return photo;
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.tableView reloadData];
  
}

//- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
//    return [[self.selectedPhotos objectAtIndex:index] boolValue];
//}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected {
    PhotoModel *photo = [self.tripPhotosSrc objectAtIndex:index];
    
    if(selected)
        [self.items addObject:photo];
    else
        [self.items removeObject:photo];
}


-(IBAction)dismissPreview:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) modalTextViewDoneEditing:(NSString *)text {
    self.messageCell.textField.text = text;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSString *) titleForModal {
    return @"Newsletter Message";
}

-(NSString *) initialText {
    return self.messageCell.textField.text;
}

-(void) textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

-(IBAction)moreActions:(id)sender {
    RNGridMenuItem *send = [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"Send"] title:@"Send Now" action:^{
        [self send:nil];
    }];
    
    RNGridMenuItem *preview = [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"Preview"] title:@"Preview" action:^{
        [self preview:nil];
    }];
    
    RNGridMenuItem *saveDraft = [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"save_draft"] title:@"Save Draft" action:^{
        [self save:nil];
    }];
    
    
    self.moreActionsMenu = [[RNGridMenu alloc] initWithItems:@[send, preview, saveDraft]];
    [self.moreActionsMenu showInViewController:self center:self.view.center];
}


-(IBAction)send:(id)sender {
    [self save:nil];
    [NewsletterService sendNewsletter:self.newsletter.documentID :self.tripModel];
    [self.view makeToast:@"Email sent!" duration:3.0 position:@"top"];
}

-(IBAction)manageRecipients:(id)sender {
    [self.manageRecipientsViewController build:self.tripModel];
    [self.navigationController pushViewController:self.manageRecipientsViewController animated:YES];
}

@end
