//
//  NewsletterItem.h
//  Mission List
//
//  Created by Austin Aldrich on 8/1/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "DocumentModel.h"

@interface NewsletterItem : DocumentModel

-(NSString *) itemType;

@end
