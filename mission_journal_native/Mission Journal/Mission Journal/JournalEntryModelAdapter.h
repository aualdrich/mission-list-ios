//
//  JournalEntryModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/9/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JournalEntryModelAdapter


+(NSMutableArray *) getJournalEntriesForTrip:(NSString *) tripID includeTeamPosts:(BOOL)includeTeamPosts;



@end
