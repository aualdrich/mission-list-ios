//
//  JournalEntryTemplateModel.m
//  Mission List
//
//  Created by Austin Aldrich on 7/28/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "JournalEntryNewsletterItem.h"
#import <CouchbaseLite/CouchbaseLite.h>

@implementation JournalEntryNewsletterItem


-(NSMutableDictionary *) getDocumentModel {
    NSMutableDictionary *dictionary = [super getDocumentModel];
    
    [dictionary setValue:[CBLJSON JSONObjectWithDate:self.entryDate] forKey:@"entry_date"];
    [dictionary setValue:self.entryDateFormatted forKey:@"entry_date_formatted"];
    [dictionary setValue:self.entryDescription forKey:@"entry_description"];
    [dictionary setValue:self.entryID forKey:@"entry_id"];
    
    return dictionary;
}

-(void) save {
    [self saveOrUpdateWithId:self.documentID];
}

+(JournalEntryNewsletterItem *) buildFromEntry:(JournalEntryModel *)entry {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE, MMM d yyyy"];
    
    JournalEntryNewsletterItem *model = [JournalEntryNewsletterItem new];
    model.entryDescription = entry.entryDescription;
    model.entryDate = entry.entryDate;
    model.entryDateFormatted = [dateFormatter stringFromDate:model.entryDate];
    model.entryID = entry.documentID;
    return model;
}

-(NSString *) itemType {
    return @"journal_entry";
}

@end
