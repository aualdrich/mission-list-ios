
//
//  AddJournalEntryViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/9/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "AddJournalEntryViewController.h"
#import "AddTagsViewController.h"
#import "JournalEntryModel.h"
#import "TagModel.h"
#import "UserService.h"
#import "PhotoModel.h"
#import "PhotoSyncDto.h"

#define kAddFromCameraIndex 0
#define kAddFromPhotoLibraryIndex 1
#define kSelectPhotoActionSheetTag 0
#define kDeletePhotoActionSheetTag 1




@interface AddJournalEntryViewController ()

@end

@implementation AddJournalEntryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"MMM d yyyy hh:mm a"];
        self.addTagsViewController = [AddTagsViewController new];
        self.photos = [@[] mutableCopy];
        self.mwPhotos = [[NSMutableArray alloc] init];
        self.mwThumbPhotos = [[NSMutableArray alloc] init];
        self.PLACEHOLDER_TEXT = @"What happened?";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"Add Entry";
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(saveEntry:)];
    
    self.navigationItem.rightBarButtonItem = saveButton;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.datePicker = [[UIDatePicker alloc] init];
    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    
    NSDate *date = [NSDate date];
    self.dateTextField.text = [self.dateFormatter stringFromDate:date];
    [self.datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    self.dateTextField.inputView = self.datePicker;
    
    [self.isPrivateSwitch setOn:self.journalEntry.isPrivate];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    
    tap.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tap];
    
    //create mode
    if(self.newEntryMode) {
        self.journalEntry = [[JournalEntryModel alloc] init];
        self.journalEntry.documentID = [[NSUUID UUID] UUIDString];
        self.journalEntry.tripID = self.trip.documentID;
        self.journalEntry.userID = [[UserService currentUser] userID];
        self.descriptionTextView.text = self.PLACEHOLDER_TEXT;

    }
    
    //edit mode
    else {
        [self populateFieldsFromEntry];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)saveEntry:(id)sender {
    
    if(self.descriptionTextView.text.length > 0) {
        
        self.journalEntry.entryDescription = self.descriptionTextView.text;
        self.journalEntry.entryDate = [self.dateFormatter dateFromString:self.dateTextField.text];
        self.journalEntry.isPrivate = [self.isPrivateSwitch isOn];
        
        [self saveTags];
        [self savePhotos];
        [self.journalEntry save];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    else {
        UIAlertView *invalidAlert = [[UIAlertView alloc] initWithTitle:@"Invalid Entry" message:@"Please enter a description of what happened." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [invalidAlert show];
        
    }
    
    
}

-(void) saveTags {
    
    NSArray *tags = self.addTagsViewController.tags;
    [TagModel saveTags:tags];
    
    [self.journalEntry.tagIDs removeAllObjects];

    for (TagModel *tag in tags) {
        [self.journalEntry.tagIDs addObject:tag.documentID];
    }
}

-(void) savePhotos {
    
    NSMutableArray *unsyncedPhotos = [PhotoModel loadUnsyncedPhotos];
    
    for (PhotoModel *photo in self.photos) {
        
        if(![self.journalEntry photoExists:photo.documentID]) {
            [self.journalEntry.photoIDs addObject:photo.documentID];
        }
        
        photo.isPrivate = self.journalEntry.isPrivate;
        [photo save];
        
        PhotoSyncDto *syncDto = [PhotoSyncDto new];
        syncDto.photoID = photo.documentID;
        syncDto.isPrivate = photo.isPrivate;
        [unsyncedPhotos addObject:syncDto];
        
        [photo saveLocalPhoto:photo.displayImage];
        
    }
    
    [PhotoModel saveUnsyncedPhotos:unsyncedPhotos];
    
    [PhotoModel syncRemotePhotosInQueue:@""];
    

}


-(IBAction)tagsSelected:(id)sender {
    [self.navigationController pushViewController:self.addTagsViewController animated:YES];
     
}

-(IBAction)shareSelected:(id)sender {
    [FBDialogs presentShareDialogWithLink:[self.journalEntry getEntryURL] name:self.trip.tripName handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
        if(error) {
            NSLog(@"Error: %@", error.description);
        } else {
            NSLog(@"Success!");
        }
        
    }];
}

-(IBAction)dateChanged:(id)sender {
    self.dateTextField.text = [self.dateFormatter stringFromDate:self.datePicker.date];
}


-(IBAction)dismissKeyboard:(id)sender {
    [self.descriptionTextView resignFirstResponder];
}

-(void) textViewDidBeginEditing:(UITextView *)textView {
    if([textView.text isEqualToString: self.PLACEHOLDER_TEXT])
        textView.text = @"";
}


-(void) populateFieldsFromEntry {
    
    self.descriptionTextView.text = self.journalEntry.entryDescription;
    self.dateTextField.text = [self.dateFormatter stringFromDate:self.journalEntry.entryDate];
    self.datePicker.date = self.newEntryMode ? [NSDate date] : self.journalEntry.entryDate;
    

    for (NSString *tagID in self.journalEntry.tagIDs) {
        TagModel *tag = [TagModel getTag:tagID];
        [self.addTagsViewController.tags addObject:tag];
    }
    
    for (NSString *photoID in self.journalEntry.photoIDs) {
        PhotoModel *photo = [PhotoModel getPhoto:photoID];
        [self.photos addObject:photo];
    }
    
    
}


-(IBAction) photosSelected:(id)sender {
    
    [self setPhotosFromModel:self.photos];
    
    self.photoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    self.photoBrowser.enableGrid = YES;
    self.photoBrowser.startOnGrid = YES;
    self.photoBrowser.displayActionButton = YES;
    self.photoBrowser.alwaysShowControls = YES;
    self.photoBrowser.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addPhotoClicked:)];
    
    UINavigationController *photoNavController = [[UINavigationController alloc] initWithRootViewController:self.photoBrowser];
    
    [self presentViewController:photoNavController animated:YES completion:nil];
    
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.mwPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.mwPhotos.count)
        return [self.mwPhotos objectAtIndex:index];
    return nil;
}




- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if(index < self.mwThumbPhotos.count) {
        return [self.mwPhotos objectAtIndex:index];
    }
    
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index {
    
    UIActionSheet *deletePhotoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles:nil, nil];
    
    deletePhotoActionSheet.tag = kDeletePhotoActionSheetTag;
    
    [deletePhotoActionSheet showInView:self.photoBrowser.navigationController.view];
    
}


-(void) setPhotosFromModel:(NSArray *)model {
    [self.mwPhotos removeAllObjects];
    [self.mwThumbPhotos removeAllObjects];
    
    for (PhotoModel *p in model) {
        [self.mwPhotos addObject:[MWPhoto photoWithImage:[p getLocalImage]]];
        [self.mwThumbPhotos addObject:[MWPhoto photoWithImage:[p getLocalImage]]];
    }
}

-(IBAction)addPhotoClicked:(id)sender {
    UIActionSheet *addPhotoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Add from Camera", @"Choose from Photo Library", nil];
    
    addPhotoActionSheet.tag = kSelectPhotoActionSheetTag;
    
    [addPhotoActionSheet showInView:self.photoBrowser.navigationController.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(actionSheet.tag == kSelectPhotoActionSheetTag && (buttonIndex == kAddFromCameraIndex || buttonIndex == kAddFromPhotoLibraryIndex)) {
        [self pickImages:buttonIndex];
    }
    
    else if(actionSheet.tag == kDeletePhotoActionSheetTag) {
        
        //delete photo
        if(buttonIndex == 0) {
            [self deletePhoto];
        }
        
        
    }
    
}

-(void) pickImages:(NSInteger) buttonIndex {
    
    UIImagePickerController *imagePicker =
    [[UIImagePickerController alloc] init];
    
    imagePicker.delegate = self;
    
    
    if(buttonIndex == kAddFromCameraIndex) {
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
    }
    
    else if(buttonIndex == kAddFromPhotoLibraryIndex) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    imagePicker.allowsEditing = YES;
    [self.photoBrowser.navigationController presentViewController:imagePicker animated:YES completion:nil];
    
}

-(void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    // Code here to work with media
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    PhotoModel *photoModel = [[PhotoModel alloc] init];
    photoModel.documentID = [[NSUUID UUID] UUIDString];
    photoModel.userID = [[UserService currentUser] userID];
    photoModel.displayImage = image;
    [photoModel saveLocalPhoto:image];
    
    [self.photos addObject:photoModel];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

-(void)imagePickerControllerDidCancel: (UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}
                             
                             
-(void)deletePhoto {
    
    PhotoModel *photoModel = [self.photos objectAtIndex:self.photoBrowser.currentIndex];
    
    if(!self.newEntryMode) {
        [self.journalEntry.photoIDs removeObject:photoModel.documentID];
    }
    
    [self.photos removeObjectAtIndex:self.photoBrowser.currentIndex];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}







@end
