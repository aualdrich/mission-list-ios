//
//  JournalEntryModel.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/9/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "JournalEntryModel.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"
#import "TripModel.h"
#import "PhotoModel.h"
#import "UserService.h"

@implementation JournalEntryModel


-(JournalEntryModel *) init {
    
    self.tagIDs = [[NSMutableArray alloc] init];
    self.photoIDs = [[NSMutableArray alloc] init];
    
   
    
    return self;
}


-(NSMutableDictionary *) getDocumentModel {
    return [@{@"entry_description": self.entryDescription,
              @"entry_date": [CBLJSON JSONObjectWithDate:self.entryDate],
              @"user_id": self.userID,
              @"trip_id": self.tripID,
              @"doc_type": @"journal_entry",
              @"tag_ids": self.tagIDs,
              @"photo_ids": self.photoIDs,
              @"is_private": self.isPrivate ? @"true" : @"false"
              } mutableCopy];
}


+(NSMutableArray *) getJournalEntriesForTrip:(NSString *)tripID includeTeamPosts:(BOOL) includeTeamPosts {
    
    NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    CBLDatabase *db = [DatabaseService getDatabase];
    CBLQuery *query = [[db viewNamed:@"journal_entries_for_trip"] createQuery];
    
    NSError *error;
    CBLQueryEnumerator *rowEnum = [query run:&error];
    
    for(CBLQueryRow *row in rowEnum) {
        NSString *rowKey = row.key;
        
        if([rowKey.lowercaseString isEqualToString:tripID.lowercaseString]) {
            JournalEntryModel *j = [self buildJournalEntry:row.document];
            
            //non-team posts means you effectively only want your own posts ("Me")
            if(!includeTeamPosts) {
                
                NSString *currentUserID = [[UserService currentUser] userID];
                
                if([j.userID isEqualToString:currentUserID]) {
                    [entries addObject:j];
                }
            }
            
            //team posts includes everyone
            else {
                
                if(!j.isPrivate)
                    [entries addObject:j];
            }
            
        }
    }
    
    return entries;
    
}


-(BOOL) isJournalEntryOwner {
    NSString *currentUserID = [[UserService currentUser] userID];
    return [self.userID isEqualToString:currentUserID];    
}



-(void) save {
    [self saveOrUpdateWithId:self.documentID];
}


-(BOOL) photoExists:(NSString *)photoID {
    
    for (NSString *p in self.photoIDs) {
        if([photoID isEqualToString:p])
            return YES;
    }
    
    return NO;
    
}

-(NSURL *) getEntryURL {
     NSString *url = [NSString stringWithFormat:@"http://www.missionlist.org/journal_entry/show/%@", self.documentID];
     return [NSURL URLWithString:url];
}

+(JournalEntryModel *) getJournalEntry:(NSString *)journalEntryID {
    CBLDatabase *db = [DatabaseService getDatabase];
    CBLDocument *doc = [db documentWithID:journalEntryID];
    
    return [self buildJournalEntry:doc];
}

+(JournalEntryModel *) buildJournalEntry:(CBLDocument *)document {
   
    JournalEntryModel *j = [[JournalEntryModel alloc] init];
    j.documentID = document.documentID;
    j.tripID = [document.properties valueForKey:@"trip_id"];
    
    NSString *description = [document.properties valueForKey:@"entry_description"];
    j.entryDescription = description;
    
    NSString *entryDateJSON = [document.properties valueForKey:@"entry_date"];
    NSDate *entryDate = [CBLJSON dateWithJSONObject:entryDateJSON];
    j.entryDate = entryDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE, MMM dd yyyy"];
    
    j.entryDateFormatted = [dateFormatter stringFromDate:entryDate];
    
    
    NSString *userID = [document.properties valueForKey:@"user_id"];
    j.userID = userID;
    
    NSArray *tags = [document.properties valueForKey:@"tag_ids"];
    if(tags != nil)
        j.tagIDs = [NSMutableArray arrayWithArray:tags];
    
    
    NSArray *photos = [document.properties valueForKey:@"photo_ids"];
    if(photos != nil)
        j.photoIDs = [NSMutableArray arrayWithArray:photos];
    
    id isPrivateTrip = [document.properties valueForKey:@"is_private"];
    
    //this is an awful hack to deal with the way the app stores TRUE as a string but
    //Rails stores it as an actual bit field.
    if ([isPrivateTrip isKindOfClass:[NSNumber class]]) {
        
        NSNumber *numVal = isPrivateTrip;
        j.isPrivate = [numVal isEqual:@1];
    }
    
    else if([isPrivateTrip isEqualToString:@"true"]) {
        j.isPrivate = true;
    }
    
    return j;
    
    
}




@end
