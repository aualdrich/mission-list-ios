//
//  FBUserModel.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "FBUserModel.h"
#import "DatabaseService.h"
#import <FacebookSDK/FacebookSDK.h>

@implementation FBUserModel


-(id) init {
    self = [super init];
    
    if(self) {
        self.tripsFollowed = [@[] mutableCopy];
    }
    
    return self;
}

-(NSMutableDictionary *) getDocumentModel {
    NSMutableDictionary *model = [super getDocumentModel];
    [model setValue:@"fb_user" forKey:@"doc_type"];
    
    if(self.email.length > 0)
        [model setValue:self.email forKey:@"email"];
    
    if(self.firstName.length > 0)
        [model setValue:self.firstName forKey:@"first_name"];
    
    if(self.lastName.length > 0)
        [model setValue:self.lastName forKey:@"last_name"];
    
    if(self.photoURL.length > 0)
        [model setValue:self.photoURL forKey:@"photo_url"];
    
    if(self.tripsFollowed != nil)
        [model setValue:self.tripsFollowed forKey:@"trips_followed"];
    
    if(self.welcomeEmailSent.length > 0)
        [model setValue:self.welcomeEmailSent forKey:@"welcome_email_sent"];
    
    return model;
}


-(void) setPropertiesFromDb:(NSString *)userID {
    NSDictionary *user = [[DatabaseService getDatabase] documentWithID:userID].properties;
    
    if(user != nil) {
        NSString *dateAdded = [user valueForKey:@"date_added"];
        
        if(dateAdded.length > 0) {
            self.dateAdded = [CBLJSON dateWithJSONObject:dateAdded];
        }
        
        self.email = [user valueForKey:@"email"];
        self.firstName = [user valueForKey:@"first_name"];
        self.lastName = [user valueForKey:@"last_name"];
        self.photoURL = [user valueForKey:@"photo_url"];
        self.tripsFollowed = [[user valueForKey:@"trips_followed"] mutableCopy];
        self.welcomeEmailSent = [user valueForKey:@"welcome_email_sent"];
        
    }
}



@end
