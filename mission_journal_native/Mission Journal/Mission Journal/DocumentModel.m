//
//  DocumentModel.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/7/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "DocumentModel.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"

@implementation DocumentModel

-(DocumentModel *) init {
    
    return self;
    
}

-(NSMutableDictionary *) getDocumentModel {
    return [NSMutableDictionary dictionary];
}

-(void) save {
    CBLDatabase *database = [DatabaseService getDatabase];
    CBLDocument *doc = [database createDocument];
    [doc putProperties:self.getDocumentModel error:nil];
}

-(void) deleteDoc {
    CBLDatabase *database = [DatabaseService getDatabase];
    CBLDocument *doc = [database existingDocumentWithID:self.documentID];
    NSError *error;
    [doc deleteDocument:&error];
}

-(void) saveOrUpdateWithId:(NSString *)docID {
    CBLDatabase *database = [DatabaseService getDatabase];
    
    CBLDocument *doc = [database existingDocumentWithID:docID];
    BOOL isUpdate = YES;
    
    if(doc == nil) {
        doc = [database documentWithID:docID];
        isUpdate = NO;
    }
    
    
    NSError *error;
    NSMutableDictionary *docModel = [self getDocumentModel];
    
    if(isUpdate) {
        NSString *rev = doc.currentRevision.revisionID;
        [docModel setValue:rev forKey:@"_rev"];
    }
    
    
    [doc putProperties:docModel error:&error];
    
    
}

-(void) update {
    CBLDatabase *database = [DatabaseService getDatabase];
    CBLDocument *doc = [database existingDocumentWithID:self.documentID];
    NSError *error;
    
    NSMutableDictionary *docModel = [self getDocumentModel];
    NSString *rev = doc.currentRevision.revisionID;
    [docModel setValue:rev forKey:@"_rev"];
    
    [doc putProperties:docModel error:&error];
}



@end
