//
//  NewsletterHTMLRenderer.h
//  Mission List
//
//  Created by Austin Aldrich on 7/28/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TripModel.h"
#import "JournalEntryModel.h"
#import "PhotoModel.h"
#import <UIImage+Resize.h>
#import <MWPhotoBrowser.h>

@interface NewsletterHTMLRenderer : NSObject


-(NSString *) buildHTMLForJournalEntry:(JournalEntryModel *)entry;
-(NSString *) buildHTMLForPhoto:(PhotoModel *)photo;
-(NSString *) buildHTMLForItems:(NSMutableArray *)items :(TripModel *) trip :(NSString *) customMessage;

@end
