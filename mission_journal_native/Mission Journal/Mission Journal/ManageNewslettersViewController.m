//
//  ManageNewslettersViewController.m
//  Mission List
//
//  Created by Austin Aldrich on 7/23/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "ManageNewslettersViewController.h"
#import "NewsletterModel.h"
#import "ManageNewsletterRecipientsViewController.h"
#import "UserService.h"

@interface ManageNewslettersViewController ()

@end

@implementation ManageNewslettersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.toolbarHidden = YES;
    
     [self buildNewsletters];
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Newsletters";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewsletter:)];
    
    self.manageNewsletterRecipientsViewController = [ManageNewsletterRecipientsViewController new];

    
}


-(void) buildNewsletters {
    self.newsletters = [NewsletterModel getNewsletters:self.trip.documentID];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)addNewsletter:(id)sender {
    AddNewsletterViewController *addNewsletterViewController = [[AddNewsletterViewController alloc] initWithStyle:UITableViewStylePlain];
    NewsletterModel *newsletter = [NewsletterModel new];
    [addNewsletterViewController build:self.trip :newsletter :NO];
    [self.navigationController pushViewController:addNewsletterViewController animated:YES];
}

-(IBAction)manageRecipientsClicked:(id)sender {
    [self.manageNewsletterRecipientsViewController build:self.trip];
    [self.navigationController pushViewController:self.manageNewsletterRecipientsViewController animated:YES];
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsletterModel *newsletter = [self.newsletters objectAtIndex:indexPath.row];
    
    AddNewsletterViewController *addNewsletterViewController = [[AddNewsletterViewController alloc] initWithStyle:UITableViewStylePlain];
    [addNewsletterViewController build:self.trip :newsletter :YES];
    [self.navigationController pushViewController:addNewsletterViewController animated:YES];
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsletters.count;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsletterModel *newsletter = [self.newsletters objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
        cell.detailTextLabel.textColor = [Branding getDarkGreyColor];
        cell.textLabel.textColor = [Branding getBlueColor];
    }
    
    cell.textLabel.text = newsletter.name;
    cell.detailTextLabel.text = newsletter.customMessage;

    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0f;
}


-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NewsletterModel *newsletter = [self.newsletters objectAtIndex:indexPath.row];
    
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        [newsletter deleteDoc];
        [self.newsletters removeObjectAtIndex:indexPath.row];
        [self.tableView reloadData];
    }
    
}




@end
