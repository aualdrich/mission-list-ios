//
//  TagModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/11/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "DocumentModel.h"

@interface TagModel : DocumentModel

@property (nonatomic, strong) NSString *tagName;
@property (nonatomic, strong) NSString *userID;

+(NSMutableArray *) getTags;
+(TagModel *) getNewOrExistingTag:(NSString *) tagName;
+(TagModel *) getTag:(NSString *) docID;
+(void) saveTags:(NSArray *)tags;

@end
