//
//  APIService.m
//  Mission List
//
//  Created by Austin Aldrich on 8/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "APIService.h"

@implementation APIService

+(AFHTTPRequestOperationManager *) buildRequestManager {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager setCredential:[NSURLCredential credentialWithUser:@"missionsinmotion" password:@"M1m34348" persistence:NSURLCredentialPersistenceForSession]];
    
    return manager;
}


@end
