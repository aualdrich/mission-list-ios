//
//  NewsletterRecipientModel.h
//  Mission List
//
//  Created by Austin Aldrich on 8/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentModel.h"

@interface NewsletterRecipientModel : DocumentModel

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *email;

+(NewsletterRecipientModel *) getByID:(NSString *)documentID;

@end
