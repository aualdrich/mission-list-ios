//
//  AddNewsletterRecipientViewController.h
//  Mission List
//
//  Created by Austin Aldrich on 8/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsletterRecipientModel.h"
#import <BZGFormViewController.h>
#import <BZGFormFieldCell.h>
#import "NewsletterModel.h"

@protocol AddNewsletterRecipientDelegate <NSObject>

-(void) newsletterRecipientSaved:(NewsletterRecipientModel *)recipient;

@end

@interface AddNewsletterRecipientViewController : BZGFormViewController

@property (nonatomic, strong) NewsletterRecipientModel *recipient;
@property (nonatomic, strong) BZGFormFieldCell *firstNameCell;
@property (nonatomic, strong) BZGFormFieldCell *lastNameCell;
@property (nonatomic, strong) BZGFormFieldCell *emailCell;
@property (nonatomic, assign) BOOL editMode;
@property (nonatomic, weak) id <AddNewsletterRecipientDelegate> delegate;

-(void) build:(NewsletterRecipientModel *)recipient :(BOOL)editMode;


@end
