//
//  TripNewsletterRecipientModel.h
//  Mission List
//
//  Created by Austin Aldrich on 8/20/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "DocumentModel.h"

@interface TripNewsletterRecipientsModel : DocumentModel

@property (nonatomic, strong) NSString *tripID;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSMutableArray *recipientIDs;


+(TripNewsletterRecipientsModel *) getTripNewsletterRecipients:(NSString *)docID;

@end
