//
//  PhotoModel.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/14/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "PhotoModel.h"
#import "DatabaseService.h"
#import "UserService.h"
#import "RemoteStorageService.h"
#import "PhotoSyncDto.h"
#import <UIImage+Resize.h>

@implementation PhotoModel

-(void) save {
    [self saveOrUpdateWithId:self.documentID];
}

-(NSMutableDictionary *) getDocumentModel {
    
    NSString *userID = [[UserService currentUser] userID];
    
    return [@{@"doc_type": @"photo",
              @"user_id": userID
              } mutableCopy];
}

+(PhotoModel *) getPhoto:(NSString *)photoID {
    
    PhotoModel *model = [[PhotoModel alloc] init];
    
    CBLDatabase *db = [DatabaseService getDatabase];
    CBLDocument *doc = [db existingDocumentWithID:photoID];
    
    model.documentID = doc.documentID;
    model.userID = [doc.properties objectForKey:@"user_id"];
    model.thumbnailSrc = [[model getRemoteImageThumbURL] absoluteString];
    model.photoSrc = [[model getRemoteImageURL] absoluteString];
    
    return model;
}

-(UIImage *) getLocalImage {
    
    NSString *path = [self getPhotoPath];
    return [UIImage imageWithContentsOfFile:path];
    
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(NSString *) getPhotoPath {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@", self.documentID]];
    
    return path;
}

-(void) saveLocalPhoto:(UIImage *) image {

    [UIImageJPEGRepresentation(image, 1.0) writeToFile:[self getPhotoPath] atomically:YES];
}

-(BOOL) photoExists {
    
    BOOL exists = NO;
    
    RemoteStorageService *remoteStorageService = [RemoteStorageService getRemoteStorageService];
    AmazonS3Client *client = remoteStorageService.client;
    
    S3GetObjectMetadataRequest *request = [[S3GetObjectMetadataRequest alloc] initWithKey:[self getPhotoKey] withBucket:remoteStorageService.bucketName];
    
    
    S3GetObjectMetadataResponse *response = [client getObjectMetadata:request];
        
    if(!response.error)
        return YES;


    return exists;
    
    
}

-(NSString *) getPhotoKey {
    NSString *photoKey = [NSString stringWithFormat:@"%@.jpg", self.documentID];
    return photoKey;
}

-(BOOL) saveRemotePhoto:(UIImage *) image {

    NSLog(@"Saving remote photo: %@", self.documentID);
    NSString *photoKey = [self getPhotoKey];
    S3Response *response = [self saveToS3:image :photoKey];
    
    NSLog(@"Saving thumbnail photo");
    UIImage *resized = [image resizedImageToFitInSize:(CGSizeMake(100.0, 100.0)) scaleIfSmaller:YES];
    NSString *newPhotoKey = [NSString stringWithFormat:@"thumbnails/%@", photoKey];
    S3Response *thumbnailResponse = [self saveToS3:resized :newPhotoKey];
    
    BOOL isSuccessful = YES;
    
    if(response.error != nil)
        isSuccessful = NO;
    
    else if(thumbnailResponse.error != nil)
        isSuccessful = NO;
    
    return isSuccessful;
    
}

-(S3Response *) saveToS3:(UIImage *)image :(NSString *)photoKey {
    
    RemoteStorageService *remoteStorageService = [RemoteStorageService getRemoteStorageService];
    AmazonS3Client *client = remoteStorageService.client;
    
    S3PutObjectRequest *request = [[S3PutObjectRequest alloc] initWithKey:photoKey inBucket:remoteStorageService.bucketName];
    request.contentType = @"image/jpeg";
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    request.data = imageData;
    
    if(self.isPrivate)
        request.cannedACL = [S3CannedACL private];
    else
        request.cannedACL = [S3CannedACL publicRead];
    
    S3PutObjectResponse *response = [client putObject:request];
    
    return response;

}

-(UIImage *) getRemoteImage {
    RemoteStorageService *remoteStorageService = [RemoteStorageService getRemoteStorageService];
    AmazonS3Client *client = remoteStorageService.client;
    
    NSString *photoKey = [self getPhotoKey];
    S3GetObjectRequest *request = [[S3GetObjectRequest alloc] initWithKey:photoKey withBucket:remoteStorageService.bucketName];
    
    @try {
        S3GetObjectResponse *response = [client getObject:request];
        NSData *data = [response body];
        UIImage *img = [UIImage imageWithData:data];
        return img;
    }
    @catch (NSException *exception) {
    }
    @finally {
        
    }

    return nil;
    
    
    
}

-(UIImage *) getRemoteThumbnailImage {
    return [UIImage imageWithData:[NSData dataWithContentsOfURL:[self getRemoteImageThumbURL]]];
}

-(NSData *) getRemoteImageData {
    RemoteStorageService *remoteStorageService = [RemoteStorageService getRemoteStorageService];
    AmazonS3Client *client = remoteStorageService.client;
    
    NSString *photoKey = [self getPhotoKey];
    S3GetObjectRequest *request = [[S3GetObjectRequest alloc] initWithKey:photoKey withBucket:remoteStorageService.bucketName];
    
    @try {
        S3GetObjectResponse *response = [client getObject:request];
        NSData *data = [response body];
        return data;
    }
    @catch (NSException *exception) {
    }
    @finally {
        
    }
    
    return nil;
    
}

+(void) syncRemotePhotosInQueue:(NSString *) arg {

    NSMutableArray *unsyncedPhotos = [self loadUnsyncedPhotos];
    
    NSMutableArray *photos = [@[] mutableCopy];
    
    for (PhotoSyncDto *dto in unsyncedPhotos) {
        PhotoModel *photo = [PhotoModel getPhoto:dto.photoID];
        photo.isPrivate = dto.isPrivate;
        
        [photos addObject:photo];
    }
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableArray *unsuccessfulSyncs = [@[] mutableCopy];
        NSLock *lock = [[NSLock alloc] init];
        
        for (PhotoModel *p in photos) {
            
            UIImage *localImage = [p getLocalImage];
            BOOL successful = [p saveRemotePhoto:localImage];
            
            if(!successful) {
                [lock lock];
                PhotoSyncDto *failureDto = [[PhotoSyncDto alloc] init];
                failureDto.photoID = p.documentID;
                failureDto.isPrivate = p.isPrivate;
                [unsuccessfulSyncs addObject:failureDto];
                [lock unlock];
            }
            
        }
        
        [unsyncedPhotos removeAllObjects];
        [unsyncedPhotos addObjectsFromArray:unsuccessfulSyncs];
        
        [self saveUnsyncedPhotos:unsyncedPhotos];
        
    });

    
}

+ (NSString *)documentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return paths[0];
}

+ (NSString *)unsycedPhotosPath
{
    return [[self documentsDirectory] stringByAppendingPathComponent:@"unsynced_photos.plist"];
}


+ (NSMutableArray *)loadUnsyncedPhotos
{
    NSMutableArray *array;
    NSString *path = [self unsycedPhotosPath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSData *data = [[NSData alloc] initWithContentsOfFile:path];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        array = [unarchiver decodeObjectForKey:@"unsynched_photos"];
        [unarchiver finishDecoding];
    } else {
        array = [[NSMutableArray alloc] initWithCapacity:20];
    }
    return array;
}

+ (void)saveUnsyncedPhotos:(NSMutableArray *)array
{
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:array forKey:@"unsynched_photos"];
    [archiver finishEncoding];
    [data writeToFile:[self unsycedPhotosPath] atomically:YES];
}

-(NSURL *) getRemoteImageURL {
    NSString *stringURL = [NSString stringWithFormat:@"http://s3.amazonaws.com/missionjournal/%@.jpg", self.documentID];
    return [NSURL URLWithString:stringURL];
}

-(NSURL *) getRemoteImageThumbURL {
    NSString *stringURL = [NSString stringWithFormat:@"http://s3.amazonaws.com/missionjournal/thumbnails/%@.jpg", self.documentID];
    return [NSURL URLWithString:stringURL];
}



@end
