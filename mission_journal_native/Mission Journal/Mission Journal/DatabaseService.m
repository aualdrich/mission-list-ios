//
//  DatabaseService.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/7/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "DatabaseService.h"
#import "UserService.h"

#define kRemoteURL @"https://missionsinmotion:M1m34348@missionsinmotion.cloudant.com/journal"


@implementation DatabaseService

+(CBLDatabase *)getDatabase;

{
    
    static CBLDatabase *database;
    
    @synchronized(self) {
        if(!database) {
            CBLManager *manager = [CBLManager sharedInstance];
            NSError *error;
            database = [manager databaseNamed:@"mission_journal" error:&error];
            
            NSURL *remoteURL = [NSURL URLWithString:kRemoteURL];

            CBLReplication *push = [database createPushReplication:remoteURL];
            push.continuous = YES;
            [push start];
            
            CBLReplication *pull = [database createPullReplication:remoteURL];
            pull.continuous = YES;
            [pull start];
            
            [self generateViews];
        }
        
        return database;
    }
    
}


+(NSString *) getRemoteDatabaseURL {
    return kRemoteURL;
}


+(void) generateViews {
    
    CBLDatabase *database = [self getDatabase];
    
    
        [[database viewNamed:@"trip_list"] setMapBlock:^(NSDictionary *doc, CBLMapEmitBlock emit) {
            id docType = [doc objectForKey:@"doc_type"];
            id userID = [doc objectForKey:@"user_id"];
            id docID = [doc objectForKey:@"id"];
            if(docType != nil && [docType isEqualToString:@"trip"]) {
                if(userID != nil) {
                    emit(docID, doc);
                }
            }
        } version:@"1.0"];
    
    
    [[database viewNamed:@"tag_list"] setMapBlock:^(NSDictionary *doc, CBLMapEmitBlock emit) {
        id docType = [doc objectForKey:@"doc_type"];
        id userID = [doc objectForKey:@"user_id"];
        id docID = [doc objectForKey:@"id"];
        if(docType != nil && [docType isEqualToString:@"tag"]) {
            if(userID != nil) {
                emit(docID, doc);
            }
        }
        
    } version:@"1.0"];
    

    [[database viewNamed:@"journal_entries_for_trip"] setMapBlock:^(NSDictionary *doc, CBLMapEmitBlock emit) {
        id docType = [doc objectForKey:@"doc_type"];
        id userID = [doc objectForKey:@"user_id"];
        id tripID = [doc objectForKey:@"trip_id"];
        if(docType != nil && [docType isEqualToString:@"journal_entry"]) {
            if(userID != nil) {
                
                if(tripID != nil)
                    emit(tripID, doc);
            }
        }
        
    } version:@"1.0"];
    
    [[database viewNamed:@"unsynced_user_photos"] setMapBlock:^(NSDictionary *doc, CBLMapEmitBlock emit) {
        id docType = [doc objectForKey:@"doc_type"];
        id userID = [doc objectForKey:@"user_id"];
        id docID = [doc objectForKey:@"_id"];
        id imageSavedRemotely = [doc objectForKey:@"image_saved_remotely"];
        if(docType != nil && [docType isEqualToString:@"photo"]) {
            if(userID != nil && [userID isEqualToString:[[UserService currentUser] userID]]) {
                
                if(imageSavedRemotely != nil && [imageSavedRemotely isEqualToString:@"false"]) {
                    emit(docID, doc);
                }
                

            }
        }

        
    } version:@"1.0"];
    
    [[database viewNamed:@"newsletters"] setMapBlock:^(NSDictionary *doc, CBLMapEmitBlock emit) {
        id docType = [doc objectForKey:@"doc_type"];
        id userID = [doc objectForKey:@"user_id"];
        id docID = [doc objectForKey:@"_id"];
        if(docType != nil && [docType isEqualToString:@"newsletter"]) {
            if(userID != nil && [userID isEqualToString:[[UserService currentUser] userID]]) {
                emit(docID, doc);
            }
        }
        
        
    } version:@"1.0"];
    
    
}




@end
