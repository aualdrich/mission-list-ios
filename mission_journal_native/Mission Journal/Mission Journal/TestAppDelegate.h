//
//  TestAppDelegate.h
//  Mission Journal
//
//  Created by Austin Aldrich on 4/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestAppDelegate : NSObject<UIApplicationDelegate>
@end