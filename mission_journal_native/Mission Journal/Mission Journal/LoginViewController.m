//
//  LoginViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "LoginViewController.h"
#import <FacebookSDK/FBSession.h>
#import "MissionsInMotionAppDelegate.h"
#import "TripListViewController.h"
#import "MenuViewController.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "UserService.h"
#import "WelcomeService.h"

@interface LoginViewController () {
    
}
@end

@implementation LoginViewController





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = @"Login";
        
        loginView = [[FBLoginView alloc] initWithReadPermissions:@[@"basic_info", @"email", @"user_likes", @"friends"]];
        
        loginView.delegate = self;

        
            
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"WelcomeBG"]];

    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user {
    
    [UserService saveFBUserDefaults:user];
    [WelcomeService sendWelcomeEmail];
    [self finishFirstLaunch];
}

- (void) loginView:(FBLoginView *)loginView handleError:(NSError *)error {

    NSString *alertMessage, *alertTitle;
    
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures since that happen outside of the app.
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
   



}



-(IBAction)continueWithoutLogin:(id)sender {
    
    [UserService saveAnonymousUserDefaults];
    
    [self finishFirstLaunch];
}



-(void) finishFirstLaunch {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:YES forKey:@"HasLaunched"];
    [userDefaults synchronize];
    
    MissionsInMotionAppDelegate *appDelegate = (MissionsInMotionAppDelegate *)[UIApplication sharedApplication].delegate;
    
    [appDelegate finishFirstLaunch];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

@end
