//
//  JournalEntryTableViewCell.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/15/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "JournalEntryTableViewCell.h"

@implementation JournalEntryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if(self) {
    }
    
    return self;
        
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setPropertiesFromInput: (NSDate *)date :(NSString *)description {
    NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"MMM"];
    self.monthLabel.text = [[monthFormatter stringFromDate:date] uppercaseString];
        
    NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
    [dayFormatter setDateFormat:@"dd"];
    self.dayLabel.text = [dayFormatter stringFromDate:date];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm a"];
    self.timeLabel.text = [timeFormatter stringFromDate:date];
    
    self.descriptionLabel.text = description;
        
}


@end
