//
//  JournalEntryTemplateModel.h
//  Mission List
//
//  Created by Austin Aldrich on 7/28/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsletterItem.h"
#import "JournalEntryModel.h"

@interface JournalEntryNewsletterItem : NewsletterItem

@property (nonatomic, strong) NSString *entryDescription;
@property (nonatomic, strong) NSDate *entryDate;
@property (nonatomic, strong) NSString *entryDateFormatted;
@property (nonatomic, strong) NSString *entryID;

+(JournalEntryNewsletterItem *) buildFromEntry:(JournalEntryModel *)entry;

@end
