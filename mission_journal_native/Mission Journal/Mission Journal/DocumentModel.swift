//
//  DocumentModel.swift
//  Mission List
//
//  Created by Austin Aldrich on 7/4/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation


class DocumentModel {
    var documentID :String = ""
    
    
    func getDocumentModel() -> NSMutableDictionary {
        return NSMutableDictionary()
    }
    
    func save() {
        var database = DatabaseService.getDatabase()
        var doc = database.createDocument()
        doc.putProperties(self.getDocumentModel(), error: nil)
    }
    
    func deleteDoc() {
        var database = DatabaseService.getDatabase()
        var doc = database.existingDocumentWithID(self.documentID)
        doc.deleteDocument(nil)
    }
    
    
    func saveOrUpdateWithId(docID :NSString) {
        var database = DatabaseService.getDatabase()
        var doc = database.existingDocumentWithID(self.documentID)
        var isUpdate = true
        
        
        if(doc == nil) {
            doc = database.documentWithID(docID)
            isUpdate = false
        }
        
        var docModel = self.getDocumentModel()
        
        if(isUpdate) {
            var rev = doc.currentRevision.revisionID
            docModel.setValue(rev, forKey: "_rev")
        }
        
        doc.putProperties(docModel, error: nil)
        
    }
    
    func update() {
        var database = DatabaseService.getDatabase()
        var doc = database.existingDocumentWithID(self.documentID)
        
        var docModel = self.getDocumentModel()
        var rev = doc.currentRevision.revisionID
        docModel.setValue(rev, forKey: "_rev")
        
        doc.putProperties(docModel, error: nil)
        
    }
    
}