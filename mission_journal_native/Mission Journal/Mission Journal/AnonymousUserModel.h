//
//  AnonymousUserModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseUserModel.h"

@interface AnonymousUserModel : BaseUserModel


@end
