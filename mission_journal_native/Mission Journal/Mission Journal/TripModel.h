//
//  TripModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/7/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentModel.h"
#import <CouchbaseLite/CouchbaseLite.h>

@interface TripModel : DocumentModel

@property (nonatomic, strong) NSString *tripName;
@property (nonatomic, strong) NSString *teamName;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic) BOOL isPublicTrip;
@property (nonatomic, strong) NSMutableArray *teammateUserIDs;
@property (nonatomic, strong) NSString *tripDescription;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *contactEmail;
@property (nonatomic, strong) NSString *tripNewsletterRecipientsID;

+(NSMutableArray *) getTrips:(CBLQueryEnumerator *) rows;
+(NSMutableArray *) getTripsFollowing;
+(TripModel *) getTrip:(NSString *) tripID;
+(TripModel *) buildTripFromDocument:(CBLDocument *) document;

-(NSString *) getFormattedTripDates;

-(BOOL) isTripOwner;
-(BOOL) isTeammate;

-(NSURL *) getTripURL;

-(BOOL) userIsFollowingTrip;
-(void) followTrip;
-(void) unfollowTrip;

@end
