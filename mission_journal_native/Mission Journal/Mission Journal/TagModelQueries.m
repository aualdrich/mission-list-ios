//
//  TagModel.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/11/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TagModelQueries.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"
#import "MissionList-Swift-Fixed.h"

@implementation TagModelQueries : NSObject


+(NSArray *) getTags {
    
    NSMutableArray *tags = [@[] mutableCopy];
    
    CBLDatabase *database = [DatabaseService getDatabase];
    
    CBLQuery *tripListQuery = [[database viewNamed:@"tag_list"] createQuery];
    
    NSError *error;
    
    CBLQueryEnumerator *enumerator = [tripListQuery run:&error];
    
    for(CBLQueryRow *row in enumerator) {
        TagModel *tag = [TagModel new];
        
        [tag setTagProperties:row.document];
        [tags addObject:tag];
        
    }
    
    
    NSMutableArray *combinedTags = [NSMutableArray arrayWithArray:tags];
    
    NSSet *distinctTagsSet = [NSSet setWithArray:combinedTags];
    
    NSMutableArray *distinctTags = [[distinctTagsSet allObjects] mutableCopy];
    
    return distinctTags;
    
    
}

+(NSArray *) getDefaultTags {
    
    NSString *userID = [[UserService currentUser] userID];
    
    TagModel *miracle = [TagModel new];
    TagModel *salvation = [TagModel new];
    TagModel *interesting_location = [TagModel new];
    TagModel *story = [TagModel new];
    
    miracle.tagName = @"miracle";
    miracle.userID = userID;
    
    salvation.tagName = @"salvation";
    salvation.userID = userID;
    
    interesting_location.tagName = @"interesting location";
    interesting_location.userID = userID;
    
    story.tagName = @"story";
    story.userID = userID;
    
    return [@[miracle, interesting_location, salvation, story] mutableCopy];
    
}




@end
