//
//  PhotoSyncDto.m
//  Mission Journal
//
//  Created by Austin Aldrich on 4/19/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "PhotoSyncDto.h"

@implementation PhotoSyncDto


-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if(!self)
        return nil;
    
    
    self.photoID = [aDecoder decodeObjectForKey:@"photoID"];
    self.isPrivate = [aDecoder decodeBoolForKey:@"isPrivate"];
    
    return self;
}

-(void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.photoID forKey:@"photoID"];
    [aCoder encodeBool:self.isPrivate forKey:@"isPrivate"];
}




@end
