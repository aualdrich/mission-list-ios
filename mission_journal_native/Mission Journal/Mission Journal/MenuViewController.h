//
//  MenuViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ECSlidingViewController/UIViewController+ECSlidingViewController.h>

@interface MenuViewController : UITableViewController
@property (nonatomic, strong) NSArray *menuItems;

@end
