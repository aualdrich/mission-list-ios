//
//  JournalEntryModel.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/9/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"
#import "UserService.h"
#import "MissionList-Swift-Fixed.h"

@implementation JournalEntryModelAdapter

+(NSMutableArray *) getJournalEntriesForTrip:(NSString *)tripID includeTeamPosts:(BOOL) includeTeamPosts {
    
    NSMutableArray *entries = [[NSMutableArray alloc] init];
    
    CBLDatabase *db = [DatabaseService getDatabase];
    CBLQuery *query = [[db viewNamed:@"journal_entries_for_trip"] createQuery];
    
    NSError *error;
    CBLQueryEnumerator *rowEnum = [query run:&error];
    
    for(CBLQueryRow *row in rowEnum) {
        NSString *rowKey = row.key;
        
        if([rowKey.lowercaseString isEqualToString:tripID.lowercaseString]) {
            JournalEntryModel *j = [[JournalEntryModel alloc] init];
            j.documentID = row.documentID;
            j.tripID = tripID;
            
            
            NSString *description = [row.document.properties valueForKey:@"entry_description"];
            j.entryDescription = description;
            
            NSString *entryDateJSON = [row.document.properties valueForKey:@"entry_date"];
            NSDate *entryDate = [CBLJSON dateWithJSONObject:entryDateJSON];
            j.entryDate = entryDate;
            
            NSString *userID = [row.document.properties valueForKey:@"user_id"];
            j.userID = userID;
            
            NSArray *tags = [row.document.properties valueForKey:@"tag_ids"];
            if(tags != nil)
                j.tagIDs = [NSMutableArray arrayWithArray:tags];
            
            
            NSArray *photos = [row.document.properties valueForKey:@"photo_ids"];
            if(photos != nil)
                j.photoIDs = [NSMutableArray arrayWithArray:photos];
                        
            id isPrivateTrip = [row.document.properties valueForKey:@"is_private"];
            
            //this is an awful hack to deal with the way the app stores TRUE as a string but
            //Rails stores it as an actual bit field.
            if ([isPrivateTrip isKindOfClass:[NSNumber class]]) {
                
                NSNumber *numVal = isPrivateTrip;
                j.isPrivate = [numVal isEqual:@1];
            }
            
            else if([isPrivateTrip isEqualToString:@"true"]) {
                j.isPrivate = true;
            }
            
            //non-team posts means you effectively only want your own posts ("Me")
            if(!includeTeamPosts) {
                
                NSString *currentUserID = [[UserService currentUser] userID];
                
                if([userID isEqualToString:currentUserID]) {
                    [entries addObject:j];
                }
            }
            
            //team posts includes everyone
            else {
                
                if(!j.isPrivate)
                    [entries addObject:j];
            }
            
        }
        
        
    }
    
    return entries;
    
}







@end
