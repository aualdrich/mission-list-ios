//
//  TextViewModalViewController.h
//  Mission List
//
//  Created by Austin Aldrich on 8/3/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TextViewModalDelegate <NSObject>

-(void) modalTextViewDoneEditing:(NSString *) text;
-(NSString *) titleForModal;
-(NSString *) initialText;

@end


@interface TextViewModalViewController : UIViewController <UITextViewDelegate>
    @property (nonatomic, strong) IBOutlet UITextView *textView;
    @property (nonatomic, weak) id <TextViewModalDelegate> delegate;
    @property (nonatomic, strong) NSString *PLACEHOLDER_TEXT;
@end
