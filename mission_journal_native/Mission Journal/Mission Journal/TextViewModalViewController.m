//
//  TextViewModalViewController.m
//  Mission List
//
//  Created by Austin Aldrich on 8/3/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TextViewModalViewController.h"

@interface TextViewModalViewController ()

@end

@implementation TextViewModalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
    
    self.PLACEHOLDER_TEXT = @"Enter a message";
    
    NSString *initialText = [self.delegate initialText];
    
    self.textView.text = initialText.length > 0 ? initialText : self.PLACEHOLDER_TEXT;
    
    self.navigationItem.title = [self.delegate titleForModal];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)done:(id)sender {
    NSString *result = [self.textView.text isEqualToString:self.PLACEHOLDER_TEXT] ? @"" : self.textView.text;
    
    [self.delegate modalTextViewDoneEditing:result];
}

-(void) textViewDidBeginEditing:(UITextView *)textView {
    if([textView.text isEqualToString:self.PLACEHOLDER_TEXT]) {
        self.textView.text = @"";
    }
}

@end
