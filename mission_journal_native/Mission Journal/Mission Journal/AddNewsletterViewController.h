//
//  AddNewsletterViewController.h
//  Mission List
//
//  Created by Austin Aldrich on 7/23/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"
#import "UserService.h"
#import "TripModel.h"
#import "JournalEntryPickerTableViewController.h"
#import <MWPhotoBrowser.h>
#import <BZGFormViewController.h>
#import <BZGFormFieldCell.h>
#import <BZGFormInfoCell.h>
#import "TextViewModalViewController.h"
#import "NewsletterModel.h"
#import <RNGridMenu.h>
#import "ManageNewsletterRecipientsViewController.h"

@interface AddNewsletterViewController : BZGFormViewController <UIActionSheetDelegate, UITextViewDelegate, JournalEntryPickerDelegate, MWPhotoBrowserDelegate, TextViewModalDelegate>

@property (nonatomic, strong) TripModel *tripModel;
@property (nonatomic, strong) BZGFormFieldCell *nameCell;
@property (nonatomic, strong) BZGFormFieldCell *messageCell;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) JournalEntryPickerTableViewController *journalEntryPicker;
@property (nonatomic, strong) MWPhotoBrowser *photoBrowser;
@property (nonatomic, strong) NSMutableArray *tripPhotos;
@property (nonatomic, strong) NSMutableArray *tripThumbPhotos;
@property (nonatomic, strong) NSMutableArray *tripPhotosSrc; //contains the raw PhotoModels
@property (nonatomic, strong) NSMutableArray *selectedPhotos;
@property (nonatomic, strong) TextViewModalViewController *textViewModalViewController;
@property (nonatomic, strong) NewsletterModel *newsletter;
@property (nonatomic, assign) BOOL editMode;
@property (nonatomic, strong) RNGridMenu *moreActionsMenu;
@property (nonatomic, strong) ManageNewsletterRecipientsViewController *manageRecipientsViewController;


-(IBAction)preview:(id)sender;
-(IBAction)save:(id)sender;
-(IBAction)addItemClicked:(id)sender;
-(void) build:(TripModel *) trip :(NewsletterModel *)newsletter :(BOOL) editMode;

@end
