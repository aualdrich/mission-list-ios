//
//  AboutViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 4/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController


-(IBAction)issuesGeneralQuestionsClicked:(id)sender;
-(IBAction)mainSiteClicked:(id)sender;

@end
