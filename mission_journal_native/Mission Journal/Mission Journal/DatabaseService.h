//
//  DatabaseService.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/7/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "MissionsInMotionAppDelegate.h"


@interface DatabaseService : NSObject

+(CBLDatabase *)getDatabase;
+(NSString *)getRemoteDatabaseURL;
+(void) generateViews;

@end
