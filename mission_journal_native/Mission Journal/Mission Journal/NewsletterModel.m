//
//  NewsletterBodyContentTemplateModel.m
//  Mission List
//
//  Created by Austin Aldrich on 7/29/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "NewsletterModel.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "UserService.h"
#import "DatabaseService.h"
#import "JournalEntryModel.h"
#import "PhotoModel.h"

@implementation NewsletterModel

-(NSMutableDictionary *) getDocumentModel {
    
    NSMutableDictionary *dictionary = [super getDocumentModel];
    [dictionary setValue:@"newsletter" forKey:@"doc_type"];
    [dictionary setValue:self.customMessage forKey:@"custom_message"];
    [dictionary setValue:self.userID forKey:@"user_id"];
    [dictionary setValue:self.tripID forKey:@"trip_id"];
    [dictionary setValue:self.tripName forKey:@"trip_name"];
    [dictionary setValue:self.newsletterItemIDs  forKey:@"newsletter_items"];
    [dictionary setValue:[CBLJSON JSONObjectWithDate:self.dateCreated] forKey:@"date_created"];
    [dictionary setValue:self.name forKey:@"name"];
    
    return dictionary;
}

-(void) save {
    [self saveOrUpdateWithId:self.documentID];
}


+(NSMutableArray *) getNewsletters:(NSString *)tripID {
    
    NSMutableArray *newsletters = [[NSMutableArray alloc] init];
    
    CBLDatabase *db = [DatabaseService getDatabase];
    CBLQuery *query = [[db viewNamed:@"newsletters"] createQuery];
    
    NSError *error;
    CBLQueryEnumerator *rowEnum = [query run:&error];
    
    for(CBLQueryRow *row in rowEnum) {
        
        NSString *currentTripID = [row.document.properties valueForKey:@"trip_id"];
        
        if([tripID isEqualToString:currentTripID]) {
        
            NewsletterModel *n = [NewsletterModel new];
            
            CBLDocument *doc = row.document;
            
            n.documentID = doc.documentID;
            n.tripID = [doc.properties valueForKey:@"trip_id"];
            n.tripName = [doc.properties valueForKey:@"trip_name"];
            
            NSString *message = [doc.properties valueForKey:@"custom_message"];
            
            if(message)
                n.customMessage = message;
            
            n.name = [doc.properties valueForKey:@"name"];
            n.dateCreated = [CBLJSON dateWithJSONObject:[doc.properties valueForKey:@"date_created"]];
            
            n.newsletterItemIDs = [@[] mutableCopy];
            
            NSArray *newsletterItems = [doc.properties valueForKey:@"newsletter_items"];
            
            for (NSString *itemID in newsletterItems) {
                [n.newsletterItemIDs addObject:itemID];
            }
            
            [newsletters addObject:n];
        }
        
        
    }
    
    return newsletters;
    
}


-(NSMutableArray *) getNewsletterItems {
    
    NSMutableArray *results = [@[] mutableCopy];
    
    for (NSString *itemID in self.newsletterItemIDs) {
        CBLDatabase *db = [DatabaseService getDatabase];
        CBLDocument *doc = [db documentWithID:itemID];
        
        NSString *itemType = [doc.properties valueForKey:@"doc_type"];
        
        if([itemType isEqualToString:@"journal_entry"]) {
            JournalEntryModel *entry = [JournalEntryModel getJournalEntry:doc.documentID];
            [results addObject:entry];
        }
        
        else if([itemType isEqualToString:@"photo"]) {
            PhotoModel *photo = [PhotoModel getPhoto:doc.documentID];
            [results addObject:photo];
        }
    }
    
    return results;

}

@end
