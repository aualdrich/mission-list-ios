//
//  PhotoModel.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

@objc class PhotoModel : DocumentModel {
    var userID = ""
    var displayImage = UIImage()
    var remotePhotoData = NSData()
    var isPrivate = false
    
    override func save() {
        self.saveOrUpdateWithId(self.documentID)
    }
    
    override func getDocumentModel() -> NSMutableDictionary {
        var model = NSMutableDictionary()
        model.setValue("photo", forKey: "doc_type")
        model.setValue(UserService.currentUser.userID, forKey: "user_id")
        return model
    }
    
    
    class func getPhoto(photoID :String) -> PhotoModel {
        var model = PhotoModel()
        var doc = DatabaseService.getDatabase().existingDocumentWithID(photoID)
        model.documentID = doc.documentID
        model.userID = doc.properties.objectForKey("user_id") as String
        return model
    }
    
    func getLocalImage() -> UIImage {
        var path = self.getPhotoPath() as String
        return UIImage(contentsOfFile: path)
    }
    
    func applicationDocumentsDirectory() -> NSURL {
       var items = NSFileManager.defaultManager().URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask) as NSArray
       return items.lastObject() as NSURL
    }
    
    func getPhotoPath() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true) as NSArray
        var documentsDirectory = paths.objectAtIndex(0) as String
        var path = documentsDirectory.stringByAppendingPathComponent("/\(self.documentID)")
        return path
    }
    
    func saveLocalPhoto(image :UIImage) {
       UIImageJPEGRepresentation(image, 1.0).writeToFile(self.getPhotoPath(), atomically: true)
    }
    
    
    func photoExists() -> Bool {
        
        var remoteStorageService = RemoteStorageService.getRemoteStorageService()
        var client = remoteStorageService.client;
        
        var request = S3GetObjectMetadataRequest(key: self.getPhotoKey(), withBucket: remoteStorageService.bucketName)
        
        var response = client.getObjectMetadata(request)
        
        
        if !response.error {
            return true
        }
        
        return false

    }
    
    func getPhotoKey() -> String {
        var photoKey = "\(self.documentID).jpg"
        return photoKey
    }
    
    func saveRemotePhoto(image :UIImage) -> Bool {
        var photoKey = self.getPhotoKey()
        var response = self.saveToS3(image, photoKey: photoKey)
        
        var resized = image.resizedImageToFitInSize(CGSizeMake(100.0, 100.0), scaleIfSmaller: true)
        var newPhotoKey = "thumbnails/\(photoKey)"
        var thumbnailResponse = self.saveToS3(resized, photoKey: newPhotoKey)
        
        var isSuccessful = true
        
        if(response.error != nil) {
            isSuccessful = false
        }
        
        else if(thumbnailResponse != nil) {
            isSuccessful = false
        }
        
        return isSuccessful
        
    }
    
    func saveToS3(image :UIImage, photoKey :String) -> S3Response {
        var remoteStorageService = RemoteStorageService.getRemoteStorageService()
        var client = remoteStorageService.client
        
        var request = S3PutObjectRequest(key: photoKey, inBucket: remoteStorageService.bucketName)
        request.contentType = "image/jpeg"
        
        var imageData = UIImageJPEGRepresentation(image, 1.0)
        request.data = imageData
        
        if(self.isPrivate) {
            request.cannedACL = S3CannedACL.private()
        }
        else {
            request.cannedACL = S3CannedACL.publicRead()
        }
        
        var response = client.putObject(request)
        
        return response
    }
    
    func getRemoteImage() -> UIImage {
        var remoteStorageService = RemoteStorageService.getRemoteStorageService()
        var client = remoteStorageService.client
        
        var photoKey = self.getPhotoKey()
        var request = S3GetObjectRequest(key: photoKey, withBucket: remoteStorageService.bucketName)
        
        
        var response = client.getObject(request)
        var data = response.body
        var img = UIImage(data: data)
        return img
        
    }
    
    
    func getRemoteImageData() -> NSData {
        var remoteStorageService = RemoteStorageService.getRemoteStorageService()
        var client = remoteStorageService.client
        
        var photoKey = self.getPhotoKey()
        var request = S3GetObjectRequest(key: photoKey, withBucket: remoteStorageService.bucketName)
        
        var response = client.getObject(request)
        var data = response.body
        
        return data
        
    }
    
    class func syncRemotePhotosInQueue(arg :String) {
        var unsyncedPhotos = PhotoModel.loadUnsynchedPhotos()
        var photos = PhotoModel[]()
        
        for dto in unsyncedPhotos {
            var photo = PhotoModel.getPhoto(dto.photoID)
            photo.isPrivate = dto.isPrivate
            
            photos.append(photo)
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                var unsuccessfulSyncs = PhotoSyncDto[]()
                var lock = NSLock()
            
                for p in photos {
                    var localImage = p.getLocalImage()
                    var successful = p.saveRemotePhoto(localImage)
                    
                    if(!successful) {
                        lock.lock()
                        var failureDto = PhotoSyncDto()
                        failureDto.photoID = p.documentID
                        failureDto.isPrivate = p.isPrivate
                        unsuccessfulSyncs.append(failureDto)
                        lock.unlock()
                    }
                }
            
                unsyncedPhotos.removeAll()
            
                for x in unsuccessfulSyncs {
                   unsyncedPhotos.append(x)
                }
            
                PhotoModel.saveUnsynchedPhotos(unsyncedPhotos)

            
            })
    }
    
    class func documentsDirectory() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true) as NSArray
        return paths.objectAtIndex(0) as String
    }
    
    class func unsyncedPhotosPath() -> String {
        return self.documentsDirectory().stringByAppendingPathComponent("unsynched_photos.plist")
    }
    
    
    class func loadUnsynchedPhotos() -> PhotoSyncDto[] {
        var array = NSMutableArray()
        var returnItems = PhotoSyncDto[]()
        var path = PhotoModel.unsyncedPhotosPath()
        
        if NSFileManager.defaultManager().fileExistsAtPath(path) {
            var data = NSData(contentsOfFile: path)
            var unarchiver = NSKeyedUnarchiver(forReadingWithData: data)
            array = unarchiver.decodeObjectForKey("unsynched_photos") as NSMutableArray
            unarchiver.finishDecoding()
        }
        
        for x :AnyObject in array {
            returnItems.append(x as PhotoSyncDto)
        }
        
        return returnItems
        
    }
    
    class func saveUnsynchedPhotos(array :PhotoSyncDto[]) {
        var data = NSMutableData()
        var archiver = NSKeyedArchiver(forWritingWithMutableData: data)
        archiver.encodeObject(array, forKey: "unsynched_photos")
        archiver.finishEncoding()
        data.writeToFile(self.unsyncedPhotosPath(), atomically: true)
    }
    
    func getRemoteImageURL() -> NSURL {
        var stringURL = "http://s3.amazonaws.com/missionjournal/\(self.documentID).jpg"
        return NSURL(string: stringURL)
    }
    
    func getRemoteImageThumbURL() -> NSURL {
        var stringURL = "http://s3.amazonaws.com/missionjournal/thumbnails/\(self.documentID).jpg"
        return NSURL(string: stringURL)
    }
    

}