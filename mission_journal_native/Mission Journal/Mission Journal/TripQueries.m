//
//  TripQueries.m
//  MissionList
//
//  Created by Austin Aldrich on 7/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TripQueries.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "MissionList-Swift-Fixed.h"

@implementation TripQueries

+(NSMutableArray *) getTrips:(CBLQueryEnumerator *) rows {
    
    NSMutableArray *trips = [@[] mutableCopy];
    
    NSString *currentUserID = [[UserService currentUser] userID];
    
    
    for(CBLQueryRow *row in rows) {
        
        NSString *rowUserID = [row.document.properties valueForKey:@"user_id"];
        NSMutableArray *teammateUserIDs = [row.document.properties valueForKey:@"teammate_user_ids"];
        
        if([rowUserID isEqualToString:currentUserID] || [self isTeammateInTrip:teammateUserIDs :currentUserID]) {
            
            TripModel *trip = [self buildTripFromDocument:row.document];
            [trips addObject:trip];
            
        }
        
        
    }
    
    
    return trips;
    
    
}




@end
