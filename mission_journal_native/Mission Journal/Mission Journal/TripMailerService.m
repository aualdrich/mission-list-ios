//
//  TripMailerService.m
//  Mission List
//
//  Created by Austin Aldrich on 7/22/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TripMailerService.h"
#import "Constants.h"
#import "UserService.h"
#import <AFNetworking.h>

@implementation TripMailerService



+(void) sendFollowerNotification:(NSString *) tripID {
    FBUserModel *user = (FBUserModel *)[UserService currentUser];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"trip_id": tripID, @"user_id": user.userID};
    
    NSString *url = [NSString stringWithFormat:@"%@/trips/send_follower_notification_email", [Constants getBaseURL]];
    
    [manager setCredential:[NSURLCredential credentialWithUser:@"missionsinmotion" password:@"M1m34348" persistence:NSURLCredentialPersistenceForSession]];
    
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"done");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


@end
