//
//  AnonymousUserModel.swift
//  Mission List
//
//  Created by Austin Aldrich on 7/4/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class AnonymousUserModel : BaseUserModel {
    
    
    override func getDocumentModel() -> NSMutableDictionary {
        
        var model = NSMutableDictionary()
        model.setObject("anonymous_user", forKey: "doc_type")
        return model
    }
    
    
}