//
//  UserService.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/8/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MissionsInMotionAppDelegate.h"
#import "BaseUserModel.h"
#import <FacebookSDK/FacebookSDK.h>
#import <FacebookSDK/FBGraphUser.h>

@interface UserService : NSObject

+(BaseUserModel *) currentUser;
+(void) resetCurrentUser;

+(void) saveAnonymousUserDefaults;
+(void) saveFBUserDefaults:(id<FBGraphUser>)user;

@end
