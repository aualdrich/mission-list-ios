//
//  PhotoTemplateModel.m
//  Mission List
//
//  Created by Austin Aldrich on 7/28/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "PhotoNewsletterItem.h"

@implementation PhotoNewsletterItem


-(NSMutableDictionary *) getDocumentModel {
    NSMutableDictionary *dictionary = [super getDocumentModel];
    
    [dictionary setValue:@"photo" forKey:@"item_type"];
    [dictionary setValue:self.photoSrc forKey:@"photo_src"];
    [dictionary setValue:self.thumbnailSrc forKey:@"thumbnail_src"];
    [dictionary setValue:self.photoID forKey:@"photo_id"];
    
    return dictionary;
}

-(void) save {
    [self saveOrUpdateWithId:self.documentID];
}

+(PhotoNewsletterItem *) buildFromPhotoModel:(PhotoModel *)photoModel {
    PhotoNewsletterItem *model = [PhotoNewsletterItem new];
    model.photoSrc = [[photoModel getRemoteImageURL] absoluteString];
    model.thumbnailSrc = [[photoModel getRemoteImageThumbURL] absoluteString];
    model.photoID = photoModel.documentID;
    
    return model;
}

-(NSString *) itemType {
    return @"photo";
}


@end
