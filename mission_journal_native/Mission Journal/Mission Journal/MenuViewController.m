//
//  MenuViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "MenuViewController.h"
#import "Branding.h"
#import "TripListViewController.h"
#import "MyProfileViewController.h"
#import "AnonymousProfileViewController.h"
#import "UserService.h"
#import "AboutViewController.h"
#import "FindTripsViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(20, self.tableView.contentInset.left, self.tableView.contentInset.bottom, self.tableView.contentInset.right)];
    
    //self.tableView.backgroundColor = [Branding getLightGreyColor];
    
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.textColor = [Branding getDarkestGreyColor];
    }
    
    // Configure the cell...
    switch(indexPath.row) {
        case 0:
            cell.imageView.image = [UIImage imageNamed:@"Search"];
            cell.textLabel.text = @"Find Trips";
            break;
        case 1:
            cell.imageView.image = [UIImage imageNamed:@"Trips"];
            cell.textLabel.text = @" My Trips";
            break;
        case 2:
            cell.imageView.image = [UIImage imageNamed:@"UserProfile"];
            cell.textLabel.text = @"My Profile";
            break;
        case 3:
            cell.imageView.image = [UIImage imageNamed:@"Info"];
            cell.textLabel.text = @"About";
            break;
            
    }
    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0) {
        FindTripsViewController *findTripsViewController = [[FindTripsViewController alloc] initWithNibName:@"FindTripsViewController" bundle:[NSBundle mainBundle]];
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:findTripsViewController];
        self.slidingViewController.topViewController = navController;
    }

    //trips
    if(indexPath.row == 1) {
        TripListViewController *tripsViewController = [TripListViewController new];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:tripsViewController];
        self.slidingViewController.topViewController = navController;
    }
    
    
    //profile info
    if(indexPath.row == 2) {
        
        BaseUserModel *userModel = [UserService currentUser];
        if([userModel isKindOfClass:[AnonymousUserModel class]]) {
            AnonymousProfileViewController *anonymousProfileViewController = [AnonymousProfileViewController new];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:anonymousProfileViewController];
            self.slidingViewController.topViewController = navController;
        }
        
        else {
        
        MyProfileViewController *myProfileViewController = [MyProfileViewController new];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:myProfileViewController];
        self.slidingViewController.topViewController = navController;
            
        }
    }
    
    //about view
    if(indexPath.row == 3) {
        AboutViewController *aboutViewController = [AboutViewController new];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:aboutViewController];
        self.slidingViewController.topViewController = navController;
    }
    
    [self.slidingViewController resetTopViewAnimated:YES];
    
}
 


@end
