//
//  Constants.m
//  Mission List
//
//  Created by Austin Aldrich on 7/19/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "Constants.h"

@implementation Constants

+(NSString *) getBaseURL {
  return @"http://localhost:3000";
  //  return @"http://www.missionlist.org";
}


@end
