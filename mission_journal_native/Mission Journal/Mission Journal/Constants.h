//
//  Constants.h
//  Mission List
//
//  Created by Austin Aldrich on 7/19/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Constants : NSObject


+(NSString *) getBaseURL;

@end
