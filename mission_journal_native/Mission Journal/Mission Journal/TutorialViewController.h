//
//  TutorialViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 4/3/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <EAIntroView.h>

@interface TutorialViewController : UIViewController <EAIntroDelegate>

@end
