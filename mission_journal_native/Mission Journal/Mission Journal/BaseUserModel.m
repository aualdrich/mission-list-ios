//
//  BaseUserModel.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/8/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "BaseUserModel.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"
#import "UserService.h"
#import "RemoteStorageService.h"

@implementation BaseUserModel

-(void) save {
    
    [self saveOrUpdateWithId:self.userID];

}

-(NSMutableDictionary *) getDocumentModel {
    return [@{@"date_added": [CBLJSON JSONObjectWithDate:self.dateAdded]} mutableCopy];
        
}

@end
