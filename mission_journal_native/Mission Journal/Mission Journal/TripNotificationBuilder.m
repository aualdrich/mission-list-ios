//
//  TripNotificationBuilder.m
//  Mission List
//
//  Created by Austin Aldrich on 8/25/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TripNotificationBuilder.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"


@implementation TripNotificationBuilder


+(void) buildNotificationForTrip:(TripModel *)trip {
    
    NSMutableArray *dateList = [NSMutableArray array];
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:1];
    
    [dateList addObject: trip.startDate];
    NSDate *currentDate = trip.startDate;
    currentDate = [currentCalendar dateByAddingComponents:comps toDate:currentDate  options:0];
    while ( [trip.endDate compare: currentDate] != NSOrderedAscending) {
        [dateList addObject: currentDate];
        currentDate = [currentCalendar dateByAddingComponents:comps toDate:currentDate  options:0];
    }
    
    for (NSDate *date in dateList) {
        //if the date has already passed, no need to create notifications--just create the ones for the rest of the days on the trip.
        if([date compare:[NSDate date]] == NSOrderedAscending) continue;
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = date;
        notification.alertAction = @"Go to Trip";
        notification.alertBody = [NSString stringWithFormat:@"%@ has begun! Don't forget to post your experiences!", trip.tripName];
        notification.applicationIconBadgeNumber = 1;
        notification.userInfo = @{@"trip_id": trip.documentID};
    
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
    
    
}


+(void) buildTripNotificationsForUser {
    CBLDatabase *db = [DatabaseService getDatabase];
    CBLQuery *query = [[db viewNamed:@"trip_list"] createQuery];
    NSMutableArray *trips = [TripModel getTrips:[query run:nil]];
    
    NSArray *existingNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (TripModel *trip in trips) {
        if(trip.startDate == nil || trip.endDate == nil) continue;
        if(![self isInDateRangewithInputDate:[NSDate date] start:trip.startDate end:trip.endDate]) continue;
            
            BOOL notificationExistsForTrip = FALSE;
            
            for (UILocalNotification *notifications in existingNotifications) {
                NSString *tripID = [notifications.userInfo objectForKey:@"trip_id"];
                
                if(tripID != nil && [trip.documentID isEqualToString:tripID])
                    notificationExistsForTrip = TRUE;
                    break;
            }
        
            if(!notificationExistsForTrip)
                [self buildNotificationForTrip:trip];
    }
    
}

+ (BOOL)isInDateRangewithInputDate:(NSDate*)date start:(NSDate*)beginDate end:(NSDate*)endDate
{
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}


@end
