//
//  PhotoTemplateModel.h
//  Mission List
//
//  Created by Austin Aldrich on 7/28/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsletterItem.h"
#import "PhotoModel.h"

@interface PhotoNewsletterItem : NewsletterItem

@property (nonatomic, strong) NSString *photoSrc;
@property (nonatomic, strong) NSString *thumbnailSrc;
@property (nonatomic, strong) NSString *photoID;

+(PhotoNewsletterItem *) buildFromPhotoModel:(PhotoModel *)photoModel;

@end
