//
//  TagModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/11/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//


@interface TagModelQueries

+(NSArray *) getTags;
+(NSArray *) getDefaultTags;

@end
