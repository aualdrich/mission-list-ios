//
//  TripMailerService.h
//  Mission List
//
//  Created by Austin Aldrich on 7/22/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TripMailerService : NSObject

+(void) sendFollowerNotification:(NSString *) tripID;

@end
