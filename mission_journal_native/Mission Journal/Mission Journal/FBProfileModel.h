//
//  FBProfileModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 3/17/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBProfileModel : NSObject

@property (nonatomic, strong) NSString *profileID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) UIImage *profileImage;

@end
