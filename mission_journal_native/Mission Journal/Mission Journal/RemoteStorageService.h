//
//  RemoteStorageService.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/16/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AWSS3/AWSS3.h>

@interface RemoteStorageService : NSObject

@property (nonatomic, strong) AmazonS3Client *client;
@property (nonatomic, strong) NSString *bucketName;

+(RemoteStorageService *) getRemoteStorageService;


@end
