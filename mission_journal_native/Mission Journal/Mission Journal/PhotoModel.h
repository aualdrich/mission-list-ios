//
//  PhotoModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/14/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "DocumentModel.h"
#import <AWSS3/AWSS3.h>

@interface PhotoModel : DocumentModel

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) UIImage *displayImage;
@property (nonatomic, strong) NSData *remotePhotoData;
@property (nonatomic) BOOL isPrivate;
@property (nonatomic, strong) NSString *thumbnailSrc;
@property (nonatomic, strong) NSString *photoSrc;


+(PhotoModel *) getPhoto:(NSString *) photoID;

-(UIImage *) getLocalImage;
-(UIImage *) getRemoteImage;
-(UIImage *) getRemoteThumbnailImage;
-(NSData *) getRemoteImageData;
-(NSURL *) getRemoteImageURL;
-(NSURL *) getRemoteImageThumbURL;

-(void) saveLocalPhoto:(UIImage *)image;
-(BOOL) saveRemotePhoto:(UIImage *)image;

-(NSString *) getPhotoKey;


+(void) syncRemotePhotosInQueue:(NSString *) arg;
+ (NSMutableArray *)loadUnsyncedPhotos;
+ (void)saveUnsyncedPhotos:(NSMutableArray *)array;
+ (NSString *)unsycedPhotosPath;



@end
