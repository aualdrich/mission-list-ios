//
//  NewsletterService.h
//  Mission List
//
//  Created by Austin Aldrich on 8/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TripModel.h"

@interface NewsletterService : NSObject

+(void) sendNewsletter:(NSString *) newsletterID :(TripModel *) trip;

@end
