//
//  TripModel.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/7/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TripModel.h"
#import "DatabaseService.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "UserService.h"
#import "TripMailerService.h"

@implementation TripModel

-(TripModel *) init {
    
    self.teammateUserIDs = [[NSMutableArray alloc] init];
    self.tripNewsletterRecipientsID = @"";
    
    return self;

}



-(NSMutableDictionary *)getDocumentModel {
    

    return [@{@"trip_name": self.tripName,
             @"team_name": self.teamName,
              @"start_date": (self.startDate != nil) ? [CBLJSON JSONObjectWithDate:self.startDate] : @"",
              @"end_date": (self.endDate != nil) ? [CBLJSON JSONObjectWithDate:self.endDate] : @"",
             @"user_id": self.userID,
              @"is_public_trip": self.isPublicTrip ? @"true" : @"false",
              @"doc_type": @"trip",
              @"teammate_user_ids": self.teammateUserIDs,
              @"description": self.tripDescription,
              @"price": self.price ? self.price : @"",
              @"contact_email": self.contactEmail ? self.contactEmail : @"",
              @"trip_newsletter_recipients_id": self.tripNewsletterRecipientsID
              } mutableCopy];
    
}




+(NSMutableArray *) getTrips:(CBLQueryEnumerator *) rows {
    
    NSMutableArray *trips = [@[] mutableCopy];
    
    NSString *currentUserID = [[UserService currentUser] userID];
    

    for(CBLQueryRow *row in rows) {
        
        NSString *rowUserID = [row.document.properties valueForKey:@"user_id"];
        NSMutableArray *teammateUserIDs = [row.document.properties valueForKey:@"teammate_user_ids"];
        
        if([rowUserID isEqualToString:currentUserID] || [self isTeammateInTrip:teammateUserIDs :currentUserID]) {
            
            TripModel *trip = [self buildTripFromDocument:row.document];
            [trips addObject:trip];
                
        }
        
        
    }
    
    
    return trips;

    
}

+(NSMutableArray *) getTripsFollowing {
    
    NSMutableArray *trips = [@[] mutableCopy];
    
    NSMutableArray *tripIDsFollowing = [TripModel getTripsIDsFollowed];
    
    CBLDatabase *db = [DatabaseService getDatabase];
    
    for(NSString *tripID in tripIDsFollowing) {
        CBLDocument *tripDoc = [db documentWithID:tripID];
        TripModel *trip = [self buildTripFromDocument:tripDoc];
        [trips addObject:trip];
    }
    
    return trips;
    
    
}

+(TripModel *) getTrip:(NSString *)tripID {
    
    CBLDatabase *db = [DatabaseService getDatabase];
    
    CBLDocument *doc = [db documentWithID:tripID];
    
    TripModel *trip = [self buildTripFromDocument:doc];
    
    return trip;
    
    
}

+(TripModel *) buildTripFromDocument:(CBLDocument *) document {
    TripModel *trip = [TripModel new];
    trip.documentID = document.documentID;
    trip.isPublicTrip = YES;
    
    
    NSString *tripName = [document.properties valueForKey:@"trip_name"];
    NSString *teamName = [document.properties valueForKey:@"team_name"];
    NSString *startDate = [document.properties valueForKey:@"start_date"];
    NSString *endDate = [document.properties valueForKey:@"end_date"];
    id is_public_trip = [document.properties valueForKey:@"is_public_trip"];
    NSString *userID = [document.properties valueForKey:@"user_id"];
    NSMutableArray *teammateUserIDs = [document.properties valueForKey:@"teammate_user_ids"];
    NSString *tripDesc = [document.properties valueForKey:@"description"];
    NSString *price = [document.properties valueForKey:@"price"];
    NSString *contactEmail = [document.properties valueForKey:@"contact_email"];
    NSString *tripNewsletterRecipientsID = [document.properties valueForKey:@"trip_newsletter_recipients_id"];
    
    if(tripName)
        trip.tripName = tripName;
    
    if(teamName)
        trip.teamName = teamName;
    
    if(startDate)
        trip.startDate = [CBLJSON dateWithJSONObject:startDate];
    
    if(endDate)
        trip.endDate = [CBLJSON dateWithJSONObject:endDate];
    
    if(userID)
        trip.userID = userID;
    
    //this is an awful hack to deal with the way the app stores TRUE as a string but
    //Rails stores it as an actual bit field.
    if ([is_public_trip isKindOfClass:[NSNumber class]]) {
        
        NSNumber *numVal = is_public_trip;
        if([numVal isEqual:@0]) {
            trip.isPublicTrip = NO;
        }
    }
    
    else if([is_public_trip isEqualToString:@"false"]) {
        trip.isPublicTrip = NO;
    }
    
    if(teammateUserIDs) {
        trip.teammateUserIDs = [[NSMutableArray alloc] initWithArray:teammateUserIDs];
    }
    
    if(tripDesc)
        trip.tripDescription = tripDesc;
    
    if(price)
        trip.price = price;
    
    if(contactEmail)
        trip.contactEmail = contactEmail;
    
    if(tripNewsletterRecipientsID)
        trip.tripNewsletterRecipientsID = tripNewsletterRecipientsID;
    
    return trip;
}

+(BOOL) isTeammateInTrip:(NSMutableArray *)teammateUserIDs :(NSString *) currentUserID{
    if(teammateUserIDs == nil)
        return NO;
    
    return [teammateUserIDs containsObject:currentUserID];
}

-(BOOL) isTripOwner {
    NSString *currentUserID = [[UserService currentUser] userID];
    return [self.userID isEqualToString:currentUserID];  
}

-(BOOL) isTeammate {
    NSString *currentUserID = [[UserService currentUser] userID];
    
    if([self.userID isEqualToString:currentUserID])
        return YES;
    
    if(self.teammateUserIDs == nil || self.teammateUserIDs.count == 0)
        return NO;
    
    return ([self.teammateUserIDs containsObject:currentUserID]);
        
}

-(void) save {
    [self saveOrUpdateWithId:self.documentID];
}

-(NSString *) getFormattedTripDates {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d yyyy"];
    
    NSString *formattedStartDate = (self.startDate != nil) ? [dateFormatter stringFromDate:self.startDate] : @"";
    NSString *formattedEndDate = (self.endDate != nil) ? [dateFormatter stringFromDate:self.endDate] : @"";
    
    NSString *formattedDates = [NSString stringWithFormat:@"%@ - %@", formattedStartDate, formattedEndDate];
    
    return formattedDates;
    
}


-(NSURL *) getTripURL {
    NSString *url = [NSString stringWithFormat:@"http://www.missionlist.org/trips/show/%@", self.documentID];
    return [NSURL URLWithString:url];
}

+(CBLDocument *) getUserDoc {
    BaseUserModel *user = [UserService currentUser];
    CBLDatabase *db = [DatabaseService getDatabase];
    CBLDocument *userDoc = [db documentWithID:user.userID];
    return userDoc;

}

+(NSMutableArray *) getTripsIDsFollowed {
    FBUserModel *user = (FBUserModel *)[UserService currentUser];
    return user.tripsFollowed != nil ? user.tripsFollowed : [@[] mutableCopy];
}

-(BOOL) userIsFollowingTrip {
    NSMutableArray *tripsFollowed = [TripModel getTripsIDsFollowed];
    
    for (NSString *followingTripID in tripsFollowed) {
        if([followingTripID isEqualToString:self.documentID])
            return YES;
    }
    
    return NO;
    
}

-(void) followTrip {
    NSMutableArray *tripsFollowed = [TripModel getTripsIDsFollowed];
    [tripsFollowed addObject:self.documentID];
    [self updateUserWithTripsFollowed:tripsFollowed];
    
    [TripMailerService sendFollowerNotification:self.documentID];
    
}

-(void) unfollowTrip {
    NSMutableArray *tripsFollowed = [TripModel getTripsIDsFollowed];
    [tripsFollowed removeObject:self.documentID];
    [self updateUserWithTripsFollowed:tripsFollowed];

}

-(void) updateUserWithTripsFollowed: (NSMutableArray *) tripsFollowed {
    FBUserModel *user = (FBUserModel *) [UserService currentUser];
    user.tripsFollowed = tripsFollowed;
    [user save];
    
}



@end
