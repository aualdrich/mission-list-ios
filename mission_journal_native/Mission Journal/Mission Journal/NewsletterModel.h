//
//  NewsletterBodyContentTemplateModel.h
//  Mission List
//
//  Created by Austin Aldrich on 7/29/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentModel.h"

@interface NewsletterModel : DocumentModel
@property (nonatomic, strong) NSString *customMessage;
@property (nonatomic, strong) NSString *itemsContent;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *tripID;
@property (nonatomic, strong) NSString *tripName;
@property (nonatomic, strong) NSMutableArray *newsletterItemIDs;
@property (nonatomic, strong) NSDate *dateCreated;
@property (nonatomic, strong) NSString *name;


+(NSMutableArray *) getNewsletters:(NSString *) tripID;

-(NSMutableArray *) getNewsletterItems;

@end
