//
//  JournalEntryTableViewCell.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/15/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalEntryTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *monthLabel;
@property (nonatomic, weak) IBOutlet UILabel *dayLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;

-(void) setPropertiesFromInput: (NSDate *)date :(NSString *)description;

@end
