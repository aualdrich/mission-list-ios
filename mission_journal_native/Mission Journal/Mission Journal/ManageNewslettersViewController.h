//
//  ManageNewslettersViewController.h
//  Mission List
//
//  Created by Austin Aldrich on 7/23/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripModel.h"
#import "UserService.h"
#import "AddNewsletterViewController.h"
#import "Branding.h"
#import "ManageNewsletterRecipientsViewController.h"


@interface ManageNewslettersViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) TripModel *trip;
@property (nonatomic, strong) IBOutlet UITableView *newsletterTableView;
@property (nonatomic, strong) NSMutableArray *newsletters;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) ManageNewsletterRecipientsViewController *manageNewsletterRecipientsViewController;
@property (nonatomic, strong) TripNewsletterRecipientsModel *recipients;


-(IBAction)manageRecipientsClicked:(id)sender;

@end
