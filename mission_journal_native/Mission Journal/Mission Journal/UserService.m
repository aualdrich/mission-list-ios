//
//  UserService.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/8/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "UserService.h"
#import "FBUserModel.h"
#import "AnonymousUserModel.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"


@implementation UserService
    static BaseUserModel *currentUser;


+ (BaseUserModel *) currentUser {

    
    @synchronized(self) {
        if(!currentUser) {
            currentUser = [self generateAppropriateUserModel];
        }
        
        return currentUser;
    }
    
}

+(void) resetCurrentUser {
    
    currentUser = [self generateAppropriateUserModel];
}


+ (BaseUserModel *) generateAppropriateUserModel {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    FBSession *fbSession = [FBSession activeSession];
    FBSessionState fbState = [fbSession state];
    
    //Use FB Authentication
    if (fbState == FBSessionStateOpen || fbState == FBSessionStateCreatedTokenLoaded || fbState == FBSessionStateCreatedOpening || fbState == FBSessionStateOpenTokenExtended) {
        FBUserModel *fbUserModel = [FBUserModel new];
        fbUserModel.userID = [userDefaults stringForKey:@"FBUserID"];
        fbUserModel.accessToken = [userDefaults stringForKey:@"FBToken"];
        
        [self openFBSession];
        
        [fbUserModel setPropertiesFromDb:fbUserModel.userID];
        
        return fbUserModel;
        
    }
    
    //Use Anonymous Authentication
    else {
        AnonymousUserModel *anonymousUserModel = [AnonymousUserModel new];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *userID = [userDefaults stringForKey:@"AnonymousUserID"];
        
        if(userID != nil) {
            anonymousUserModel.userID = userID;
        }
        
        else {
            [self saveAnonymousUserDefaults];
            anonymousUserModel.userID = [userDefaults stringForKey:@"AnonymousUserID"];
        }
        
        CBLDocument *user = [[DatabaseService getDatabase] documentWithID:anonymousUserModel.userID];
        anonymousUserModel.dateAdded = [CBLJSON dateWithJSONObject:[user.properties valueForKey:@"date_added"]];
        
        return anonymousUserModel;
        
    }

}

+ (void)openFBSession
{
    if (!FBSession.activeSession.isOpen) {
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession.activeSession openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
            switch (state) {
                case FBSessionStateClosedLoginFailed:
                {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                        message:error.localizedDescription
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
                    break;
                default:
                    break;
            }
        }];
    }}

+(void) saveAnonymousUserDefaults {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if([userDefaults stringForKey:@"AnonymousUserID"] == nil) {
        NSString *newUserID = [[NSUUID UUID] UUIDString];
        [userDefaults setObject:newUserID forKey:@"AnonymousUserID"];
        [userDefaults synchronize];
    }
    
    BaseUserModel *user = [UserService currentUser];
    user.dateAdded = [NSDate date];
    [user save];

}

+(void) saveFBUserDefaults:(id<FBGraphUser>) user {
    NSString *fbToken = [[[FBSession activeSession] accessTokenData] accessToken];
    NSString *fbUserID = [user objectID];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:fbToken forKey:@"FBToken"];
    [userDefaults setObject:fbUserID forKey:@"FBUserID"];
    [userDefaults synchronize];
    
    FBUserModel *fbUser = (FBUserModel *) [UserService currentUser];
    fbUser.userID = fbUserID;
    fbUser.email = [user objectForKey:@"email"];
    fbUser.dateAdded = [NSDate date];
    fbUser.firstName = [user first_name];
    fbUser.lastName = [user last_name];
    [fbUser save];
    
}


@end
