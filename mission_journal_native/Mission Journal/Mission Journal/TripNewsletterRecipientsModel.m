//
//  TripNewsletterRecipientModel.m
//  Mission List
//
//  Created by Austin Aldrich on 8/20/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TripNewsletterRecipientsModel.h"
#import "DatabaseService.h"
#import <CouchbaseLite/CouchbaseLite.h>

@implementation TripNewsletterRecipientsModel

-(NSMutableDictionary *) getDocumentModel {
    return [@{@"doc_type":  @"trip_newsletter_recipients",
              @"trip_id": self.tripID,
              @"user_id": self.userID,
              @"recipient_ids": self.recipientIDs
            } mutableCopy];
}

-(void) save {
    [self saveOrUpdateWithId:self.documentID];
}


+(TripNewsletterRecipientsModel *) getTripNewsletterRecipients:(NSString *)docID {
    TripNewsletterRecipientsModel *recipients = [TripNewsletterRecipientsModel new];
    
    CBLDatabase *db = [DatabaseService getDatabase];
    CBLDocument *doc = [db documentWithID:docID];
    
    recipients.documentID = doc.documentID;
    recipients.tripID = [doc.properties valueForKey:@"trip_id"];
    recipients.userID = [doc.properties valueForKey:@"user_id"];
    
    recipients.recipientIDs = [@[] mutableCopy];
    [recipients.recipientIDs addObjectsFromArray:[doc.properties valueForKey:@"recipient_ids"]];
   
    return recipients;
}

@end
