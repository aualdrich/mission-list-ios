//
//  MissionsInMotionAppDelegate.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/1/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBUserModel.h"
#import "AnonymousUserModel.h"
#import <FacebookSDK/FacebookSDK.h>
#import <CouchbaseLite/CouchbaseLite.h>

@interface MissionsInMotionAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) FBUserModel *fbUserModel;
@property (nonatomic, strong) AnonymousUserModel *anonymousUserModel;
@property (nonatomic, strong) CBLDatabase *database;
@property (nonatomic, strong) NSString *tripIDFromNotification;
@property (nonatomic, strong) UINavigationController *navController;

- (NSURL *)applicationDocumentsDirectory;

- (BOOL) firstLaunch;





-(void) showLoginViewController;

-(void) finishFirstLaunch;



@end
