//
//  TripNotificationBuilder.h
//  Mission List
//
//  Created by Austin Aldrich on 8/25/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserService.h"
#import "TripModel.h"

@interface TripNotificationBuilder : NSObject

+(void) buildNotificationForTrip:(TripModel *)trip;

+(void) buildTripNotificationsForUser;

@end
