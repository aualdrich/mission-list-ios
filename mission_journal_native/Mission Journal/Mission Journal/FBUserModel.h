//
//  FBUserModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseUserModel.h"

@interface FBUserModel : BaseUserModel

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSMutableArray *tripsFollowed;
@property (nonatomic, strong) NSString *photoURL;
@property (nonatomic) NSString *welcomeEmailSent;


-(void) setPropertiesFromDb:(NSString *)userID;

@end
