//
//  NewsletterItem.m
//  Mission List
//
//  Created by Austin Aldrich on 8/1/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "NewsletterItem.h"

@implementation NewsletterItem

-(NSMutableDictionary *) getDocumentModel {
    
    NSMutableDictionary *dictionary = [super getDocumentModel];
    
    [dictionary setValue:@"newsletter_item" forKey:@"doc_type"];
    [dictionary setValue:self.itemType forKey:@"item_type"];
    
    return dictionary;
    
}


@end
