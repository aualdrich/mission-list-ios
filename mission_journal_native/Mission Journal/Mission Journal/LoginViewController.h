//
//  LoginViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <EAIntroView.h>

@interface LoginViewController : UIViewController <FBLoginViewDelegate> {
    IBOutlet FBLoginView *loginView;
}

-(IBAction)continueWithoutLogin:(id)sender;
-(void) finishFirstLaunch;



@end
