//
//  BaseUserModel.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/8/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentModel.h"

@interface BaseUserModel : DocumentModel

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSDate *dateAdded;

@end
