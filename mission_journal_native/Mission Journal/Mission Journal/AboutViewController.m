//
//  AboutViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 4/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "AboutViewController.h"
#import <ECSlidingViewController/UIViewController+ECSlidingViewController.h>

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"About";
    
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(menuPressed:)];
    self.navigationItem.leftBarButtonItem = menuButton;
    
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    
}

-(IBAction)menuPressed:(id)sender {
    
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionAnchoredRight) {
        [self.slidingViewController resetTopViewAnimated:YES];
    }
    
    else {
        [self.slidingViewController anchorTopViewToRightAnimated:YES];
    }
    
    
}

-(void) issuesGeneralQuestionsClicked:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:info@missions-in-motion.com"]];
}

-(void) mainSiteClicked:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"www.missions-in-motion.com"]];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
