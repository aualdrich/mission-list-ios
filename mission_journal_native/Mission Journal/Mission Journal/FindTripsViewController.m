//
//  FindTripsViewController.m
//  Mission List
//
//  Created by Austin Aldrich on 7/7/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "FindTripsViewController.h"
#import "DatabaseService.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "JournalentriesViewController.h"
#import <ECSlidingViewController/UIViewController+ECSlidingViewController.h>
#import <ECSlidingViewController.h>
#import "ReadOnlyJournalEntriesViewController.h"

@interface FindTripsViewController ()
    
@end


@implementation FindTripsViewController


-(void) viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(menuPressed)];
    
    self.navigationItem.title = @"Find Trips";
    
    [self.searchTableView registerNib:[UINib nibWithNibName:@"TripResultTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
                                            
    
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchResults.count;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 103;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TripResultTableViewCell *cell = [self.searchTableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    TripModel *trip = [self.searchResults objectAtIndex:indexPath.row];
    cell.trip = trip;
    
    [cell updateCell];
    
    return cell;
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *searchText = searchBar.text;
    
    NSMutableArray *trips = [NSMutableArray array];
    
    NSString *remoteUrl = @"https://missionsinmotion:M1m34348@missionsinmotion.cloudant.com/journal";
    
    NSString *searchQuery = [NSString stringWithFormat:@"trip_name:%@* or team_name:%@*", searchText, searchText];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/_design/trips/_search/trip_search?q=%@", remoteUrl, searchQuery];
    NSString *encodedRequestUrl = [requestUrl stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:encodedRequestUrl];
    
    NSError *error;
    
    NSData *data = [NSData dataWithContentsOfURL:url options:NSDataReadingMapped error:&error];
    
    NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    NSArray *rows = [jsonDict valueForKey:@"rows"];
    
    
    for (NSDictionary *row in rows) {
        NSString *tripID = [row valueForKey:@"id"];
        TripModel *trip = [TripModel getTrip:tripID];
        
        if([trip isPublicTrip] || [trip isTeammate]) {
            [trips addObject:trip];
        }
    }
    
    self.searchResults = trips;

    [self.searchTableView reloadData];

    
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TripModel *trip = [self.searchResults objectAtIndex:indexPath.row];
    
    
    if([trip isTeammate]) {
        JournalEntriesViewController *journalEntriesViewController = [JournalEntriesViewController new];
        journalEntriesViewController.trip = trip;
        journalEntriesViewController.defaultViewMode = @"Team";
        [self.navigationController pushViewController:journalEntriesViewController animated:YES];
    }
    
    else {
        ReadOnlyJournalEntriesViewController *readOnlyJournalEntriesViewController = [ReadOnlyJournalEntriesViewController new];
        readOnlyJournalEntriesViewController.trip = trip;
        [self.navigationController pushViewController:readOnlyJournalEntriesViewController animated:YES];
    }

    
}

-(IBAction) menuPressed {
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionAnchoredRight) {
        [self.slidingViewController resetTopViewAnimated:true];
    }
    
    else {
        [self.slidingViewController anchorTopViewToRightAnimated:YES];
    }
    
}


@end
