//
//  NewsletterRecipientModel.m
//  Mission List
//
//  Created by Austin Aldrich on 8/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "NewsletterRecipientModel.h"
#import "DatabaseService.h"
#import <CouchbaseLite/CouchbaseLite.h>

@implementation NewsletterRecipientModel

-(NSMutableDictionary *) getDocumentModel {
    return [@{@"first_name": self.firstName, @"last_name": self.lastName, @"email": self.email,
              @"doc_type": @"newsletter_recipient"
              } mutableCopy];
    
}

-(void) save {
    [self saveOrUpdateWithId:self.documentID];
}

+(NewsletterRecipientModel *) getByID:(NSString *)documentID {
    NewsletterRecipientModel *recipient = [NewsletterRecipientModel new];
    
    CBLDatabase *db = [DatabaseService getDatabase];
    CBLDocument *doc = [db documentWithID:documentID];
    
    recipient.documentID = doc.documentID;
    recipient.firstName = [doc.properties valueForKey:@"first_name"];
    recipient.lastName = [doc.properties valueForKey:@"last_name"];
    recipient.email = [doc.properties valueForKey:@"email"];
    
    return recipient;
}

@end
