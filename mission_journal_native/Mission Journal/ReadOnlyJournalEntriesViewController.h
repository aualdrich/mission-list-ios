//
//  ReadOnlyJournalEntriesViewController.h
//  Mission List
//
//  Created by Austin Aldrich on 7/8/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JournalEntriesViewController.h"

@interface ReadOnlyJournalEntriesViewController : JournalEntriesViewController

@property (nonatomic, strong) IBOutlet UIBarButtonItem *followTripButton;


-(IBAction)followTripClicked:(id)sender;

@end
