//
//  MyProfileViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 3/1/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "MyProfileViewController.h"
#import "UserService.h"
#import "BaseUserModel.h"
#import "FBUserModel.h"
#import "AnonymousUserModel.h"
#import "AnonymousProfileViewController.h"
#import "DatabaseService.h"
#import "WelcomeService.h"

@interface MyProfileViewController ()

@end

@implementation MyProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.profilePictureView = [[FBProfilePictureView alloc] init];
        self.navigationItem.title = @"My Profile";
        self.loginView = [[FBLoginView alloc] initWithReadPermissions:@[@"basic_info", @"email", @"user_likes", @"friends"]];
        self.loginView.delegate = self;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(menuPressed:)];
    self.navigationItem.leftBarButtonItem = menuButton;
    
       [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    
}

-(IBAction)menuPressed:(id)sender {
    
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionAnchoredRight) {
        [self.slidingViewController resetTopViewAnimated:YES];
    }
    
    else {
        [self.slidingViewController anchorTopViewToRightAnimated:YES];
    }
    
    
}


-(void) loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    [UserService resetCurrentUser];
    [UserService saveAnonymousUserDefaults];
    [DatabaseService generateViews]; //the user id has changed, so the view's user check is out of sync
    
    AnonymousProfileViewController *anonymousProfileViewController = [AnonymousProfileViewController new];
    [self.navigationController pushViewController:anonymousProfileViewController animated:NO];
}

-(void) loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    NSLog(@"log in event");
}

-(void) loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user {
    
     NSString *profileID = [user objectForKey:@"id"];
    
     self.profileNameLabel.text = [user objectForKey:@"name"];
     self.profilePictureView.profileID = profileID;
     self.profilePictureView.pictureCropping = FBProfilePictureCroppingSquare;
  
}

-(IBAction)testWelcomeEmail:(id)sender {
    [WelcomeService sendWelcomeEmail];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
