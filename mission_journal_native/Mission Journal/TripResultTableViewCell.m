//
//  TripResultTableViewCell.m
//  Mission List
//
//  Created by Austin Aldrich on 7/7/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TripResultTableViewCell.h"

@implementation TripResultTableViewCell


-(void) updateCell {
    self.tripNameLabel.text = self.trip.tripName;
    self.teamNameLabel.text = [self.trip.teamName isEqualToString:@""] ? @"No Team Specified" : self.trip.teamName;
    self.tripDatesLabel.text = [self.trip getFormattedTripDates];
}

@end
