//
//  ViewJournalEntryViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 3/28/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "ViewJournalEntryViewController.h"

#import "PhotoModel.h"
#import "RemoteStorageService.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Branding.h"

@interface ViewJournalEntryViewController ()

@end

@implementation ViewJournalEntryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"MMM d yyyy hh:mm a"];
        self.photos = [[NSMutableArray alloc] init];
        self.mwPhotos = [[NSMutableArray alloc] init];
        self.mwThumbPhotos = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareItem:)];
    
    for (NSString *photoID in self.entry.photoIDs) {
        PhotoModel *photo = [PhotoModel getPhoto:photoID];
        [self.photos addObject:photo];
    }
}

-(IBAction)shareItem:(id)sender {
    
    [FBDialogs presentShareDialogWithLink:[self.entry getEntryURL] name:self.trip.tripName handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
        if(error) {
            NSLog(@"Error: %@", error.description);
        } else {
            NSLog(@"Success!");
        }
 
    }];
    
}
                                            

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = self.entry.entryDescription;
    self.dateLabel.text = [self.dateFormatter stringFromDate:self.entry.entryDate];
    self.descriptionTextView.text = self.entry.entryDescription;
    
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) photosClicked:(id)sender {
    //PhotosViewController *photosViewController = [PhotosViewController new];
    //[photosViewController setPhotosFromModel: self.photos];
    //[self.navigationController pushViewController:photosViewController animated:YES];
    
    [self setPhotosFromModel:self.photos];
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.enableGrid = YES;
    browser.startOnGrid = YES;
    
    [self.navigationController pushViewController:browser animated:NO];
    
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.mwPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.mwPhotos.count)
        return [self.mwPhotos objectAtIndex:index];
    return nil;
}




- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if(index < self.mwThumbPhotos.count) {
        return [self.mwPhotos objectAtIndex:index];
    }
    
    return nil;
}


-(void) setPhotosFromModel:(NSArray *)model {
    [self.mwPhotos removeAllObjects];
    
    for (PhotoModel *p in model) {
        [self.mwPhotos addObject:[MWPhoto photoWithURL:p.getRemoteImageURL]];
        [self.mwThumbPhotos addObject:[MWPhoto photoWithURL:p.getRemoteImageThumbURL]];
    }
}





@end
