//
//  AnonymousProfileViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 3/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <ECSlidingViewController.h>
#import <ECSlidingViewController/UIViewController+ECSlidingViewController.h>

@interface AnonymousProfileViewController : UIViewController <FBLoginViewDelegate>

@property IBOutlet FBLoginView *loginView;

-(IBAction)menuPressed:(id)sender;



@end
