//
//  ReadOnlyJournalEntriesViewController.m
//  Mission List
//
//  Created by Austin Aldrich on 7/8/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "ReadOnlyJournalEntriesViewController.h"
#import "Branding.h"
#import "UserService.h"
#import "AnonymousUserModel.h"

@interface ReadOnlyJournalEntriesViewController ()

@end

@implementation ReadOnlyJournalEntriesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    self.navigationItem.rightBarButtonItem = nil;
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    if(!self.userIsAnonymous) {
        BOOL userIsFollowingTrip = [self.trip userIsFollowingTrip];
        [self toggleFollowTripState:userIsFollowingTrip];
        [self.followTripButton setEnabled:YES];
    }
    
    else {
        [self.followTripButton setEnabled:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) isInPersonalMode {
    return NO;
}

-(BOOL) isInTeamMode {
    return YES;
}

-(void) loadTripEntriesForCurrentView {
    [self getJournalEntriesWithOrWithoutTeamPosts:YES];
    
}

-(IBAction)followTripClicked:(id)sender {
    
    BOOL userIsFollowingTrip = [self.trip userIsFollowingTrip];
    
    if(!userIsFollowingTrip) {
        [self.trip followTrip];
        [self toggleFollowTripState:YES];
    }
    
    else {
        [self.trip unfollowTrip];
        [self toggleFollowTripState:NO];
    }
    
}

-(void) toggleFollowTripState:(BOOL) isCurrentlyInFollowMode {
    if(isCurrentlyInFollowMode) {
        self.followTripButton.tintColor = [UIColor redColor];
        self.followTripButton.title = @"Unfollow Trip";
    }
    else {
        self.followTripButton.tintColor = [Branding getBlueColor];
        self.followTripButton.title = @"Follow Trip";
    }
    
}


-(BOOL) userIsAnonymous {
    BaseUserModel *user = [UserService currentUser];
    
    return [user isKindOfClass:[AnonymousUserModel class]];
}

@end
