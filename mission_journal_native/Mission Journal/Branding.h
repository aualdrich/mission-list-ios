//
//  Branding.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Branding : NSObject

+(UIColor *) getBlueColor;
+(UIColor *) getDarkestGreyColor;
+(UIColor *) getDarkGreyColor;
+(UIColor *) getLightGreyColor;

@end
