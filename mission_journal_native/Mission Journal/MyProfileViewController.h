//
//  MyProfileViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 3/1/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBUserModel.h"
#import <FacebookSDK/FacebookSDK.h>
#import <ECSlidingViewController.h>
#import <ECSlidingViewController/UIViewController+ECSlidingViewController.h>


@interface MyProfileViewController : UIViewController <FBLoginViewDelegate>


@property IBOutlet UILabel *profileNameLabel;
@property IBOutlet FBProfilePictureView *profilePictureView;
@property IBOutlet FBLoginView *loginView;

-(IBAction)menuPressed:(id)sender;

-(IBAction)testWelcomeEmail:(id)sender;


@end
