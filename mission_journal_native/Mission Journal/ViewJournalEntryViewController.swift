//
//  ViewJournalEntryViewController.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class ViewJournalEntryViewController : AddJournalEntryViewController {
    
    @IBOutlet var viewDateTextField = UITextField()
    @IBOutlet var viewDescriptionTextView = UITextView()
    
    init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    
    override func viewDidLoad() {
        self.dateTextField = self.viewDateTextField
        self.descriptionTextView = self.viewDescriptionTextView
    }
    
    
    
    
    
    
}