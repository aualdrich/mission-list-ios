//
//  AddTeammatesToTripViewController.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class AddTeammatesToTripViewController : UIViewController, FBFriendPickerDelegate, UISearchBarDelegate {
    
    var trip = TripModel()
    var friendPickerViewController = FBFriendPickerViewController()
    var searchBar = UISearchBar()
    var searchText = ""
    
        
    init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.friendPickerViewController.title = "Add Teammates"
        self.friendPickerViewController.delegate = self
    }
    
    override func viewDidLoad() {
       super.viewDidLoad()
        
        if !FBSession.activeSession().isOpen {
            // if the session is closed, then we open it here, and establish a handler for state changes
            
            FBSession.activeSession().openWithCompletionHandler({ (session :FBSession!, state :FBSessionState!, error :NSError!) -> Void in
                
                if state.value == FBSessionStateClosedLoginFailed.value {
                        var alertView = UIAlertView(title: "Error", message: error.localizedDescription(), delegate: nil, cancelButtonTitle: "OK")
                        alertView.show()
                }
                
            } as FBSessionStateHandler)
            
        self.friendPickerViewController.loadData()
            self.friendPickerViewController.clearSelection()
            self.friendPickerViewController.presentModallyFromViewController(self, animated: false, handler: nil)
        
        }
        
    }
    
    
    func facebookViewControllerDoneWasPressed() {
        
        var userIDs = NSMutableArray()
        
        for user :AnyObject in self.friendPickerViewController.selection {
            var graphUser = user as FBGraphUser
           
            if self.trip.teammateUserIDs.containsObject(graphUser.objectID) {
                userIDs.addObject(user.objectID)
                self.trip.teammateUserIDs.addObject(graphUser.objectID)
            }
            
        }
        
        self.trip.save()
        
        var userIDsstring = userIDs.componentsJoinedByString(",")
        
        var params = NSMutableDictionary()
        params.setValue(userIDsstring, forKey: "to")
        params.setValue(self.trip.documentID, forKey: "data")
        
        FBWebDialogs.presentRequestsDialogModallyWithSession(nil, message: "You've been invited to join a mission trip!", title: "Mission List", parameters: params, handler: nil)
        
        self.navigationController.popViewControllerAnimated(true)
        
    }
    
    
    func facebookViewControllerCancelWasPressed() {
        self.navigationController.popViewControllerAnimated(true)
    }
    
    
    
    
}