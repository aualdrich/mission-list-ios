//
//  TripResultTableViewCell.h
//  Mission List
//
//  Created by Austin Aldrich on 7/7/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TripModel.h"

@interface TripResultTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *tripNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *teamNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *tripDatesLabel;
@property (nonatomic, strong) TripModel *trip;


-(void) updateCell;



@end
