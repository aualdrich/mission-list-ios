//
//  TeammateProfileViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 4/4/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TeammateProfileViewController.h"

@interface TeammateProfileViewController ()

@end

@implementation TeammateProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
