//
//  TeamListTableViewController.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class TeamListTableViewController : UITableViewController {
    var trip = TripModel()
    var friends = FBProfileModel[]()
    
    override func viewDidLoad() {
        self.navigationItem.title = "My Team";
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "addTeammates")
        self.navigationItem.rightBarButtonItem.enabled = self.trip.isTeammate()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.friends.removeAll(keepCapacity: false)
        
        for profileID :AnyObject in self.trip.teammateUserIDs {
            var strProfileID = profileID as String
            var graphPath = "\(strProfileID)?fields=id,name,picture"
            
            
            FBRequestConnection.startWithGraphPath(graphPath, {(connection: FBRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
                
                if(!error) {
                    var name = result.valueForKey("name") as String
                    var picture = result.valueForKey("picture") as NSDictionary
                    var picture_data = picture.valueForKey("data") as NSDictionary
                    var profilePicUrl = picture_data.valueForKey("url") as String
            
                    var profile = FBProfileModel()
                    profile.profileID = strProfileID;
                    profile.name = name;
                    profile.profileImage = UIImage(data: NSData(contentsOfURL: NSURL(string: profilePicUrl)))
                    
                    self.friends.append(profile)
                    
                    func sortByName(f1 :FBProfileModel, f2 :FBProfileModel) -> Bool {
                       return f1.name.compare(f2.name) == NSComparisonResult.OrderedAscending
                    }
                    
                    var sortedTeam = sort(self.friends, sortByName)
                    
                    self.friends.removeAll(keepCapacity: false)
                    
                    
                    for f in sortedTeam {
                        self.friends.append(f)
                    }
                    
                    self.tableView.reloadData()
                    
                }
              } as FBRequestHandler
            )
            
        }
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return 1
    }
    
    
    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return self.friends.count
    }
    
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
       var cell = tableView.dequeueReusableCellWithIdentifier("CellIdentifier") as UITableViewCell!
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CellIdentifier")
        }
        
        var profile = self.friends[indexPath.row]
        
        cell.textLabel.text = profile.name
        cell.imageView.image = profile.profileImage
        
        return cell
    }
    
    @IBAction func addTeammates() {
        var addTeammatesToTripViewController = AddTeammatesToTripViewController(nibName: "AddTeammatesToTripViewController", bundle: nil)
        addTeammatesToTripViewController.trip = self.trip
        self.navigationController.pushViewController(addTeammatesToTripViewController, animated: true)
    }
    
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return self.trip.isTripOwner()
    }
    
    override func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return 100.0
    }
    
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            var profile = self.friends[indexPath.row]
            var index = -1
            
            for i in 0..self.friends.count {
               var currentProfile = self.friends[i]
                
                if currentProfile == profile {
                    index = i
                    break
                }
            }
            
            self.friends.removeAtIndex(index)
            self.trip.save()
            
            self.tableView.reloadData()
            
        }
    }
    
    /*
    
    
*/
    
}