//
//  TeamListTableViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 3/17/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripModel.h"

@interface TeamListTableViewController : UITableViewController

@property (nonatomic, strong) TripModel *trip;
@property (nonatomic, strong) NSMutableArray *friends;

-(IBAction)addTeammates:(id)sender;

@end
