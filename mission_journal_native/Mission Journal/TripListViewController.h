//
//  TripListViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/4/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ECSlidingViewController/UIViewController+ECSlidingViewController.h>
#import <ECSlidingViewController.h>
#import "TripModel.h"
#import <CouchbaseLite/CouchbaseLite.h>

@interface TripListViewController : UIViewController <UIActionSheetDelegate, UITableViewDataSource, UITableViewDelegate>

-(IBAction)menuPressed:(id)sender;
-(IBAction)addTripPressed:(id)sender;

@property (nonatomic, strong) IBOutlet UITableView *tripsTableView;
@property (nonatomic, strong) NSMutableArray *trips;
@property (nonatomic) NSInteger indexToDelete;
@property (nonatomic, strong) CBLQuery *tripQuery;
@property (nonatomic, strong) IBOutlet UISegmentedControl *tripsViewSegmentedControl;


-(IBAction)tripsViewSegmentedControlChanged:(id)sender;

@end
