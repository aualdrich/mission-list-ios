//
//  TripTests.m
//  Mission Journal
//
//  Created by Austin Aldrich on 4/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "TripModel.h"
#import "DatabaseService.h"

@interface TripTests : XCTestCase

@end

@implementation TripTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testRetrievingAllTrips
{
    CBLDatabase *db = [DatabaseService getDatabase];
    
    CBLQuery *query = [[db viewNamed:@"trip_list"] createQuery];
    NSError *error;
    CBLQueryEnumerator *e = [query run:&error];
    
    NSMutableArray *trips = [TripModel getTrips:e];
    
    XCTAssertNotNil(trips, @"Trips not null");
    XCTAssertNotEqual(trips.count, 0, @"Trips count > 0");
    
}

- (void) testSearch {
    NSArray *tripResults = [TripModel searchPublicTrips:@"G"];
    
    XCTAssertNotNil(tripResults, @"Results were not nil");
    XCTAssertNotEqual(tripResults.count, 0, @"Results count > 0");
}


@end
