//
//  TeamListTableViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 3/17/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TeamListTableViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "FBProfileModel.h"
#import "AddTeammatesToTripViewController.h"


@interface TeamListTableViewController ()

@end

@implementation TeamListTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"My Team";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTeammates:)];
    
    [self.navigationItem.rightBarButtonItem setEnabled:[self.trip isTeammate]];
    
    self.friends = [[NSMutableArray alloc] init];

    
    
    
}

- (void) viewWillAppear:(BOOL)animated {

    [self.friends removeAllObjects];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    for (NSString *profileID in self.trip.teammateUserIDs) {
        NSString *graphPath = [NSString stringWithFormat:@"%@?fields=id,name,picture", profileID];
        
        
        // Configure the cell...
        [FBRequestConnection startWithGraphPath:graphPath completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            
            if(!error) {
                NSString *name = [result valueForKey:@"name"];
                NSDictionary *picture = [result valueForKey:@"picture"];
                NSDictionary *picture_data = [picture valueForKey:@"data"];
                NSString *profilePicUrl = [picture_data valueForKey:@"url"];
                
                FBProfileModel *profile = [FBProfileModel new];
                profile.profileID = profileID;
                profile.name = name;
                profile.profileImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:profilePicUrl]]];
                
                [self.friends addObject:profile];
                
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
                NSArray *sortedTeam = [self.friends sortedArrayUsingDescriptors:@[sortDescriptor]];
                
                [self.friends removeAllObjects];
                [self.friends addObjectsFromArray:sortedTeam];
                
                [self.tableView reloadData];
                
            }
            
            
        }];
        
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.friends.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"CellIdentifier";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    FBProfileModel *profile = [self.friends objectAtIndex:indexPath.row];
    
    cell.textLabel.text = profile.name;
    cell.imageView.image = profile.profileImage;
    
    return cell;
}

-(IBAction)addTeammates:(id)sender {
    AddTeammatesToTripViewController *addTeammatesToTripViewController = [AddTeammatesToTripViewController new];
    addTeammatesToTripViewController.trip = self.trip;
    
    [self.navigationController pushViewController:addTeammatesToTripViewController animated:YES];

}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return [self.trip isTripOwner];
}




// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        FBProfileModel *profile = [self.friends objectAtIndex:indexPath.row];
        
        [self.trip.teammateUserIDs removeObject:profile.profileID];
        [self.friends removeObjectAtIndex:indexPath.row];
        [self.trip save];
        
        [self.tableView reloadData];
    }
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
  //  <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
//    [self.navigationController pushViewController:detailViewController animated:YES];
}


@end
