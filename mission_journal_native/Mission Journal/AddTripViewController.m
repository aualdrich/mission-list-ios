//
//  AddTripViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "AddTripViewController.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "MissionsInMotionAppDelegate.h"
#import "DatabaseService.h"
#import "UserService.h"
#import "TeamListTableViewController.h"
#import <Reachability.h>
#import "TripNotificationBuilder.h"

#define kTripNameTag 0
#define kTeamNameTag 1
#define kStartDateTag 2
#define kEndDateTag 3


@implementation AddTripViewController


- (id)initWithStyle:(UITableViewStyle)style
{
  
    self = [super initWithStyle:style];
    if (self) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.trip == nil)
        self.trip = [[TripModel alloc] init];
    
    // Do any additional setup after loading the view from its nib.
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"MM/dd/yyyy"];
    
    [self setInitialFormState];
    
    self.navigationItem.title = @"Add a Trip";
    
}

- (void) setRequiredFieldValidators:(BZGFormFieldCell *) cell requiredText:(NSString *)text {
    if(text.length < 1) {
        cell.validationState =  BZGValidationStateInvalid;
        [cell.infoCell setText:text];
        cell.shouldShowInfoCell = YES;
    }
    else {
        cell.validationState = BZGValidationStateValid;
        cell.shouldShowInfoCell = NO;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)startDateValueChanged:(id)sender {
    self.startDateCell.textField.text = [self.dateFormatter stringFromDate:self.datePicker.date];
}

-(IBAction)endDateValueChanged:(id)sender {
    self.endDateCell.textField.text = [self.dateFormatter stringFromDate:self.datePicker.date];
}


-(void) textFieldDidBeginEditing:(UITextField *)textField {
    if(textField.tag == kStartDateTag) {
        self.datePicker = [[UIDatePicker alloc] init];
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        [self.datePicker addTarget:self action:@selector(startDateValueChanged:) forControlEvents:UIControlEventValueChanged];
        self.startDateCell.textField.inputView = self.datePicker;
    }
    
    else if(textField.tag == kEndDateTag) {
        self.datePicker = [[UIDatePicker alloc] init];
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        [self.datePicker addTarget:self action:@selector(endDateValueChanged:) forControlEvents:UIControlEventValueChanged];
        self.endDateCell.textField.inputView = self.datePicker;
    }
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

        return [super tableView:tableView cellForRowAtIndexPath:indexPath];

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == self.formSection) {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    } else {
        return 44;
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(NSInteger)  tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [super tableView:tableView numberOfRowsInSection:section];
}

-(IBAction)saveButtonPressed:(id)sender {
    
    if([self validateForm]) {
        
        if(!self.editMode) {
            self.trip.documentID = [[NSUUID UUID] UUIDString];
        }
        
        self.trip.tripName = self.tripNameCell.textField.text;
        self.trip.tripDescription = self.descriptionTripCell.textField.text;
        self.trip.teamName = self.teamNameCell.textField.text;
        self.trip.userID = [[UserService currentUser] userID];
        
        NSString *startDateText =  self.startDateCell.textField.text;
        NSString *endDateText =self.endDateCell.textField.text;
        
        if(startDateText.length > 0)
            self.trip.startDate = [self.dateFormatter dateFromString:startDateText];
        
        if(endDateText.length > 0)
        self.trip.endDate = [self.dateFormatter dateFromString:endDateText];
        
        self.trip.price = self.priceCell.textField.text;
        self.trip.contactEmail = self.contactEmailCell.textField.text;
        self.trip.isPublicTrip = self.publicTripSwitch.isOn;
        
        [self saveToDB:self.trip];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}


-(void) saveToDB:(TripModel *)tripModel {
    [tripModel save];
}

-(BOOL) validateForm {
    
    BOOL all_required_fields_valid = YES;
    BOOL proper_date_ranges = YES;
    
    
    if(self.tripNameCell.textField.text.length < 1)
        all_required_fields_valid = NO;
    
    //user entered a start and end date--we need to check the ranges
    if(self.startDateCell.textField.text.length > 0 && self.endDateCell.textField.text.length > 0) {
        
        NSDate *startDate = [self.dateFormatter dateFromString:self.startDateCell.textField.text];
        NSDate *endDate = [self.dateFormatter dateFromString:self.endDateCell.textField.text];
        
        //start date after end date
        if([startDate compare:endDate] == NSOrderedDescending) {
            proper_date_ranges = NO;
        }
        
    }
    
    if(!proper_date_ranges)
        [self displayInvalidAlert:@"Start date must be after end date."];
    else if(!all_required_fields_valid)
        [self displayInvalidAlert:@"Please fill out all required fields."];


    return all_required_fields_valid && proper_date_ranges;
    
}

-(void) displayInvalidAlert:(NSString *)message {
    UIAlertView *invalidAlertView = [[UIAlertView alloc] initWithTitle:@"Invalid Trip" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [invalidAlertView show];

}

-(void) setInitialFormState {
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(saveButtonPressed:)];
    
    self.navigationItem.rightBarButtonItem = saveButton;
    
    
    self.tripNameCell = [BZGFormFieldCell new];
    self.tripNameCell.label.text = @"Trip Name *";
    self.tripNameCell.textField.placeholder = @"Trip Name";
    self.tripNameCell.textField.delegate = self;
    
    self.descriptionTripCell = [BZGFormFieldCell new];
    self.descriptionTripCell.label.text = @"Description";
    self.descriptionTripCell.textField.placeholder = @"Description";
    self.descriptionTripCell.textField.delegate = self;
    
    self.teamNameCell = [BZGFormFieldCell new];
    self.teamNameCell.label.text = @"Team Name";
    self.teamNameCell.textField.placeholder = @"Team Name";
    
    self.startDateCell = [BZGFormFieldCell new];
    self.startDateCell.label.text = @"Start Date";
    self.startDateCell.textField.placeholder = @"Trip Start Date";
    self.startDateCell.textField.delegate = self;
    self.startDateCell.textField.tag = kStartDateTag;
    
    self.endDateCell = [BZGFormFieldCell new];
    self.endDateCell.label.text = @"End Date";
    self.endDateCell.textField.placeholder = @"Trip End Date";
    self.endDateCell.textField.delegate = self;
    self.endDateCell.textField.tag = kEndDateTag;
    
    self.priceCell = [BZGFormFieldCell new];
    self.priceCell.label.text = @"Price";
    self.priceCell.textField.delegate = self;
    self.priceCell.textField.keyboardType = UIKeyboardTypeDecimalPad;
    
    self.contactEmailCell = [BZGFormFieldCell new];
    self.contactEmailCell.label.text = @"Contact Email";
    self.contactEmailCell.textField.delegate = self;
    self.contactEmailCell.textField.keyboardType = UIKeyboardTypeEmailAddress;
    
    self.publicTripCell = [BZGFormFieldCell new];
    self.publicTripCell.label.text = @"Public Trip?";
    self.publicTripCell.textField.enabled = NO;
    self.publicTripCell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.publicTripSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
    self.publicTripCell.accessoryView = self.publicTripSwitch;
    [self.publicTripSwitch setOn:YES];
    
    
    self.formFieldCells = [NSMutableArray arrayWithObjects:self.tripNameCell, self.descriptionTripCell, self.teamNameCell, self.startDateCell, self.endDateCell, self.priceCell, self.contactEmailCell, self.publicTripCell, nil];

    
    
   }

-(void) updateFormFields {
    if(self.editMode) {
        
        self.tripNameCell.textField.text = self.trip.tripName;
        self.descriptionTripCell.textField.text = self.trip.tripDescription;
        self.teamNameCell.textField.text = self.trip.teamName;
        
        if(self.trip.startDate != nil) {
            self.startDateCell.textField.text = [self.dateFormatter stringFromDate:self.trip.startDate];
        }
        
        if(self.trip.endDate != nil) {
            self.endDateCell.textField.text = [self.dateFormatter stringFromDate:self.trip.endDate];
        }
        
        self.priceCell.textField.text = self.trip.price;
        self.contactEmailCell.textField.text = self.trip.contactEmail;
        
        self.publicTripSwitch.on = self.trip.isPublicTrip;
        
        [self.tableView reloadData];
        
    }

}



@end
