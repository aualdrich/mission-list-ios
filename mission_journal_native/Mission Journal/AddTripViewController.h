//
//  AddTripViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BZGFormViewController.h>
#import <BZGFormFieldCell.h>
#import "Branding.h"
#import "TripModel.h"

@interface AddTripViewController : BZGFormViewController <UITextFieldDelegate>

@property (nonatomic, strong) BZGFormFieldCell *tripNameCell;
@property (nonatomic, strong) BZGFormFieldCell *teamNameCell;
@property (nonatomic, strong) BZGFormFieldCell *startDateCell;
@property (nonatomic, strong) BZGFormFieldCell *endDateCell;
@property (nonatomic, strong) BZGFormFieldCell *publicTripCell;
@property (nonatomic, strong) BZGFormFieldCell *descriptionTripCell;
@property (nonatomic, strong) BZGFormFieldCell *priceCell;
@property (nonatomic, strong) BZGFormFieldCell *contactEmailCell;
@property (nonatomic, strong) UISwitch *publicTripSwitch;

@property (nonatomic, strong) TripModel *trip;
@property (nonatomic) BOOL editMode;


@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

-(IBAction)startDateValueChanged:(id)sender;
-(IBAction)endDateValueChanged:(id)sender;
-(IBAction)saveButtonPressed:(id)sender;


-(BOOL) validateForm;
-(void) displayInvalidAlert:(NSString *)message;
-(void) saveToDB:(TripModel *)tripModel;

-(void) setInitialFormState;
-(void) updateFormFields;

@end
