//
//  PhotoCell.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/12/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "PhotoCell.h"

@implementation PhotoCell


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"PhotoCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
}

-(void) updateCell {
    [self.imageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.imageView setImage:self.image];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
