//
//  AddTagsViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AMTagListView.h>

@interface AddTagsViewController : UIViewController <UIAlertViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet AMTagListView *tagListView;
@property (nonatomic, strong) IBOutlet UITextField *addTagField;
@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) AMTagView *selectedTag;

-(BOOL) tagExists:(NSString *) tag;
-(void) removeTag:(NSString *) tag;
-(void) populateTagsFromModel;


@end
