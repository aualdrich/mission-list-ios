//
//  AddTagsViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/10/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "AddTagsViewController.h"
#import "Branding.h"
#import "TagModel.h"
#import "UserService.h"

@interface AddTagsViewController ()

@end

@implementation AddTagsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tags = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[AMTagView appearance] setTagColor:[Branding getBlueColor]];
    [[AMTagView appearance] setInnerTagColor:[Branding getBlueColor]];
    [[AMTagView appearance] setTextColor:[UIColor whiteColor]];
    
 
    
    
    __weak AddTagsViewController* weakSelf = self;
	[self.tagListView setTapHandler:^(AMTagView *view) {
		weakSelf.selectedTag = view;
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Tag?"
														message:[NSString stringWithFormat:@"Delete %@?", [view tagText]]
													   delegate:weakSelf
											  cancelButtonTitle:@"Cancel"
											  otherButtonTitles:@"Delete", nil];
		[alert show];
	}];
    
    [self populateTagsFromModel];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    //delete it
    if(buttonIndex == 1) {
        NSString *tag = self.selectedTag.tagText;
        [self.tagListView removeTag:self.selectedTag];
        [self removeTag:tag];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    NSString *tagText = textField.text;
    
    if(tagText.length > 0) {
        if(![self tagExists:tagText]) {
           
            TagModel *newTag = [TagModel getNewOrExistingTag:tagText];
            [self.tags addObject:newTag];
            
            [self.tagListView addTag:tagText];
            textField.text = @"";
        }
    }
    
    return YES;
}

-(BOOL) tagExists:(NSString *)tag {
    
    for (TagModel *t in self.tags) {
        if([t.tagName.lowercaseString isEqualToString:tag.lowercaseString]) {
            return YES;
        }
    }
    
    return NO;
    
}

-(void) removeTag:(NSString *)tag {
    
    NSInteger tagIndex = NSNotFound;
    
    for (NSInteger i = 0; i < self.tags.count; i++) {
        TagModel *t = [self.tags objectAtIndex:i];
        
        if([t.tagName.lowercaseString isEqualToString:tag.lowercaseString]) {
            tagIndex = i;
            break;
        }
    }
    
    if(tagIndex != NSNotFound)
        [self.tags removeObjectAtIndex:tagIndex];
    
}

-(void) populateTagsFromModel {
    for (TagModel *tag in self.tags) {
        [self.tagListView addTag:tag.tagName];
    }
}





@end
