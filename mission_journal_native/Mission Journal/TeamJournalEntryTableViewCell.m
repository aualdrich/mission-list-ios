//
//  TeamJournalEntry.m
//  Mission Journal
//
//  Created by Austin Aldrich on 3/30/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TeamJournalEntryTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation TeamJournalEntryTableViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.teamProfilePictureView = [[FBProfilePictureView alloc] init];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void) setPropertiesFromInput: (NSDate *)date :(NSString *)description  userID:(NSString *) userID {
    [super setPropertiesFromInput:date :description];

    
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%@?fields=id,name", userID] completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
    
        NSString *name = [result valueForKey:@"name"];
        self.teamMemberNameLabel.text = name;
    }];
    
    self.teamProfilePictureView.profileID = userID;
    self.teamProfilePictureView.pictureCropping = FBProfilePictureCroppingSquare;

}


@end
