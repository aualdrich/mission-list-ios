//
//  AddTeammatesToTripViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 3/16/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "AddTeammatesToTripViewController.h"

@interface AddTeammatesToTripViewController ()

@end

@implementation AddTeammatesToTripViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.friendPickerViewController = [[FBFriendPickerViewController alloc] init];
        self.friendPickerViewController.title = @"Add Teammates";
        self.friendPickerViewController.delegate = self;
      
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (!FBSession.activeSession.isOpen) {
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession.activeSession openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
            switch (state) {
                case FBSessionStateClosedLoginFailed:
                {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                        message:error.localizedDescription
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
                    break;
                default:
                    break;
            }
        }];
    }
    
    // Do any additional setup after loading the view.
    [self.friendPickerViewController loadData];
    [self.friendPickerViewController clearSelection];
    
    
    [self.friendPickerViewController presentModallyFromViewController:self animated:NO handler:^(FBViewController *sender, BOOL donePressed) {
        
    }];
    
    
   // [self addSearchBarToFriendPickerView];
    



    
}

- (void)addSearchBarToFriendPickerView
{
    if (self.searchBar == nil) {
        CGFloat searchBarHeight = 80.0;
        self.searchBar =
        [[UISearchBar alloc]
         initWithFrame:
         CGRectMake(0,0,
                    self.view.bounds.size.width,
                    searchBarHeight)];
        self.searchBar.autoresizingMask = self.searchBar.autoresizingMask |
        UIViewAutoresizingFlexibleWidth;
        [self.searchBar setSearchBarStyle:UISearchBarStyleMinimal];
        self.searchBar.delegate = self;
        self.searchBar.showsCancelButton = NO;
        self.searchBar.prompt = @"Add friends who are also going on this trip";
        
        [self.friendPickerViewController.canvasView addSubview:self.searchBar];
        CGRect newFrame = self.friendPickerViewController.view.bounds;
        newFrame.size.height -= searchBarHeight;
        newFrame.origin.y = searchBarHeight;
        self.friendPickerViewController.tableView.frame = newFrame;
    }
}

- (void) handleSearch:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    self.searchText = searchBar.text;
    [self.friendPickerViewController updateView];
}

- (void)searchBarSearchButtonClicked:(UISearchBar*)searchBar
{
    [self handleSearch:searchBar];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    self.searchText = nil;
    [searchBar resignFirstResponder];
}

- (BOOL)friendPickerViewController:(FBFriendPickerViewController *)friendPicker
                 shouldIncludeUser:(id<FBGraphUser>)user
{
    
    if([self.trip.teammateUserIDs containsObject:user.objectID])
        return NO;
    
    if (self.searchText && ![self.searchText isEqualToString:@""]) {
        NSRange result = [user.name
                          rangeOfString:self.searchText
                          options:NSCaseInsensitiveSearch];
        if (result.location != NSNotFound) {
            return YES;
        } else {
            return NO;
        }
    } else {
        return YES;
    }
    return YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) facebookViewControllerDoneWasPressed:(id)sender {
    
    NSMutableArray *userIDs = [@[] mutableCopy];
  
    
    for (id<FBGraphUser> user in self.friendPickerViewController.selection) {
        
        if(![self.trip.teammateUserIDs containsObject:user.objectID]) {
            [userIDs addObject:user.objectID];
            [self.trip.teammateUserIDs addObject:user.objectID];
        }
        
    }
    
    [self.trip save];
    
    
    NSString *userIDsString = [userIDs componentsJoinedByString:@","];
    
    
    NSDictionary *params = @{@"to":userIDsString, @"data": self.trip.documentID};
    
    
    [FBWebDialogs
     presentRequestsDialogModallyWithSession:nil
     message:@"You've been invited to join a mission trip!"
     title:@"Mission Journal"
     parameters:params
     handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
     
     
     }
     ];

    [self.navigationController popViewControllerAnimated:YES];
    
    
    
}

-(void) facebookViewControllerCancelWasPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
