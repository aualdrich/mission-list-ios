//
//  TripListViewController.m
//  Mission Journal
//
//  Created by Austin Aldrich on 2/4/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import "TripListViewController.h"
#import "AddTripViewController.h"
#import "MissionsInMotionAppDelegate.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "DatabaseService.h"
#import "TripModel.h"
#import "Branding.h"
#import "JournalEntriesViewController.h"
#import "TripResultTableViewCell.h"
#import <Toast+UIView.h>

@implementation TripListViewController

#define kMyTripsIndex 0
#define kTripsFollowingIndex 1


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(menuPressed:)];
        self.navigationItem.leftBarButtonItem = menuButton;
        
        
        UIBarButtonItem *addTripButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTripPressed:)];
        self.navigationItem.rightBarButtonItem = addTripButton;
        
        self.navigationItem.title = @"My Trips";
 
    }
    return self;
}

-(void) loadTrips {
    
    if(self.tripsViewSegmentedControl.selectedSegmentIndex == kMyTripsIndex) {
        NSMutableArray *trips = [TripModel getTrips:[self.tripQuery run:nil]];
        self.trips = [self getSortedTripsByStartDate:trips];
        
    }
    
    else if(self.tripsViewSegmentedControl.selectedSegmentIndex == kTripsFollowingIndex) {
        NSMutableArray *trips = [TripModel getTripsFollowing];
        self.trips = [self getSortedTripsByStartDate:trips];
        
    }
    
    [self.tripsTableView reloadData];
}

-(NSMutableArray *) getSortedTripsByStartDate:(NSMutableArray *) trips {
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:NO];
        NSArray *sortDescriptors = @[sortDescriptor];
        return [NSMutableArray arrayWithArray:[trips sortedArrayUsingDescriptors:sortDescriptors]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    CBLDatabase *database = [DatabaseService getDatabase];
    
    self.tripQuery = [[database viewNamed:@"trip_list"] createQuery].asLiveQuery;
    
    [self.tripQuery addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    
    [self.tripsTableView registerNib:[UINib nibWithNibName:@"TripResultTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
   
    
    
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if(object == self.tripQuery) {
        [self loadTrips];
    }
}

- (void) viewWillAppear:(BOOL)animated {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    BOOL syncMessageHasBeenSeen = [userDefaults boolForKey:@"sync_message_has_been_seen"];
    
    if(!syncMessageHasBeenSeen) {
        [self.tripsTableView makeToast:@"They will appear here when done." duration:3.0 position:@"top" title:@"Trips Loading..."];
        
        [userDefaults setBool:true forKey:@"sync_message_has_been_seen"];
        [userDefaults synchronize];
    }

    [self loadTrips];
    [self.tripsTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)menuPressed:(id)sender {
    
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionAnchoredRight) {
        [self.slidingViewController resetTopViewAnimated:YES];
    }
    
    else {
            [self.slidingViewController anchorTopViewToRightAnimated:YES];
    }
    
    
}

-(IBAction)addTripPressed:(id)sender {
    AddTripViewController *addTripViewController = [[AddTripViewController alloc] initWithStyle:UITableViewStyleGrouped];
    [self.navigationController pushViewController:addTripViewController animated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.trips count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 103.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TripModel *trip = [self.trips objectAtIndex:indexPath.row];
    
    TripResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[TripResultTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    cell.trip = trip;
    [cell updateCell];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TripModel *trip = [self.trips objectAtIndex:indexPath.row];
    JournalEntriesViewController *journalEntriesViewController = [JournalEntriesViewController new];
    journalEntriesViewController.trip = trip;
    
    [self.navigationController pushViewController:journalEntriesViewController animated:YES];
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    TripModel *trip = [self.trips objectAtIndex:indexPath.row];
    
    return [trip isTripOwner];
    
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

-(BOOL) tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        self.indexToDelete = indexPath.row;
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Really delete this trip? Deleting a trip deletes ALL your journal entries you've made!" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles: nil];
        
        [actionSheet showInView:self.view];
        
        
    }
}




-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        TripModel *trip = [self.trips objectAtIndex:self.indexToDelete];
        [trip deleteDoc];
        
        [self.trips removeObjectAtIndex:self.indexToDelete];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.indexToDelete inSection:0];
        [self.tripsTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];

    }
}


-(IBAction)tripsViewSegmentedControlChanged:(id)sender {
    [self loadTrips];
}

@end
