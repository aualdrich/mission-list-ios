//
//  JournalEntriesViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/9/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripModel.h"

@interface JournalEntriesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) TripModel *trip;
@property (nonatomic, strong) NSMutableDictionary *journalEntries;
@property (nonatomic, strong) IBOutlet UITableView *entriesTableView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *shareTripButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *editTripButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *myTeamButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *newsletterButton;
@property (nonatomic, strong) IBOutlet UISegmentedControl *entryTypeSegmentedControl;
@property (nonatomic, strong) NSString *defaultViewMode;


-(IBAction)addJournalEntry:(id)sender;
-(void) getJournalEntriesWithOrWithoutTeamPosts:(BOOL) includeTeamPosts;
-(IBAction)editTrip:(id)sender;
-(IBAction)myTeamClicked:(id)sender;
-(IBAction)entryTypeValueChanged:(id)sender;
-(IBAction)shareTripClicked:(id)sender;
-(IBAction)newslettersClicked:(id)sender;

-(BOOL) isInPersonalMode;
-(BOOL) isInTeamMode;
-(void) loadTripEntriesForCurrentView;

@end
