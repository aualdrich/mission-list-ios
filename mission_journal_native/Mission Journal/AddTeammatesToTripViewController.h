//
//  AddTeammatesToTripViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 3/16/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import "TripModel.h"

@interface AddTeammatesToTripViewController : UIViewController <FBFriendPickerDelegate, UISearchBarDelegate>

@property (nonatomic, strong) TripModel *trip;
@property (nonatomic, strong) FBFriendPickerViewController *friendPickerViewController;
@property (retain, nonatomic) UISearchBar *searchBar;
@property (retain, nonatomic) NSString *searchText;


@end
