//
//  PhotoCell.h
//  Mission Journal
//
//  Created by Austin Aldrich on 2/12/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UICollectionViewCell

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;


-(void)updateCell;

@end
