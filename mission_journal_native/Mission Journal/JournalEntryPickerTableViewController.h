//
//  JournalEntryPickerTableViewController.h
//  Mission List
//
//  Created by Austin Aldrich on 7/26/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JournalEntryModel.h"
#import "TripModel.h"


@protocol JournalEntryPickerDelegate <NSObject>
-(void) journalEntryPicked:(JournalEntryModel *) journalEntry;
@end

@interface JournalEntryPickerTableViewController : UITableViewController

@property (nonatomic, strong) TripModel *trip;
@property (nonatomic, strong) NSMutableArray *journalEntries;
@property (nonatomic, weak) id <JournalEntryPickerDelegate> delegate;


@end
