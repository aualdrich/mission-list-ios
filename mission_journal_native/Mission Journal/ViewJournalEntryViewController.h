//
//  ViewJournalEntryViewController.h
//  Mission Journal
//
//  Created by Austin Aldrich on 3/28/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripModel.h"
#import "JournalEntryModel.h"
#import "MWPhotoBrowser.h"
#import <AssetsLibrary/AssetsLibrary.h>


@interface ViewJournalEntryViewController : UIViewController <MWPhotoBrowserDelegate>

@property (nonatomic, strong) IBOutlet UILabel *dateLabel;
@property (nonatomic, strong) IBOutlet UITextView *descriptionTextView;
@property (nonatomic, strong) TripModel *trip;
@property (nonatomic, strong) JournalEntryModel *entry;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *mwPhotos;
@property (nonatomic, strong) NSMutableArray *mwThumbPhotos;

-(IBAction)photosClicked:(id)sender;



-(void) setPhotosFromModel:(NSArray *) model;


@end
