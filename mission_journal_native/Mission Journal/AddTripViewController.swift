//
//  AddTripViewController.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class AddTripViewController : BZGFormViewController, UITextViewDelegate {
    
    var tripNameCell = BZGFormFieldCell()
    var teamNameCell = BZGFormFieldCell()
    var startDateCell = BZGFormFieldCell()
    var endDateCell = BZGFormFieldCell()
    var publicTripCell = BZGFormFieldCell()
    var descriptionTripCell = BZGFormFieldCell()
    var publicTripSwitch = UISwitch()
    var trip = TripModel()
    var editMode = false
    var datePicker = UIDatePicker()
    var dateFormatter = NSDateFormatter()
    
    let kTripNameTag = 0
    let kTeamNameTag = 1
    let kStartDateTag = 2
    let kEndDateTag = 3
    
    init(style: UITableViewStyle) {
        super.init(style: style)
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
        
    }
    
    override func viewDidLoad() {
        // Do any additional setup after loading the view from its nib.
        self.dateFormatter.setDateFormat("MM/dd/yyyy")
        
        self.setInitialFormState()
        
        self.navigationItem.title = "Add a Trip";
    }
    
    func setRequiredFieldValidators(cell :BZGFormFieldCell, requiredText text:String) {
        if text.utf16count < 1 {
            cell.validationState = BZGValidationState.Invalid
            cell.infoCell.setText(text)
            cell.shouldShowInfoCell = true
        }
        
        else {
           cell.validationState = BZGValidationState.Valid
           cell.shouldShowInfoCell = false
        }
    }
    
    @IBAction func startDateValueChanged() {
       self.startDateCell.textField.text = self.dateFormatter.stringFromDate(self.datePicker.date)
    }
    
    @IBAction func endDateValueChanged() {
       self.endDateCell.textField.text = self.dateFormatter.stringFromDate(self.datePicker.date)
    }
    
    
    override func textFieldDidBeginEditing(textField :UITextField) {
        if textField.tag == kStartDateTag {
            self.datePicker = UIDatePicker()
            self.datePicker.datePickerMode = UIDatePickerMode.Date
            self.datePicker.addTarget(self, action: "startDateValueChanged", forControlEvents: UIControlEvents.ValueChanged)
            self.startDateCell.textField.inputView = self.datePicker
        }
        
        else if textField.tag == kEndDateTag {
            self.datePicker = UIDatePicker()
            self.datePicker.datePickerMode = UIDatePickerMode.Date
            self.datePicker.addTarget(self, action: "endDateValueChanged", forControlEvents: UIControlEvents.ValueChanged)
            self.endDateCell.textField.inputView = self.datePicker
        }
    }
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        return super.tableView(tableView, cellForRowAtIndexPath: indexPath)
    }
    
    
    override func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        if indexPath.section == self.formSection {
            return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
        }
        else {
            return 44.0
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return 1
    }
    
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
       self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    
    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return super.tableView(tableView, numberOfRowsInSection: section)
    }
    
    
    @IBAction func saveButtonPressed() {
        if self.validateForm() {
            if(!self.editMode) {
               self.trip.documentID = NSUUID.UUID().UUIDString()
            }
            
            self.trip.tripName = self.tripNameCell.textField.text;
            self.trip.tripDescription = self.descriptionTripCell.textField.text;
            self.trip.teamName = self.teamNameCell.textField.text;
            self.trip.userID = UserService.currentUser.userID
            
            var startDateText = self.startDateCell.textField.text
            var endDateText = self.endDateCell.textField.text
            
            if startDateText.utf16count > 0 {
                self.trip.startDate = self.dateFormatter.dateFromString(startDateText)
            }
            
            if endDateText.utf16count > 0 {
                self.trip.endDate = self.dateFormatter.dateFromString(endDateText)
            }
            
            self.trip.isPublicTrip = self.publicTripSwitch.on
            
            self.saveToDB(self.trip)
            
            self.navigationController.popViewControllerAnimated(true)
            
        }
    }

    func saveToDB(tripModel :TripModel) {
        tripModel.save()
    }
    
    
    func validateForm() -> Bool {
        var all_required_fields_valid = true
        var proper_date_ranges = true
        
        if(self.tripNameCell.textField.text.utf16count < 1) {
            all_required_fields_valid = false
        }
        
        
        //user entered a start and end date--we need to check the ranges
        if(!self.startDateCell.textField.text.isEmpty && !self.endDateCell.textField.text.isEmpty) {
           var startDate = self.dateFormatter.dateFromString(self.startDateCell.textField.text)
           var endDate = self.dateFormatter.dateFromString(self.endDateCell.textField.text)
            
           //start date after end date
            if startDate.compare(endDate) == NSComparisonResult.OrderedDescending {
               proper_date_ranges = false
            }
        }
        
        if (!proper_date_ranges) {
            self.displayInvalidAlert("Start date must be after end date.")
        }
        else if(!all_required_fields_valid) {
            self.displayInvalidAlert("Please fill out all required fields.")
        }
        
        
        return all_required_fields_valid && proper_date_ranges
    }
    
    func displayInvalidAlert(message :String) {
       var invalidAlertView = UIAlertView(title: "Invalid Trip", message: message, delegate: self, cancelButtonTitle: "OK")
        invalidAlertView.show()
    }
    
    func setInitialFormState() {
       var saveButton = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.Done, target: self, action: "saveButtonPressed")
       self.navigationItem.rightBarButtonItem = saveButton
        
        self.tripNameCell = BZGFormFieldCell();
        self.tripNameCell.label.text = "Trip Name *";
        self.tripNameCell.textField.placeholder = "Trip Name";
        self.tripNameCell.textField.delegate = self;
        
        
        self.descriptionTripCell = BZGFormFieldCell();
        self.descriptionTripCell.label.text = "Description";
        self.descriptionTripCell.textField.placeholder = "Description";
        self.descriptionTripCell.textField.delegate = self;
    
        self.teamNameCell = BZGFormFieldCell()
        self.teamNameCell.label.text = "Team Name";
        self.teamNameCell.textField.placeholder = "Team Name";
    
        self.startDateCell = BZGFormFieldCell()
        self.startDateCell.label.text = "Start Date";
        self.startDateCell.textField.placeholder = "Trip Start Date";
        self.startDateCell.textField.delegate = self;
        self.startDateCell.textField.tag = kStartDateTag;
    
        self.endDateCell = BZGFormFieldCell()
        self.endDateCell.label.text = "End Date";
        self.endDateCell.textField.placeholder = "Trip End Date";
        self.endDateCell.textField.delegate = self;
        self.endDateCell.textField.tag = kEndDateTag;
    
        self.publicTripCell = BZGFormFieldCell();
        self.publicTripCell.label.text = "Public Trip?";
        self.publicTripCell.textField.enabled = false
        self.publicTripCell.selectionStyle = UITableViewCellSelectionStyle.None;
        self.publicTripSwitch = UISwitch(frame: CGRectZero)
        self.publicTripCell.accessoryView = self.publicTripSwitch;
        self.publicTripSwitch.on = true
    
        var formCells = NSMutableArray()
        formCells.addObject(self.tripNameCell)
        formCells.addObject(self.descriptionTripCell)
        formCells.addObject(self.teamNameCell)
        formCells.addObject(self.startDateCell)
        formCells.addObject(self.endDateCell)
        formCells.addObject(self.publicTripCell)
        
        self.formFieldCells = formCells
    }
    
    func updateFormFields() {
        if self.editMode {
            self.tripNameCell.textField.text = self.trip.tripName;
            self.descriptionTripCell.textField.text = self.trip.tripDescription;
            self.teamNameCell.textField.text = self.trip.teamName;
        
            if(self.trip.startDate != nil) {
                self.startDateCell.textField.text = self.dateFormatter.stringFromDate(self.trip.startDate)
            }
        
            if(self.trip.endDate != nil) {
                self.endDateCell.textField.text = self.dateFormatter.stringFromDate(self.trip.endDate)
            }
        
            self.publicTripSwitch.on = self.trip.isPublicTrip;
        
            self.tableView.reloadData()
        }
    }

}
    
    
    