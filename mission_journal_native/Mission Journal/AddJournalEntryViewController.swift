//
//  AddJournalEntryViewController.swift
//  MissionList
//
//  Created by Austin Aldrich on 7/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class AddJournalEntryViewController : UIViewController, UITextViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MWPhotoBrowserDelegate
 {
    
    var trip = TripModel()
    @IBOutlet var descriptionTextView = UITextView()
    @IBOutlet var dateTextField = UITextField()
    @IBOutlet var isPrivateSwitch = UISwitch()
    var dateFormatter = NSDateFormatter()
    var datePicker = UIDatePicker()
    var addTagsViewController :AddTagsViewController
    var photos = PhotoModel[]()
    var mwPhotos = MWPhoto[]()
    var mwThumbPhotos = MWPhoto[]()
    var journalEntry = JournalEntryModel()
    var newEntryMode = true
    var photoBrowser = MWPhotoBrowser()
    
    let kAddFromCameraIndex = 0
    let kAddFromPhotoLibraryIndex = 1
    let kSelectPhotoActionSheetTag = 0
    let kDeletePhotoActionSheetTag = 1
    
    init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        self.addTagsViewController = AddTagsViewController(nibName: "AddTagsViewController", bundle: nil)
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.dateFormatter.setDateFormat("MMM d yyyy hh:mm a");
    }
    
    override func viewDidLoad() {
       
        self.navigationItem.title = "Add Entry";
        
        var saveButton = UIBarButtonItem(title:"Save", style:UIBarButtonItemStyle.Done, target:self, action: "saveEntry")
        
        self.navigationItem.rightBarButtonItem = saveButton
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.datePicker.datePickerMode = UIDatePickerMode.DateAndTime;
        
        var date = NSDate.date()
        self.dateTextField.text = self.dateFormatter.stringFromDate(date)
        self.datePicker.addTarget(self, action: "dateChanged", forControlEvents: UIControlEvents.ValueChanged)
        self.dateTextField.inputView = self.datePicker;
        
        self.isPrivateSwitch.setOn(self.journalEntry.isPrivate, animated: false)
        
        var tap = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        tap.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tap)
        
        //create mode
        if(self.newEntryMode) {
            self.journalEntry.documentID = NSUUID.UUID().UUIDString()
            self.journalEntry.tripID = self.trip.documentID;
            self.journalEntry.userID = UserService.currentUser.userID
        }
            
            //edit mode
        else {
            self.populateFieldsFromEntry()
        }
        
        
    }
    
    @IBAction func saveEntry() {
        if(self.descriptionTextView.text.utf16count > 0) {
            
            self.journalEntry.entryDescription = self.descriptionTextView.text;
            self.journalEntry.entryDate = self.dateFormatter.dateFromString(self.dateTextField.text)
            self.journalEntry.isPrivate = self.isPrivateSwitch.on
            
            self.saveTags()
            self.savePhotos()
            self.journalEntry.save()
            
            self.navigationController.popViewControllerAnimated(true)
            
        }
            
        else {
            var invalidAlert = UIAlertView(title: "InvalidEntry", message: "Please enter a description of what happened.", delegate: self, cancelButtonTitle: "OK")
            invalidAlert.show();
            
        }
    }
    
    func saveTags() {
        
        var tempTags = NSMutableArray(array: self.addTagsViewController.tags)
        
        var tags = TagModel[]()
        
        for t :AnyObject in tempTags {
            var tempTag = t as TagModel
            tags.append(tempTag)
        }
        
        TagModel.saveTags(tags)
        
        self.journalEntry.tagIDs.removeAll(keepCapacity: false)

        
        for tag in tags {
            self.journalEntry.tagIDs.append(tag.documentID)
        }
        

    }
    
    func savePhotos() {
        var unsyncedPhotos = PhotoModel.loadUnsynchedPhotos()
        
        for photo in self.photos {
            
            if self.journalEntry.photoExists(photo.documentID) {
               self.journalEntry.photoIDs.append(photo.documentID)
            }
            
            photo.isPrivate = self.journalEntry.isPrivate;
            photo.save()
            
            var syncDto = PhotoSyncDto()
            syncDto.photoID = photo.documentID;
            syncDto.isPrivate = photo.isPrivate;
            unsyncedPhotos.append(syncDto)
            photo.saveLocalPhoto(photo.displayImage)
        }
        
        PhotoModel.saveUnsynchedPhotos(unsyncedPhotos)
        PhotoModel.syncRemotePhotosInQueue("")
    }
    
    
    
    func tagsSelected() {
        self.navigationController.pushViewController(self.addTagsViewController, animated: true)
    }
    
    func shareSelected() {
        FBDialogs.presentShareDialogWithLink(self.journalEntry.getEntryURL(), name: self.trip.tripName, nil)
    }
    
    
    func dateChanged() {
        self.dateTextField.text = self.dateFormatter.stringFromDate(self.datePicker.date)
    }
    
    func dismissKeyboard() {
        self.descriptionTextView.resignFirstResponder()
    }
    
    func populateFieldsFromEntry() {
        
        self.descriptionTextView.text = self.journalEntry.entryDescription;
        self.dateTextField.text = self.dateFormatter.stringFromDate(self.journalEntry.entryDate)
        self.datePicker.date = (self.newEntryMode) ? NSDate.date() : self.journalEntry.entryDate
    
        
        for tagID in self.journalEntry.tagIDs {
            var tag = TagModel.getTag(tagID)
            self.addTagsViewController.tags.addObject(tag)
        }
    
    
        for photoID in self.journalEntry.photoIDs {
            var photo = PhotoModel.getPhoto(photoID)
            self.photos.append(photo)
        }
    }
    
    func photosSelected() {
        self.setPhotosFromModel(self.photos)
        self.photoBrowser = MWPhotoBrowser()
        self.photoBrowser.delegate = self
        self.photoBrowser.enableGrid = true;
        self.photoBrowser.startOnGrid = true;
        self.photoBrowser.displayActionButton = true;
        self.photoBrowser.alwaysShowControls = true;
        self.photoBrowser.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "addPhotoClicked")
    
        var photoNavController = UINavigationController(rootViewController: self.photoBrowser)
    
        self.presentViewController(photoNavController, animated: true, completion: nil)
    
    }
    
    /*
    - (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser;
    - (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index;

*/
    
    func numberOfPhotosInPhotoBrowser(photoBrowser :MWPhotoBrowser!) -> NSInteger {
       return self.mwPhotos.count
    }
    
    func photoBrowser(photoBrowser :MWPhotoBrowser!, photoAtIndex index :NSInteger) -> AnyObject! {
        if index < self.mwPhotos.count {
           return self.mwPhotos[index]
        }
        
        return nil
    }
    
    func photoBrowser(photoBrowser :MWPhotoBrowser!, thumbPhotoAtIndex index :NSInteger) -> AnyObject! {
        if index < self.mwPhotos.count {
            return self.mwPhotos[index]
        }
        
        return nil
    }
    
    
    func photoBrowser(photoBrowser :MWPhotoBrowser!, actionButtonpressedForPhotoAtIndex index :UInt) {
        var deletePhotoActionSheet = UIActionSheet(title:"Delete Photo", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Delete")
        deletePhotoActionSheet.tag = kDeletePhotoActionSheetTag
        deletePhotoActionSheet.showInView(self.photoBrowser.navigationController.view)
    }
    
    
    func setPhotosFromModel(model :PhotoModel[]) {
        self.mwPhotos.removeAll(keepCapacity: false)
        self.mwThumbPhotos.removeAll(keepCapacity: false)
        
        for photo in model {
            self.mwPhotos.append(MWPhoto(image: photo.getLocalImage()))
            self.mwThumbPhotos.append(MWPhoto(image: photo.getLocalImage()))
        }
    }
    
    func addPhotoClicked() {
        var addPhotoActionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Take Photo", "Choose Existing")
        
        addPhotoActionSheet.tag = kSelectPhotoActionSheetTag
        addPhotoActionSheet.showInView(self.photoBrowser.navigationController.view)
    }
    
    
    func actionSheet(actionSheet :UIActionSheet, clickedButtonAtIndex buttonIndex :Int) {
        if(actionSheet.tag == kSelectPhotoActionSheetTag && (buttonIndex == kAddFromCameraIndex || buttonIndex == kAddFromPhotoLibraryIndex)) {
            self.pickImages(buttonIndex)
        }
        
        else if(actionSheet.tag == kDeletePhotoActionSheetTag) {
            if buttonIndex == 0 {
                self.deletePhoto()
            }
        }
    }
    
    func pickImages(buttonIndex :Int) {
        var imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        if buttonIndex == kAddFromCameraIndex {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        }
        
        else if buttonIndex == kAddFromPhotoLibraryIndex {
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        
        imagePicker.allowsEditing = true
        self.photoBrowser.navigationController.presentViewController(imagePicker, animated: true, completion: nil)
    }

    
    func imagePickerController(picker :UIImagePickerController, didFinishPickingMediaWithInfo info :NSDictionary) {
        self.dismissViewControllerAnimated(true, completion: nil)
        var image = info.objectForKey(UIImagePickerControllerOriginalImage) as UIImage
        var photoModel = PhotoModel()
        photoModel.documentID = NSUUID.UUID().UUIDString()
        photoModel.displayImage = image;
        photoModel.saveLocalPhoto(image)
    
        self.photos.append(photoModel)
        self.dismissViewControllerAnimated(true, completion: nil)
    
    }
    
    func imagePickerControllerDidCancel(picker :UIImagePickerController) {
       self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func deletePhoto() {
        var photoModel = self.photos[self.photoBrowser.currentIndex]
        
        if(!self.newEntryMode) {
            
            for index in 0..self.journalEntry.photoIDs.count {
                var photoIDAtIndex = self.journalEntry.photoIDs[index]
                
                if photoIDAtIndex == photoModel.documentID {
                   self.journalEntry.photoIDs.removeAtIndex(index)
                }
            }
            
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    
    /*
    
    
*/
    
    
    
}