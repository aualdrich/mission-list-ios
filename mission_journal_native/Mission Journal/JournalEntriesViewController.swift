//
//  JournalEntriesViewController.swift
//  Mission List
//
//  Created by Austin Aldrich on 7/3/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class JournalEntriesViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var trip = TripModel()
    var journalEntries :Dictionary<NSDate, Array<JournalEntryModel>> = Dictionary()
    @IBOutlet var entriesTableView :UITableView
    @IBOutlet var editTripButton = UIBarButtonItem()
    @IBOutlet var myTeamButton = UIBarButtonItem()
    @IBOutlet var entryTypeSegmentedControl = UISegmentedControl()
    @IBOutlet var shareTripButton = UIBarButtonItem()
    
    let kMyEntryTypesIndex = 0
    let kTeamEntryTypesIndex = 1
    
    
    init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "addJournalEntry")
        
        self.journalEntries = Dictionary<NSDate, Array<JournalEntryModel>>()
        
    }
    
    override func viewDidLoad() {
        var personalNib :UINib = UINib(nibName: "JournalEntryTableViewCell", bundle: nil)
        var teamNib :UINib = UINib(nibName: "TeamJournalEntryTableViewCell", bundle: nil)
        
        self.entriesTableView.registerNib(personalNib, forCellReuseIdentifier: "Cell")
        self.entriesTableView.registerNib(teamNib, forCellReuseIdentifier: "TeamCell")
    }

    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.navigationItem.title = self.trip.tripName;
        
        self.loadTripEntriesForCurrentView()
        
        self.navigationItem.rightBarButtonItem.enabled = self.trip.isTeammate()
        
        if(self.editTripButton) {
            self.editTripButton.enabled = self.trip.isTripOwner()
        }
        
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
       return self.journalEntries.count
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
       var items = getEntriesAtKeyIndex(section)
       return items.count
    }
    
    func getEntriesAtKeyIndex(index :Int) -> Array<JournalEntryModel>{
        var items = Array<JournalEntryModel>()
        var key = getEntryKeyAtIndex(index)
        return self.journalEntries[key]!
        
     }
    
    func getEntryKeyAtIndex(index :Int) -> NSDate {
        
        var key :NSDate = NSDate()
        var keys = self.journalEntries.keys
        
        var keyIncrementer = 0
        
        for k in keys {
            if keyIncrementer == index {
               key = k
               break
            }
            
            keyIncrementer++
        }
        
        return key
 
    }
    
    
    func getJournalEntryForIndexPath(indexPath :NSIndexPath) -> JournalEntryModel {
        var itemsAtKey = getEntriesAtKeyIndex(indexPath.section)
        var entry = itemsAtKey[indexPath.row]
        return entry
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        var entry = self.getJournalEntryForIndexPath(indexPath)
        
        if self.isInPersonalMode() {
            var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as JournalEntryTableViewCell
            cell.setPropertiesFromInput(entry.entryDate, entry.entryDescription)
            return cell
        }
        
        else {
            var teamCell = tableView.dequeueReusableCellWithIdentifier("TeamCell") as TeamJournalEntryTableViewCell
            teamCell.setPropertiesFromInput(entry.entryDate, entry.entryDescription, userID: entry.userID)
            return teamCell
        }
        
    }
    
    
    
    @IBAction func addJournalEntry() {
        
        var addJournalEntryViewController = AddJournalEntryViewController(nibName: "AddJournalEntryViewController", bundle: nil)
        
        addJournalEntryViewController.trip = self.trip
        addJournalEntryViewController.newEntryMode = true
        
        self.navigationController.pushViewController(addJournalEntryViewController, animated: true)
    }
    
    
    func getJournalEntriesWithOrWithoutTeamPosts(includeTeamPosts :Bool){
        
        self.journalEntries.removeAll(keepCapacity: false)
        
        var tempEntries = JournalEntryModel.getJournalEntriesForTrip(self.trip.documentID, includeTeamPosts: includeTeamPosts) as NSMutableArray
        
        var entries = JournalEntryModel[]()
        
        for e :AnyObject in tempEntries {
            var castedEntry = e as JournalEntryModel
            entries.append(castedEntry)
        }
        
        if(tempEntries != nil) {
            
            func sortComparer (j1 :JournalEntryModel, j2 :JournalEntryModel) -> Bool {
                return j1.entryDate.compare(j2.entryDate) == NSComparisonResult.OrderedDescending
            }
            
            var sortedEntriesByDate = sort(entries, sortComparer)
            
            for entry in sortedEntriesByDate {
                var calendar = NSCalendar.currentCalendar() as NSCalendar
                var components = calendar.components(.CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: entry.entryDate)
                //we group all entries by month and year, so we always need to use the 1st day to keep dates in the same month and year together.    
                components.setDay(1)
                
                var dateKey = calendar.dateFromComponents(components)
                
                if var itemsForKey = self.journalEntries[dateKey] {
                   itemsForKey.append(entry)
                    
                   self.journalEntries[dateKey] = itemsForKey
                }
                
                else {
                    var itemsForKey = Array<JournalEntryModel>()
                    itemsForKey.append(entry)
                    
                    self.journalEntries[dateKey] = itemsForKey
                }
                
                
            }
            
            self.entriesTableView.reloadData()
            
        }
        
    }
    

    func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        var entry = self.getJournalEntryForIndexPath(indexPath)
        return entry.isJournalEntryOwner()
    }
    
    func tableView(tableView: UITableView!, shouldIndentWhileEditingRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return false
    }
    
    
    func tableView(tableView:UITableView!, didSelectRowAtIndexPath indexPath :NSIndexPath!) {
        
        var entry = self.getJournalEntryForIndexPath(indexPath)
        
        if entry.isJournalEntryOwner(){
           var editJournalEntryViewController = AddJournalEntryViewController(nibName: "AddJournalEntryViewController", bundle: nil)
           
            editJournalEntryViewController.trip = trip
            editJournalEntryViewController.newEntryMode = false
            editJournalEntryViewController.journalEntry = entry
            
            self.navigationController.pushViewController(editJournalEntryViewController, animated: true)
        }
        
        else {
            var viewJournalEntryViewController = ViewJournalEntryViewController(nibName: "ViewJournalEntryViewController", bundle: nil)
            viewJournalEntryViewController.trip = self.trip
            viewJournalEntryViewController.journalEntry = entry
            
            self.navigationController.pushViewController(viewJournalEntryViewController, animated: true)
        }
    }
    
    func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            var entries = self.getEntriesAtKeyIndex(indexPath.section)
            var entryToDelete = entries[indexPath.row]
            
            entryToDelete.deleteDoc()
            entries.removeAtIndex(indexPath.row)
            
            var key = self.getEntryKeyAtIndex(indexPath.section)
            self.journalEntries[key] = entries
            
            self.entriesTableView.reloadData()
            
        }
        
    }
    
    
    func tableView(tableView: UITableView!, titleForHeaderInSection section: Int) -> String! {
       var key = self.getEntryKeyAtIndex(section)
        
        var formatter = NSDateFormatter()
        formatter.setDateFormat("MMM yyyy")
        
        return formatter.stringFromDate(key)
        
    }
    
    @IBAction func editTrip() {
        var editTripViewController = AddTripViewController(style: UITableViewStyle.Plain)
        editTripViewController.trip = self.trip
        editTripViewController.editMode = true
        editTripViewController.updateFormFields()
        self.navigationController.pushViewController(editTripViewController, animated: true)
    }
    
    
    @IBAction func myTeamClicked() {
        var teamListTableViewController = TeamListTableViewController(nibName: "TeamListTableViewController", bundle: nil)
        teamListTableViewController.trip = self.trip
        self.navigationController.pushViewController(teamListTableViewController, animated: true)
    }
    
    
    @IBAction func entryTypeValueChanged() {
        self.loadTripEntriesForCurrentView()
    }
    
    @IBAction func shareTripClicked() {
        FBDialogs.presentShareDialogWithLink(self.trip.getTripURL(), name: self.trip.tripName, handler: nil)
    }
    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return self.isInTeamMode() ? 134.0 : 93.0
    }
    
    
    func isInTeamMode() -> Bool {
       return self.entryTypeSegmentedControl.selectedSegmentIndex == kTeamEntryTypesIndex
    }
    
    func isInPersonalMode() -> Bool {
       return self.entryTypeSegmentedControl.selectedSegmentIndex == kMyEntryTypesIndex
    }
    
    func loadTripEntriesForCurrentView() {
        if isInPersonalMode() {
            self.getJournalEntriesWithOrWithoutTeamPosts(false)
        }
        
        else if isInTeamMode() {
            self.getJournalEntriesWithOrWithoutTeamPosts(true)
        }
    }
}