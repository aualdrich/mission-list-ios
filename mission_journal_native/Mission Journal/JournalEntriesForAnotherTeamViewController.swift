//
//  ViewJournalEntriesReadOnlyViewController.swift
//  Mission List
//
//  Created by Austin Aldrich on 7/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class JournalEntriesForAnotherTeamViewController : JournalEntriesViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var followTripButton: UIBarButtonItem
    @IBOutlet var myTableView :UITableView
    
    @IBAction func followTripButtonClicked() {
        
       toggleFollowTripButton(true)
    }
    
    func toggleFollowTripButton(userFollowingTrip :Bool) {
        
        if(userFollowingTrip) {
           self.followTripButton.tintColor = UIColor.redColor()
           self.followTripButton.title = "Unfollow Trip"
        }
        
        else {
            self.followTripButton.tintColor = Branding.getBlueColor()
            self.followTripButton.title = "Follow Trip"
        }
    }
    
    override func isInPersonalMode() -> Bool {
        return false;
    }
    
    override func isInTeamMode() -> Bool {
        return true;
    }
    
    
    override func loadTripEntriesForCurrentView() {
        self.getJournalEntriesWithOrWithoutTeamPosts(true)
    }
    
    override func viewDidLoad() {
        self.entriesTableView = self.myTableView;
        super.viewDidLoad()
        
    }
    
    
    
}